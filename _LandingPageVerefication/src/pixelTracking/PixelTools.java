package pixelTracking;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class PixelTools {

	public static DesiredCapabilities declareCapabilities() {
		DesiredCapabilities capabilitiesd = DesiredCapabilities.chrome();
		LoggingPreferences logPrefs = new LoggingPreferences();
		logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
		capabilitiesd.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		return capabilitiesd;
	}

	public static void callingRobotToInstallExtension() throws AWTException, InterruptedException {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_TAB);
		Thread.sleep(50);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(50);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(5000);
	}

	public static List<String> types() {
		ArrayList<String> types = new ArrayList<String>();
		types.add("pageload");
		types.add("pageData");
		types.add("InstallAttempt");
		types.add("InstallSuccess");
		types.add("FirstLoad");
		return types;
	}

	public static Map<String, String> hashMapQueryFromUrl(String pixelUrl) {
		Map<String, String> pixelResult = new HashMap<String, String>();

		String[] all = pixelUrl.split("\\?")[1].split("&");

		for (String string : all) {
			String[] pair = string.split("=");

			String val = null;
			if (pair.length == 2) {
				val = pair[1];
			}
			pixelResult.put(pair[0], val);
		}
		return pixelResult;
	}
}
