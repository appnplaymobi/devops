package pixelTracking;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;


public class ExtractPixels {

	private static List<String> types = PixelTools.types();

	private static JSONObject returnJSONObjectMsg(LogEntry entry) {
		JSONObject obj = new JSONObject(entry.getMessage());
		JSONObject message = obj.getJSONObject("message");
		return message;
	}

	public static List<String> getAllPixelsFromPage(WebDriver driver) {
		List<String> pixles = new ArrayList<String>();
		for (LogEntry entry : driver.manage().logs().get(LogType.PERFORMANCE)) {
			JSONObject msg = returnJSONObjectMsg(entry);

			if (msg.getString("method").equals("Network.responseReceived")) {
				String url = msg.getJSONObject("params").getJSONObject("response").getString("url");
				int status = msg.getJSONObject("params").getJSONObject("response").getInt("status");
				if (url.contains("/pixel.aspx?") && status == 200) {
					pixles.add(url);
				}
			}
		}
		return pixles;
	}

	public static List<String> checkResults(List<String> allPixels) {
		boolean[] list = new boolean[4];
		List<String> results = new ArrayList<String>();
		for (String singlePixel : allPixels) {
			Map<String, String> hash = PixelTools.hashMapQueryFromUrl(singlePixel);

			int res = types.indexOf(hash.get("type"));
			if (res != -1) {
				list[res] = true;
			}
		}

		for (int i = 0; i < list.length; i++) {
			if (!list[i]) {
				results.add(types.get(i));
			}
		}
		return results;
	}

}
