package webElements;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;
import tools.*;

public class ElementsInPage extends Base {

	public static String getPageTitle() {
		String pageTitle = driver.getTitle();
		return pageTitle;
	}

	public static String getPageUrl() {
		String pageUrl = driver.getCurrentUrl();
		return pageUrl;
	}

	// public static List<WebElement> findAllLinksInPage() {
	// return findAllLinksInPage(driver);
	// }

	public static List<WebElement> findAllLinksInPage(WebDriver driver) {
		allElements = driver.findElements(By.tagName("a"));
		return allElements;
	}

	// public static List<WebElement> findAllimagesInPage() {
	// return findAllimagesInPage(driver);
	// }

	public static List<WebElement> findAllimagesInPage(WebDriver driver) {
		allElements = driver.findElements(By.tagName("img"));
		return allElements;
	}

	public static String findHrefByHashTag(WebDriver driver) {
		singleElement = driver.findElement(By.xpath("//*[@href='#']"));
		if (!singleElement.getAttribute("class").isEmpty()) {
			return singleElement.getAttribute("class");
		} else {
			return singleElement.getText();
		}
	}

	public static WebElement ifMailInHref(WebDriver driver) {
		try {
			singleElement = driver.findElement(By.xpath("//*[contains(@href,'@')] "));
			if (singleElement.isEnabled()) {
				return singleElement;
			}
		} catch (Exception e) {
			return null;
		}
		return singleElement;
	}

	public static WebElement FindHowToLink(WebDriver driver) {
		try {
			singleElement = driver.findElement(By.className("how-to-link"));
			return singleElement;
		} catch (Exception e) {
			return null;
		}
	}

	public static String locateThePrivacy() {
		try {
			String privacyUrl = findByCssSelector(driver, "privacy");
			return privacyUrl;
		} catch (Exception e) {
			return null;
		}

	}

	public static String locateTheTerms() {
		try {
			String termsUrl = findByCssSelector(driver, "terms");
			return termsUrl;
		} catch (Exception e) {
			return null;
		}
	}

	public static String findByCssSelector(WebDriver driver, String textInUrl) {
		try {
			WebElement elm2 = driver.findElement(By.cssSelector("a[href*='" + textInUrl + "']"));
			String extractedUrl = elm2.getAttribute("href");
			return extractedUrl;
		} catch (Exception e) {
			return null;
		}
	}

	public static WebElement waitByClassName(WebDriver driver, String elementValue) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.className(elementValue))));
		return element;
	}

	public static boolean waitForHowToDiv() {
		try {
			if (waitByClassName(driver, "how-to-close") != null) {
				return true;
			}
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "How it Works link is not clickable");
			return false;
		}
		return false;
	}

	public static void clickOnHowToLink() {
		try {
			FindHowToLink(driver).click();
			waitForHowToDiv();
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "'How it Works' option does not exist in the page");
		}
	}
}
