package dataBaseClass;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import tools.Base;

public class TestSelectClass extends Base {
	public TestSelectClass(String connectionString) {
		_connectionString = connectionString;
	}

	public Map<String, LPdata> selectLinksAndCreative() {
		Map<String, LPdata> mapUrlCreativeName = new HashMap<String, LPdata>();
		try {
			con = DriverManager.getConnection(_connectionString);
			Statement statement = con.createStatement();
			String selectAllUrls = "select creative.url as url , creative.creative_name as creative_name , app.appid as appid, provider from  "
					+ "(select concat('http://',pd.domain_name,'/?pid=11111&c=',pc.creative_name) "
					+ "as url, pc.creative_name as creative_name, pc.product_id  as product_id, provider as provider "
					+ "from (select product_id, p.provider,  MAX(domain_name) as domain_name from [dbo].[product_domains], [dbo].[product] as p "
					+ "WHERE domain_name like '%install.%' " + "and product_id = p.id "
					+ "group by product_id, p.provider) as pd left join [dbo].[product_creatives]  "
					+ "as pc on pd.product_id = pc.product_id ) as creative left join (select t1.product_id, "
					+ "STUFF((select (', ' + t2.appid) "
					+ "from product_appids t2 where t1.product_id = t2.product_id for xml path ('')), 1,2,'') as appid "
					+ "from product_appids t1 group by t1.product_id ) as app on creative.product_id = app.product_id";

			ResultSet rs = statement.executeQuery(selectAllUrls);
			while (rs.next()) {
				String fullUrl = rs.getString(1);
				String creativeName = rs.getString(2);
				String appId = rs.getString(3);
				int providerId = rs.getInt(4);
				if (appId == null) {
					appId = "";
				}
				mapUrlCreativeName.put(fullUrl, new LPdata(creativeName, appId, providerId));
			}
			con.close();
		} catch (Exception e) {
			System.err.println("Problem Connecting! " + e.getMessage());
		}
		return mapUrlCreativeName;
	}

	public Map<String, LPdata> selectLinksAndCreativeForFF() {
		Map<String, LPdata> mapUrlCreativeName = new HashMap<String, LPdata>();
		try {
			con = DriverManager.getConnection(_connectionString);
			Statement statement = con.createStatement();
			String selectAllUrls = "SELECT creative.url as url , creative.creative_name as creative_name , app.appid as appid, provider "
					+ "FROM (select concat('https://',pd.domain_name,'/?pid=11111&c=',pc.creative_name) as url, "
					+ "pc.creative_name as creative_name, "
					+ "pc.product_id  as product_id, "
					+ "provider as provider "
					+ "FROM (select product_id, p.provider, domain_name "
					+ "FROM [dbo].[product_domains], [dbo].[product] as p "
					+ "WHERE domain_name not like '%install[0-9].%' and domain_name not like '%testing.%' and product_id = p.id) as pd "
					+ "left join [dbo].[product_creatives] as pc on pd.product_id = pc.product_id ) as creative "
					+ "left join (SELECT t1.product_id, url as appid "
					+ "FROM product_xpiurls t1) as app on creative.product_id = app.product_id "
					+ "WHERE appid is not NULL";
			ResultSet rs = statement.executeQuery(selectAllUrls);
			while (rs.next()) {
				String fullUrl = rs.getString(1);
				String creativeName = rs.getString(2);
				String appId = rs.getString(3);
				int providerId = rs.getInt(4);
				if (appId == null) {
					appId = "";
				}
				mapUrlCreativeName.put(fullUrl, new LPdata(creativeName, appId, providerId));
			}
			con.close();
		} catch (Exception e) {
			System.err.println("Problem Connecting! " + e.getMessage());
		}
		return mapUrlCreativeName;
	}
}