package dataBaseClass;

import java.util.Arrays;
import java.util.List;

public class LPdata {
	private String creativeName;
	private List<String> appId;
	private int providerId;

	public LPdata(String creative, String appids, int provider) {
		this(creative, Arrays.asList(appids.trim().replaceAll("\\s", "").split(",")), provider);
	}

	public LPdata(String creative, List<String> lstOfAppid, int provider) {
		creativeName = creative;
		appId = lstOfAppid;
		providerId = provider;
	}

	public String getCreativeName() {
		return creativeName;
	}

	public List<String> getAllAppIds() {
		return appId;
	}

	public int getproviderId() {
		return providerId;
	}
}