package dataBaseClass;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import tools.Base;

public class ProdSelectClass extends Base {
	public ProdSelectClass(String connectionString) {
		_connectionString = connectionString;
	}

	public Map<String, LPdataProd> selectLinksAndCreative() {
		Map<String, LPdataProd> mapUrlCreativeName = new HashMap<String, LPdataProd>();
		try {
			con = DriverManager.getConnection(_connectionString);
			Statement statement = con.createStatement();
			String selectAllUrls = "select creative.url as url , creative.creative_name as creative_name, app.appid as appid from "
					+ "(select concat('http://',pd.domain_name,'/?pid=11111&c=',pc.filename) as url, pc.creative_name AS creative_name, pc.filename as page_name, pc.product_id  as product_id "
					+ "from ("
					+ " select product_id, MAX(domain_name) as domain_name from [dbo].[product_domains] " 
					+ "WHERE domain_name in ('install.mysporttab.com' ,'install.myvideotab.com', 'install.streamfrenzy.com', 'install.searchallweb.com') "
					+ "group by product_id) as pd "
					+ "left join [dbo].[product_creatives] as pc "
					+ "on pd.product_id = pc.product_id ) as creative "
					+ "left join (select t1.product_id, STUFF((select (', ' + t2.appid) "
					+ "from product_appids t2 "
					+ "where t1.product_id = t2.product_id for xml path ('')), 1,2,'') as appid "
					+ "from product_appids t1 "
					+ "group by t1.product_id ) as app on creative.product_id = app.product_id";
			ResultSet rs = statement.executeQuery(selectAllUrls);
			while (rs.next()) {
				String fullUrl = rs.getString(1);
				String creativeName = rs.getString(2);
				String appId = rs.getString(3);
				
				mapUrlCreativeName.put(fullUrl, new LPdataProd(creativeName, appId));
			}
			con.close();
		} catch (Exception e) {
			System.err.println("Problem Connecting! " + e.getMessage());
		}
		return mapUrlCreativeName;
	}
}