package dataBaseClass;

import java.util.Arrays;
import java.util.List;

public class LPdataProd {
	private String creativeName;
	private List<String> appId;

	public LPdataProd(String creative, String appids) {
		this(creative, Arrays.asList(appids.trim().replaceAll("\\s", "").split(",")));
	}

	public LPdataProd(String creative, List<String> lstOfAppid) {
		creativeName = creative;
		appId = lstOfAppid;
	}

	public String getCreativeName() {
		return creativeName;
	}

	public List<String> getAllAppIds() {
		return appId;
	}
}