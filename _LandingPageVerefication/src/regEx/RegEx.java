package regEx;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.relevantcodes.extentreports.LogStatus;
import tools.Base;

public class RegEx extends Base {

	public static String findMailRegEx(String regExToFind, String StringToSearch) {
		Pattern checkRegEx = Pattern.compile(regExToFind);
		Matcher regMatch = checkRegEx.matcher(StringToSearch);

		while (regMatch.find()) {
			if (regMatch.group().trim().length() != 0) {
				return regMatch.group(0);
			}
		}
		return null;
	}

	public static void ifPatternExist(String regExToFind, String StringToSearch) {
		ifPatternExist(regExToFind, StringToSearch, 0);
	}

	public static void ifPatternExist(String regExToFind, String StringToSearch, int maxLength) {
		List<String> allMaches = findRegEx(regExToFind, StringToSearch);

		if (allMaches != null) {
			for (String possibleMatch : allMaches) {
				if (possibleMatch.length() > 100) {
					test.log(LogStatus.SKIP, String.format("the pattern: %s was found in the page: %s ",
							regExToFind.replace("\\", ""), possibleMatch.replace("<", "").replace(">", "")));
				} else {
					test.log(LogStatus.FAIL,
							String.format("the pattern: %s was found in the page. The text in the pattern is: %s ",
									regExToFind.replace("\\", ""), possibleMatch));
				}
			}
		}
	}

	public static List<String> findRegEx(String regExToFind, String StringToSearch) {
		return findRegEx(regExToFind, StringToSearch, 0);
	}

	private static List<String> findRegEx(String regExToFind, String StringToSearch, int maxLength) {
		Pattern checkRegEx = Pattern.compile(regExToFind);
		Matcher regMatch = checkRegEx.matcher(StringToSearch);
		List<String> allMatches = new ArrayList<String>();

		while (regMatch.find()) {
			String matchGroup = regMatch.group().trim();
			if (matchGroup.length() > 0 && ((maxLength > 0 && matchGroup.length() <= maxLength) || maxLength == 0)) {
				allMatches.add(matchGroup);
			}
		}
		List<String> ret = new ArrayList<>(new HashSet<>(allMatches));
		return ret;
	}

}
