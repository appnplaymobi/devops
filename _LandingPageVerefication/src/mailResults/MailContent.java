package mailResults;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import tools.GeneralTools;

public class MailContent {

	public static String setContentSuccessTest() throws ParserConfigurationException, SAXException, IOException {

		String asureReportPath = GeneralTools.getReportPathInAsure();

		String finalUrl = "<a href=" + asureReportPath + ">" + asureReportPath + "</a>";
		String mesageContent = setMessageColor("<b>LP Report is ready:</b> ", "black") + finalUrl;
		return mesageContent;
	}

	public static String setMessageColor(String message, String color) {
		String MessageColor = "<font color=" + color + ">" + message + "</font>";
		return MessageColor;
	}
}