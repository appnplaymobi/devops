package reports;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import tools.Base;
import tools.GeneralTools;
import tools.PageFunctions;
import reports.ScrrenShotsAzure;

public class Report extends Base {

	public static void initReport() throws ParserConfigurationException, SAXException, IOException {
		String reportFilePath = reportFile();
		extentReport = new ExtentReports(reportFilePath);
	}

	public static String buildTestName(String testName) {
		testName = "<b>" + testName + "</b>";
		return testName;
	}

	public static void initTestCase(String testName, String browser, String pageUrl) {
		String _testName = buildTestName(testName);
		String _testUrl = GeneralTools.openUrlInNewTab(pageUrl);
		test = extentReport.startTest(_testName, _testUrl);
		test.assignCategory(browser);
	}

	public static void uploadReports()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		uploadHtmlReport();
		uploadBrokenLinksTxt();
	}

	public static void endTestReport() throws URISyntaxException, StorageException, IOException {
		endTestReport(test);
	}

	public static void endTestReport(ExtentTest testName) throws URISyntaxException, StorageException, IOException {
		extentReport.endTest(testName);
	}

	public static void flushTest() {
		extentReport.flush();
	}

	public static void uploadBrokenLinksTxt()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		Path path = Paths.get(PageFunctions.brokenLinksFile());
		File file = new File(PageFunctions.brokenLinksFile());
		String pathToUploadTo = _reportedFolderTest + GeneralTools.returnTodayDate() + "/" + path.getFileName();

		if (file.exists()) {
			ScrrenShotsAzure.uploadToAzure(pathToUploadTo, PageFunctions.brokenLinksFile());
		}
	}

	public static void uploadHtmlReport()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		String reportFilePathToload = reportFile();
		Path path = Paths.get(reportFilePathToload);
		String pathToUploadTo = _reportedFolderTest + GeneralTools.returnTodayDate() + "/" + path.getFileName();
		ScrrenShotsAzure.uploadToAzure(pathToUploadTo, reportFilePathToload);
	}

	public static String reportFile() throws ParserConfigurationException, SAXException, IOException {
		String retpotPath = GeneralTools.getDataFromXML("lpFiles");
		String todayDate = GeneralTools.returnTodayDate();
		String fileName = GeneralTools.getDataFromXML("reportName");

		String pathToReport = retpotPath + _reportedFolderTest + todayDate + fileName;
		return pathToReport;
	}
}