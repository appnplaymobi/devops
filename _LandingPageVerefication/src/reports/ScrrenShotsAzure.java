package reports;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.xml.sax.SAXException;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.relevantcodes.extentreports.LogStatus;

import tools.Base;
import tools.GeneralTools;
import tools.PageFunctions;

public class ScrrenShotsAzure extends Base {

	public static void initAzureConnection() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {
		String AccountName = tools.GeneralTools.getDataFromXML("AccountName");
		String AccountKey = tools.GeneralTools.getDataFromXML("AccountKey");
		final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=" + AccountName
				+ ";AccountKey=" + AccountKey;

		storageAccount = CloudStorageAccount.parse(storageConnectionString);
		client = new CloudBlobClient(storageAccount.getBlobStorageUri(), storageAccount.getCredentials());
		container = client.getContainerReference("qa-reports");
		GeneralTools.createTodayDir();
	}

	public static void takeScrrenShot() throws MalformedURLException, IOException, ParserConfigurationException,
			SAXException, URISyntaxException, StorageException {
		test.log(LogStatus.INFO, test.addScreenCapture(scrrenShotToAzure(PageFunctions.pageName(url).replace("%20", ""))));
	}

	public static String takeScreenShotAndReturnPath(String ScreenShotName)
			throws IOException, ParserConfigurationException, SAXException {
		// _reportedFolderTest for test folder
		// _reportedFolderProd for prod folder

		String imgPathWithName = tools.GeneralTools.getDataFromXML("lpFiles") + _reportedFolderTest + "//"
				+ GeneralTools.returnTodayDate() + "//" + ScreenShotName + ".png";
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(imgPathWithName));
		return imgPathWithName.trim();
	}

	public static String scrrenShotToAzure(String ScreenShotName)
			throws IOException, ParserConfigurationException, SAXException, URISyntaxException, StorageException {

		String fileToUpload = takeScreenShotAndReturnPath(ScreenShotName);
		Path path = Paths.get(fileToUpload);

		String pathToUploadTo = _reportedFolderTest + GeneralTools.returnTodayDate() + "/" + path.getFileName();

		uploadToAzure(pathToUploadTo, fileToUpload);

		return tools.GeneralTools.getDataFromXML("AzureBlob") + pathToUploadTo;
	}

	public static void uploadToAzure(String pathToUploadTo, String fileToUpload)
			throws URISyntaxException, StorageException, IOException {
		block = container.getBlockBlobReference(pathToUploadTo);
		block.getProperties().setContentType("text/html");
		block.uploadFromFile(fileToUpload);
	}

}
