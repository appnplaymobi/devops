package testCases;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import dataBaseClass.LPdata;
import dataBaseClass.TestSelectClass;

import reports.ScrrenShotsAzure;
import tools.Base;
import tools.GeneralTools;
import tools.JSfunctionsOnPage;
import tools.PageFunctions;
import tools.UrlValidation;
import webElements.ElementsInPage;

public abstract class Tests extends Base {
	static JSfunctionsOnPage fop = new JSfunctionsOnPage();
	static GeneralTools tool = new GeneralTools();

	public Tests() throws ParserConfigurationException, SAXException, IOException {
		_testDb = new TestSelectClass(tools.GeneralTools.getDataFromXML("preProdDBconnectionString"));
	}

	public void test() throws ParserConfigurationException, SAXException, IOException, InterruptedException,
			URISyntaxException, StorageException {
		Map<String, LPdata> map = getTestData();

		for (Entry<String, LPdata> entry : map.entrySet()) {
			try {
				url = entry.getKey();

				PageFunctions.getPage(url.toLowerCase());
				deleteCookeisAndLogs();
				long start = GeneralTools.startNanoTime();

				setReport();
				long endTime = GeneralTools.endNanoTime();
				boolean testThePage = UrlValidation.ifPageShouldBeTested(url);
				if (testThePage) {
					ScrrenShotsAzure.takeScrrenShot();

					TestFunctions.calculateResponcePageDuration(endTime, start);

					TestExtensionIdAndXpiUrl(entry);
					testIfValueEquals(entry.getValue().getCreativeName().toLowerCase());
					TestFunctions.testIfStringExistInPage();
					findFaildResourceInPage(url);
					UrlValidation.checkLinksInPage(
							tool.fromWebElementToString(ElementsInPage.findAllLinksInPage(driver), "href"));
					UrlValidation.checkLinksInPage(
							tool.fromWebElementToString(ElementsInPage.findAllimagesInPage(driver), "src"));
					int providerId = entry.getValue().getproviderId();
					if (providerId == 1) {
						System.out.println("yes");
						ElementsInPage.clickOnHowToLink();
					}
					TestFunctions.findRegExPatternInPage();
					TestFunctions.checkIfPagesAreValidWithOutCreavieName();

				}
			} catch (Exception e) {
				System.out.println(e);
			}

		}
	}

	public abstract void initDriver() throws ParserConfigurationException, SAXException, IOException;

	protected abstract Map<String, LPdata> getTestData();

	protected abstract void TestExtensionIdAndXpiUrl(Entry<String, LPdata> entry);

	protected abstract void deleteCookeisAndLogs();

	protected abstract void testIfValueEquals(String creativeNumber) throws MalformedURLException;

	protected abstract void setReport() throws MalformedURLException;

	protected abstract void findFaildResourceInPage(String testedUrl);
}
