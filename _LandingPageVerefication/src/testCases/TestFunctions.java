package testCases;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

import com.relevantcodes.extentreports.LogStatus;

import regEx.RegEx;
import tools.Base;
import tools.GeneralTools;
import tools.JSfunctionsOnPage;
import tools.PageFunctions;

import tools.UrlValidation;
import webElements.ElementsInPage;

public class TestFunctions extends Base {

	static JSfunctionsOnPage fop = new JSfunctionsOnPage();

	public static boolean ifJsValueEquals(String jsToCheck, String jsToExtract) throws MalformedURLException {
		return ifJsValueEquals(jsToCheck, jsToExtract, null);
	}

	public static boolean ifJsValueEquals(String jsToCheck, String jsToExtract, String friendlyNameInReport)
			throws MalformedURLException {
		if (StringUtils.isEmpty(friendlyNameInReport)) {
			friendlyNameInReport = jsToExtract;
		}
		String currentDomain = PageFunctions.getDomainName();
		if (currentDomain == "google.com") {
			driver.navigate().back();

		}
		String pageVal = fop.getJsValue(jsToExtract);
		boolean result = jsToCheck.equals(pageVal);
		if (!result) {
			test.log(LogStatus.FAIL, String.format("%s: page: %s DB: %s ", friendlyNameInReport, pageVal, jsToCheck));
		} else {
			test.log(LogStatus.PASS, String.format("%s: %s Ok", friendlyNameInReport, pageVal));
		}
		return result;
	}

	public static boolean ifJsValueContains(List<String> jsToCheck, String jsToExtract) {
		return ifJsValueContains(jsToCheck, jsToExtract);
	}

	public static Object ifJsValueContains(List<String> jsToCheck, String jsToExtract, String friendlyNameInReport) {
		try {
			if (StringUtils.isEmpty(friendlyNameInReport)) {
				friendlyNameInReport = jsToExtract;
			}
			String pageVal = fop.getJsValue(jsToExtract);
			boolean result = jsToCheck.contains(pageVal);
			if (!result && !pageVal.equals(null)) {
				if (pageVal.isEmpty()) {
					test.log(LogStatus.FAIL,
							String.format("%s: extensionid value is empty in the page %s extensionid in DB: %s ",
									friendlyNameInReport, pageVal, jsToCheck.toString()));
				} else {
					test.log(LogStatus.FAIL, String.format("%s: in page: %s in DB: %s ", friendlyNameInReport, pageVal,
							jsToCheck.toString()));
				}
			} else
				test.log(LogStatus.PASS, String.format("%s Ok: %s ", friendlyNameInReport, pageVal));
			return result;
		} catch (Exception e) {
			test.log(LogStatus.FATAL, "extention id is undefined");
		}
		return null;
	}

	public static void testIfStringExistInPage() {
		fop.findStringInPage(_bold);
		fop.findStringInPage(_new_line);
		fop.findStringInPage(_upper);
		fop.findStringInPage(_olist);
		fop.findStringInPage(_nlist);
		fop.findStringInPage(_lower);
	}

	public static void isMailHref() {
		boolean isMailClickable = ElementsInPage.ifMailInHref(driver) != null;
		if (!isMailClickable)
			test.log(LogStatus.FAIL, String.format("Mail was found in the page, but not clickable"));
		else {
			test.log(LogStatus.PASS, String.format("Mail found and clickable"));
		}
	}

	public static void findRegExPatternInPage() {
		String pageSource = driver.getPageSource();
		String singleDash = "#([^\\s\\W#]*)#";
		String coupleOfDashs = "##([^#]*)##";
		String curlyBrackets = "\\{\\{([^\\}]*)\\}\\}";
		String mailRegExPattern = "[A-Za-z0-9._%]+@[A-Za-z0-9._-]+\\.[A-Za-z]{2,4}";

		RegEx.ifPatternExist(singleDash, pageSource);
		RegEx.ifPatternExist(coupleOfDashs, pageSource);
		RegEx.ifPatternExist(curlyBrackets, pageSource);

		if (!(RegEx.findMailRegEx(mailRegExPattern, pageSource) != null)) {
			test.log(LogStatus.FAIL, String.format("no mail was found in the page"));
		} else {
			isMailHref();
		}
	}

	public static void calculateResponcePageDuration(long endTime, long start) {
		long duration = endTime - start;
		double seconds = ((double) duration / 1000000000.0);
		String doubleToStr = String.valueOf(seconds);
		test.log(LogStatus.INFO, String.format("The response of the page took:  %s ", doubleToStr + " seconds"));
	}

	public static void checkIfPagesAreValidWithOutCreavieName() throws IOException {
		String newUrl = PageFunctions.createUrlWithOutCreative();
		driver.navigate().to(newUrl);
		boolean pageLeadTo404 = UrlValidation.ifPage404(newUrl);
		boolean pageUnknownSource = UrlValidation.ifUnknownSource(newUrl);
		boolean pageIsEmpty = UrlValidation.ifPageIsEmpty(newUrl);
		String _urlInPage = GeneralTools.openUrlInNewTab(newUrl);
		if (pageUnknownSource) {
			test.log(LogStatus.FAIL,
					String.format("The page without creative number leads to 'Unknown Source' page: %s", _urlInPage));
		} else if (pageLeadTo404) {
			test.log(LogStatus.FAIL, String.format("The page without creative number leads to 404: %s", _urlInPage));
		} else if (pageIsEmpty) {
			test.log(LogStatus.FAIL, String.format("The page without creative number is empty page: %s", _urlInPage));
		} else {
			test.log(LogStatus.PASS, String.format("The page without creative number is OK: %s", _urlInPage));
		}
	}

//	public static void testPrivacyLink() {
//		boolean isPrivacyUrlInPage = ElementsInPage.locateThePrivacy() != null;
//		if (isPrivacyUrlInPage) {
//			String privacyUrl = ElementsInPage.locateThePrivacy();
//			driver.navigate().to(privacyUrl);
//			fop.getFaildLoadLinksInPage(privacyUrl);
//			driver.navigate().back();
//		}
//	}
//
//	public static void testTermsLink() {
//		boolean isTermUrlInPage = ElementsInPage.locateTheTerms() != null;
//		if (isTermUrlInPage) {
//			String termUrl = ElementsInPage.locateTheTerms();
//			driver.navigate().to(termUrl);
//			fop.getFaildLoadLinksInPage(termUrl);
//			driver.navigate().back();
//		}
}