package mainPkg;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.microsoft.azure.storage.StorageException;


import dataBaseClass.LPdataProd;
import dataBaseClass.ProdSelectClass;
import reports.Report;
import reports.ScrrenShotsAzure;
import testCases.TestFunctions;
import tools.Base;
import tools.GeneralTools;
import tools.JSfunctionsOnPage;
import tools.PageFunctions;
import tools.UrlValidation;
import webElements.ElementsInPage;

public class MainLandingPageVereficationProd extends Base {
	JSfunctionsOnPage fop = new JSfunctionsOnPage();
	GeneralTools tool = new GeneralTools();

	public MainLandingPageVereficationProd() throws ParserConfigurationException, SAXException, IOException {
		_prodDb = new ProdSelectClass(tools.GeneralTools.getDataFromXML("prodDBconnectionString"));
	}

	@BeforeClass
	public static void startSession() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {
		GeneralTools.initChrome();
		ScrrenShotsAzure.initAzureConnection();
		Report.initReport();
	}

	@AfterClass
	public static void endSession()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		driver.quit();
	}

	@Test
	public void test01() throws IOException, InterruptedException, ParserConfigurationException, SAXException,
			URISyntaxException, StorageException {

		Map<String, LPdataProd> map = _prodDb.selectLinksAndCreative();
		for (Entry<String, LPdataProd> entry : map.entrySet()) {
			url = entry.getKey();
			PageFunctions.getPage(url.toLowerCase());
			long start = GeneralTools.startNanoTime();

//			Report.initTestCase(PageFunctions.pageName(), url.toLowerCase().replace(" ", "%20"));
			long endTime = GeneralTools.endNanoTime();
			boolean testThePage = UrlValidation.ifPageShouldBeTested(url);
			if (testThePage) {
				ScrrenShotsAzure.takeScrrenShot();

				TestFunctions.calculateResponcePageDuration(endTime, start);
				List<String> appIdFromDB = entry.getValue().getAllAppIds();
				

				// the tests for strings and jsValues in the page
				TestFunctions.ifJsValueContains(appIdFromDB, "extensionid", "Extension Id");
//				TestFunctions.testIfValueEquals(entry.getValue().getCreativeName());
				TestFunctions.testIfStringExistInPage();

				// the tests for broken links in the page
//				fop.getFaildLoadLinksInPage(url);
				UrlValidation.checkLinksInPage(
						tool.fromWebElementToString(ElementsInPage.findAllLinksInPage(driver), "href"));
				UrlValidation.checkLinksInPage(
						tool.fromWebElementToString(ElementsInPage.findAllimagesInPage(driver), "src"));

				// the tests for how it works in the page if provider is 1 (ASK)

				// the tests for RegEx pattern in page
				TestFunctions.findRegExPatternInPage();
				// The test for checking the page without the creative name
				TestFunctions.checkIfPagesAreValidWithOutCreavieName();
			}
		}
		Report.endTestReport();
//		extentReport.flush();
		Report.uploadReports();
	}
}