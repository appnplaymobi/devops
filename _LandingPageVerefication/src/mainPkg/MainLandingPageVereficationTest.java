package mainPkg;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;

import javax.mail.MessagingException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.mail.EmailException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.microsoft.azure.storage.StorageException;

import browserInterface.Chrome;
import browserInterface.FireFox;
import mailResults.MailContent;
import mailResults.mailFunctions;
import mailResults.mailSendingReport;
import testCases.Tests;
import reports.Report;
import reports.ScrrenShotsAzure;

public class MainLandingPageVereficationTest {

	@BeforeClass
	public static void startSession() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {
		ScrrenShotsAzure.initAzureConnection();
		Report.initReport();
	}

	@Test
	public void test01() throws IOException, InterruptedException, ParserConfigurationException, SAXException,
			URISyntaxException, StorageException {
		Tests test = new Chrome();
		test.initDriver();
		test.test();
	}

	@Test
	public void test02() throws IOException, InterruptedException, ParserConfigurationException, SAXException,
			URISyntaxException, StorageException {
		Tests test = new FireFox();
		test.initDriver();
		test.test();
	}

	@AfterClass
	public static void endSession()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException, EmailException, MessagingException {

		Report.endTestReport();
		Report.flushTest();
		Report.uploadReports();
		List<String> sendToRecipientsForScanReport = mailFunctions.collectRecipientsForScanReport();
		mailSendingReport.createAndSendMailResults(MailContent.setContentSuccessTest(), "LP Report is ready", sendToRecipientsForScanReport);
	}

}
