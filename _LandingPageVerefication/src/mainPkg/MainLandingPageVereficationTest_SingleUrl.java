package mainPkg;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import dataBaseClass.LPdata;

import dataBaseClass.TestSelectClass;
import testCases.TestFunctions;
import tools.Base;
import tools.GeneralTools;
import tools.JSfunctionsOnPage;
import tools.PageFunctions;
import reports.Report;
import reports.ScrrenShotsAzure;
import tools.UrlValidation;
import webElements.ElementsInPage;

public class MainLandingPageVereficationTest_SingleUrl extends Base {

	JSfunctionsOnPage fop = new JSfunctionsOnPage();
	GeneralTools tool = new GeneralTools();

	public MainLandingPageVereficationTest_SingleUrl() throws ParserConfigurationException, SAXException, IOException {
		_testDb = new TestSelectClass(tools.GeneralTools.getDataFromXML("preProdDBconnectionString"));
	}

	@BeforeClass
	public static void startSession() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {
		GeneralTools.initChrome();
		ScrrenShotsAzure.initAzureConnection();
		Report.initReport();
	}

	@AfterClass
	public static void endSession()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		driver.quit();
	}

	@Test
	public void test01() throws IOException, InterruptedException, ParserConfigurationException, SAXException,
			URISyntaxException, StorageException {
		List<String> links = new ArrayList<String>();
		links.add("https://oops.stream-it.online/?pid=11111&c=store_delay");
		Map<String, LPdata> map = new HashMap<String, LPdata>();
		for (String link: links){
			map.put(link, new LPdata("", "some,thing", 1));
			for (Entry<String, LPdata> entry : map.entrySet()) {
				url = entry.getKey();
				PageFunctions.getPage(url.toLowerCase());
				long start = GeneralTools.startNanoTime();

//				Report.initTestCase(PageFunctions.pageName(), url.toLowerCase().replace(" ", "%20"));
				long endTime = GeneralTools.endNanoTime();
				boolean testThePage = UrlValidation.ifPageShouldBeTested(url);
				if (testThePage) {
					ScrrenShotsAzure.takeScrrenShot();

					TestFunctions.calculateResponcePageDuration(endTime, start);
					List<String> appIdFromDB = entry.getValue().getAllAppIds();

					// the tests for strings and jsValues in the page
					TestFunctions.ifJsValueContains(appIdFromDB, "extensionid", "Extension Id");
//					TestFunctions.testIfValueEquals(entry.getValue().getCreativeName().toLowerCase());
					TestFunctions.testIfStringExistInPage();

					// the tests for broken links in the page
//					fop.getFaildLoadLinksInPage(url);
					UrlValidation.checkLinksInPage(
							tool.fromWebElementToString(ElementsInPage.findAllLinksInPage(driver), "href"));
					UrlValidation.checkLinksInPage(
							tool.fromWebElementToString(ElementsInPage.findAllimagesInPage(driver), "src"));
					
//					test the privacy and terms in the page
//					TestFunctions.testPrivacyLink();
//					TestFunctions.testTermsLink();

					// the tests for WebElements items in the page
					ElementsInPage.clickOnHowToLink();
					// the tests for RegEx pattern in page
					TestFunctions.findRegExPatternInPage();
					// The test for checking the page without the creative name
					TestFunctions.checkIfPagesAreValidWithOutCreavieName();
		}
		
				
			}
		}
		Report.endTestReport();
		extentReport.flush();
		// Report.uploadReports();
	}

}
