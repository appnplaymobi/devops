package browserInterface;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.xml.sax.SAXException;

public interface InitBrowsers {
	WebDriver getDriver() throws ParserConfigurationException, SAXException, IOException;
}