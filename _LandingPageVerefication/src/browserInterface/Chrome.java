package browserInterface;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.xml.sax.SAXException;

import com.relevantcodes.extentreports.LogStatus;

import dataBaseClass.LPdata;
import reports.Report;
import testCases.TestFunctions;
import testCases.Tests;
import tools.GeneralTools;
import tools.PageFunctions;

public class Chrome extends Tests {

	public Chrome() throws ParserConfigurationException, SAXException, IOException {
		super();
	}

	@Override
	public void initDriver() throws ParserConfigurationException, SAXException, IOException {
		String chromeDriver = GeneralTools.getDataFromXML("chromeDriverPath");
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		driver = new ChromeDriver();
	}

	@Override
	protected Map<String, LPdata> getTestData() {
		return _testDb.selectLinksAndCreative();
	}

	@Override
	protected void TestExtensionIdAndXpiUrl(Entry<String, LPdata> entry) {
		List<String> appIdFromDB = entry.getValue().getAllAppIds();
		TestFunctions.ifJsValueContains(appIdFromDB, "extensionid", "Extension Id");
	}

	@Override
	protected void deleteCookeisAndLogs() {
		driver.manage().logs().get(LogType.BROWSER).getAll();
		driver.manage().deleteAllCookies();
	}

	@Override
	protected void testIfValueEquals(String creativeNumber) throws MalformedURLException {
		TestFunctions.ifJsValueEquals(_ip_value, "ip", "IP");
		TestFunctions.ifJsValueEquals(_country_value, "co", "Country");
		TestFunctions.ifJsValueEquals(_chromeValue, "currentBrowser", "Browser");
		TestFunctions.ifJsValueEquals(creativeNumber, "creativenumber", "Creative");
	}
	
	@Override
	protected void findFaildResourceInPage(String testedUrl) {
		List<LogEntry> logEntries = driver.manage().logs().get(LogType.BROWSER).getAll();
		if (logEntries.size() > 0) {
			for (LogEntry entry : logEntries) {
				test.log(LogStatus.FAIL, entry.getMessage());
			}
		}
	}
	
	@Override
	protected void setReport() throws MalformedURLException{
		Report.initTestCase(PageFunctions.pageName(url), "Test on Chrome" , url.toLowerCase().replace(" ", "%20"));
	}
}
