package browserInterface;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.xml.sax.SAXException;
import dataBaseClass.LPdata;
import reports.Report;
import testCases.TestFunctions;
import testCases.Tests;
import tools.GeneralTools;
import tools.PageFunctions;

public class FireFox extends Tests {

	public FireFox() throws ParserConfigurationException, SAXException, IOException {
		super();
	}

	@Override
	public void initDriver() throws ParserConfigurationException, SAXException, IOException {
		String firefoxDriver = GeneralTools.getDataFromXML("firefoxDriverPath");
		System.setProperty("webdriver.gecko.driver", firefoxDriver);
		// driver.manage().window().maximize();
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("acceptInsecureCerts", true);
		driver = new FirefoxDriver(capabilities);

	}

	@Override
	protected Map<String, LPdata> getTestData() {
		return _testDb.selectLinksAndCreativeForFF();
	}

	@Override
	protected void TestExtensionIdAndXpiUrl(Entry<String, LPdata> entry) {
		List<String> xpiUrlFromDB = entry.getValue().getAllAppIds();
		TestFunctions.ifJsValueContains(xpiUrlFromDB, "xpiUrl", "xpi Url");
	}

	@Override
	protected void deleteCookeisAndLogs() {
		((JavascriptExecutor) driver).executeScript("window.localStorage.clear();");
		driver.manage().deleteAllCookies();
	}

	@Override
	protected void testIfValueEquals(String creativeNumber) throws MalformedURLException {
		TestFunctions.ifJsValueEquals(_ip_value, "ip", "IP");
		TestFunctions.ifJsValueEquals(_country_value, "co", "Country");
		TestFunctions.ifJsValueEquals(_firefoxValue, "currentBrowser", "Browser");
		TestFunctions.ifJsValueEquals(creativeNumber, "creativenumber", "Creative");
	}

	@Override
	protected void findFaildResourceInPage(String testedUrl) {
	}

	@Override
	protected void setReport() throws MalformedURLException {
		Report.initTestCase(PageFunctions.pageName(url), "Test on Firefox", url.toLowerCase().replace(" ", "%20"));
	}
}
