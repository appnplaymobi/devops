package tools;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.conn.util.PublicSuffixMatcher;
import org.apache.http.conn.util.PublicSuffixMatcherLoader;
import org.openqa.selenium.WebDriver;

import org.xml.sax.SAXException;

import webElements.ElementsInPage;

public class PageFunctions extends Base {

	public static Map<String, String> hashMapQuery(String query) {
		Map<String, String> result = new HashMap<String, String>();
		if (query != null) {
			String[] allQuery = query.split("&");
			for (String singleQueryString : allQuery) {
				String[] pair = singleQueryString.split("=");

				// check if creative is null (c=null instead of c=p)
				String val = null;
				if (pair.length == 2) {
					val = pair[1];
				}
				result.put(pair[0], val);
			}
		}
		return result;
	}

	public static String pageName(String url) throws MalformedURLException {
		URL aURL = new URL(url);
		String query = aURL.getQuery();
		Map<String, String> map = hashMapQuery(query);
		String finalName = getDomainName() + "-" + map.get("c");
		return finalName;
	}

	public static String createUrlWithOutCreative() throws MalformedURLException {
		URL aURL = new URL(ElementsInPage.getPageUrl());
		String query = aURL.getQuery();
		Map<String, String> map = hashMapQuery(query);
		String finalUrl = "http://" + getHostUrl(url) + "/?pid=" + map.get("pid");
		return finalUrl;
	}

	public static WebDriver getPage(String pageUrl)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		System.out.println("Working on " + pageUrl);
		driver.get(pageUrl);
		driver.manage().window().maximize();

		return driver;
	}

	public static String getHostUrl(String url) throws MalformedURLException {
		URL aURL = new URL(url);
		String domainName = aURL.getHost();
		return domainName;
	}

	public static String getDomainName() throws MalformedURLException {
		PublicSuffixMatcher publicSuffixMatcher = PublicSuffixMatcherLoader.getDefault();
		String domainName = publicSuffixMatcher.getDomainRoot(getHostUrl(url));
		return domainName;
	}

	public static String brokenLinksFile() throws ParserConfigurationException, SAXException, IOException {
		String retpotPath = GeneralTools.getDataFromXML("lpFiles");
		String todayDate = GeneralTools.returnTodayDate();
		String fileName = GeneralTools.getDataFromXML("brokenLinksFileName");

		String pathTobrokenLinksFile = retpotPath + _reportedFolderTest + todayDate + fileName;
		return pathTobrokenLinksFile;
	}
}
