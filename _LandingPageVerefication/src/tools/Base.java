package tools;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.Connection;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;


import dataBaseClass.ProdSelectClass;
import dataBaseClass.TestSelectClass;

public abstract class Base { 
	// DB
	protected String _connectionString;
	protected Connection con;
	protected ProdSelectClass _prodDb;
	protected static TestSelectClass _testDb;
	

	// File
	protected static PrintWriter _writer;
	protected FileWriter fileWriter;
	// Tests
	protected static String url;
	protected static String _ip_value = "82.81.90.118";
	protected static String _chromeValue = "Chrome";
	protected static String _firefoxValue = "Firefox";
	protected static String _country_value = "IL";
	protected static String _bold = "$bold";
	protected static String _new_line = "$new_line";
	protected static String _upper = "$upper";
	protected static String _olist = "$olist";
	protected static String _nlist = "$nlist";
	protected static String _lower = "$lower";
	// WebElements
	protected static URL urlToCheck;
	protected static List<String> urlsList;
	protected static List<WebElement> allElements;
	protected static WebElement singleElement;
	protected static WebDriver driver;
	// Reports
	public static ExtentReports extentReport;
	public static ExtentTest test;
	protected static String _reportedFolderProd = "reports/Prod/";
	protected static String _reportedFolderTest = "reports/Test/";
	// Azure
	public static CloudStorageAccount storageAccount;
	public static CloudBlobClient client;
	public static CloudBlobContainer container;
	public static CloudBlockBlob block;
}
