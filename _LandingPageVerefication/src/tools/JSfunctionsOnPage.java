package tools;

import java.util.List;
import org.openqa.selenium.JavascriptExecutor;

import com.relevantcodes.extentreports.LogStatus;

public class JSfunctionsOnPage extends Base {

	public boolean isJsValueEquals(String value, String jsPageValue) {
		return value.equals(getJsValue(jsPageValue));
	}

	public boolean isJsValueContains(List<String> values, String jsPageValue) {
		String value = getJsValue(jsPageValue);
		return values.contains(value);

	}

	public String getJsValue(String jsPageValue) {
		try {
			return ((JavascriptExecutor) driver).executeScript("return " + jsPageValue).toString();
		} catch (Exception e) {
			test.log(LogStatus.FATAL, "js value function exception: " + e);
		}
		return null;
	}

	public void findStringInPage(String StringToFind) {
		if (driver.getPageSource().contains(StringToFind)) {
			test.log(LogStatus.FAIL, String.format("The srting: %s was found", StringToFind));
		}
	}
}
