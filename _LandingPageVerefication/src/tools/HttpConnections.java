package tools;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import com.relevantcodes.extentreports.LogStatus;

import webElements.ElementsInPage;

public class HttpConnections extends Base {
	
	public static String createConnection() throws IOException {
		String response = "";
		HttpURLConnection connection = (HttpURLConnection) urlToCheck.openConnection();
		connection.connect();
		response = connection.getResponseMessage();
		connection.disconnect();
		return response;
	}

	public static void responseForbidden(String response) {
		String HashTag = ElementsInPage.findHrefByHashTag(driver);
		if (HashTag.contains("Support")) {
			test.log(LogStatus.FAIL, "Found Forbidden url in item: " + HashTag);
		} else if (HashTag.contains("Add")) {
			test.log(LogStatus.WARNING, "Found Forbidden url in item: " + HashTag);
		}
	}

	public static void checkResposeResult(String singlUrl) throws IOException {
		URL urlToCheck = new URL(singlUrl);
		
		String response = createConnection();
		if (response.equals("OK")) {
			test.log(LogStatus.PASS, urlToCheck + " The status of the url is : <b>" + response + "</b>");
		} else if (response.contains("Moved")) {
			test.log(LogStatus.PASS, urlToCheck + " The status of the url is: <b>" + response + "</b>");
		} else if (response.equals("Forbidden")) {
			responseForbidden(response);
		} else {
			test.log(LogStatus.FAIL, "The url: " + urlToCheck + " is: <b>" + response + "</b>");
		}
	}

}