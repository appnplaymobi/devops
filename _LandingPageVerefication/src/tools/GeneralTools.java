package tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GeneralTools extends Base {

	public static String getDataFromXML(String elementName)
			throws ParserConfigurationException, SAXException, IOException {
		File fXmlFile = new File("D:/Bitbucket/devops/LandingPageVerefication/src/tools/externalParams.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();

		NodeList element = doc.getElementsByTagName(elementName);
		Node firstItem = element.item(0);
		return firstItem.getTextContent();
	}
	
	public static String getReportPathInAsure() throws ParserConfigurationException, SAXException, IOException{
		String retpotPath = getDataFromXML("AzureBlob");
		String todayDate = returnTodayDate();
		String fileName = getDataFromXML("reportName");

		String pathToReport = retpotPath + _reportedFolderTest + todayDate + fileName;
		return pathToReport;
	}

	public static WebDriver initChrome() throws ParserConfigurationException, SAXException, IOException {
		System.setProperty("webdriver.chrome.driver", GeneralTools.getDataFromXML("chromeDriverPath"));
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		return driver;
	}

	public static long startNanoTime() throws IOException {
		long start = System.nanoTime();
		return start;
	}

	public static long endNanoTime() throws IOException {
		long end = System.nanoTime();
		return end;
	}

	public static String returnTodayDate() {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		String todayDate = dateFormat.format(now);
		return todayDate;
	}

	public static void createTodayDir() {
		String todayDir = returnTodayDate();
		File dir = new File(todayDir);
		dir.mkdir();
	}

	public static void deleteDir(String dirTodelete) {
		try {
			File f = new File(dirTodelete);
			FileUtils.forceDelete(f);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public List<String> fromWebElementToString(List<WebElement> lst, String value) {
		List<String> stringsList = new ArrayList<String>();
		for (WebElement link : lst) {
			stringsList.add(link.getAttribute(value));
		}
		return stringsList;
	}

	public static void writeToFile(String fileName, String logContent) throws IOException {
		try {
			fileName = PageFunctions.brokenLinksFile();
			PrintWriter printWriter = null;
			File file = new File(fileName);
			try {
				if (!file.exists()) {
					file.createNewFile();
				}
				printWriter = new PrintWriter(new FileOutputStream(fileName, true));
				printWriter.write(logContent);
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				if (printWriter != null) {
					printWriter.flush();
					printWriter.close();
				}
			}
		} catch (Exception e) {
		}
	}

	public static String openUrlInNewTab(String urlPage) {
		urlPage = urlPage.replace(" ", "%20");
		String targetBlank = " target=" + '"' + "_blank" + '"';
		String finalUrl = "<a href=" + urlPage + targetBlank + ">" + urlPage + "</a>";
		return finalUrl;
	}
}
