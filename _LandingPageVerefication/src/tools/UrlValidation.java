package tools;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.validator.routines.UrlValidator;
import org.xml.sax.SAXException;

import com.relevantcodes.extentreports.LogStatus;

import webElements.ElementsInPage;

public class UrlValidation extends Base{


	public static boolean checkIfUrlIsUnknownSource(String urlToCheck) throws IOException {
		try {

			URL u = new URL(urlToCheck);
			HttpURLConnection huc = (HttpURLConnection) u.openConnection();
			huc.setRequestMethod("GET");
			huc.connect();
			@SuppressWarnings("unused")
			String respMsg = huc.getResponseMessage();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static long startNanoTime() throws IOException {
		long start = System.nanoTime();
		return start;
	}

	public static long endNanoTime() throws IOException {
		long start = startNanoTime();
		long end = System.nanoTime();
		long elapsed = end - start;
		return elapsed;
	}

	public static double urlResponseClalulationToSeconds(long responseClaculation) throws IOException {
		double elapsedTimeInSeconds = TimeUnit.MILLISECONDS.convert(responseClaculation, TimeUnit.NANOSECONDS) / 1000.0;
		return elapsedTimeInSeconds;

	}

	public static boolean ifChromeStore(String urlToCheck) throws MalformedURLException {
		String chrome_store = PageFunctions.getHostUrl(urlToCheck);
		if (chrome_store.contains("chrome.google.com")) {
			return true;
		}
		return false;
	}

	public static boolean ifPage404(String urlToCheck) {
		String titleContine = "404";
		boolean isBrokenPage = ElementsInPage.getPageTitle().toLowerCase().contains(titleContine.toLowerCase());
		if (isBrokenPage) {
			return true;
		}
		return false;
	}

	public static boolean ifPageIsEmpty(String urlToCheck) {
		int totalAtags = ElementsInPage.findAllLinksInPage(driver).size();
		int totalSrcTags = ElementsInPage.findAllimagesInPage(driver).size();

		if (totalAtags == 0 && totalSrcTags == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean ifUnknownSource(String urlToCheck) throws IOException {
		boolean knownSource = checkIfUrlIsUnknownSource(urlToCheck);
		if (!knownSource) {
			return true;
		}
		return false;
	}

	public static boolean ifPageShouldBeTested(String url)
			throws ParserConfigurationException, SAXException, IOException {

		String pageUrl = ElementsInPage.getPageUrl();
		boolean pageUnknownSource = ifUnknownSource(pageUrl);
		boolean pageLeadTo404 = ifPage404(pageUrl);
		boolean pageLeadToChromeStore = ifChromeStore(pageUrl);
		boolean pageIsEmpty = ifPageIsEmpty(pageUrl);

		if (pageLeadToChromeStore) {
			test.log(LogStatus.INFO, "Page was not tested, the URL redirected to Chrome Store: " + pageUrl);
			return false;
		} else if (pageLeadTo404) {
			test.log(LogStatus.FATAL, "Page leads to 404: ", pageUrl);
			return false;
		} else if (pageIsEmpty) {
			test.log(LogStatus.FATAL, String.format("empty page: %s", pageUrl));
			return false;
		} else if (pageUnknownSource) {
			test.log(LogStatus.FATAL, String.format("Unknown Source url: %s", pageUrl));
			return false;
		}
		return true;
	}

	public static void checkLinksInPage(List<String> list) throws IOException {
		List<String> urls = list;
		UrlValidator urlValidator = new UrlValidator();

		for (String singlUrl : urls) {
			if (urlValidator.isValid(singlUrl)) {
				try {
					HttpConnections.checkResposeResult(singlUrl);
				} catch (ClassCastException classCalstExp) {
					System.out.println("Class Cast Exception: " + classCalstExp.getMessage());
				} catch (UnknownHostException unKnoneHostExp) {
					System.out.println("Unknown Host Exception: " + unKnoneHostExp.getMessage());
				} catch (Exception ex) {
					System.out.println("Exception: " + ex.toString() + " on: " + singlUrl);
				}

			} else {
				continue;
			}
		}
	}
}
