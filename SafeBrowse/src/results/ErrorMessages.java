package results;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import projUtilities.UseDataFromXml;
import slackMessaging.MessageToSlack;

public class ErrorMessages {

	private static MessageToSlack slack = new MessageToSlack();

	private static String msgContentFoundBadUrls(String harmfulSiteName, String threatType) {
		String msg = String.format(
				"The Url: %s, was marked as site with harmful programs. The threat type from Safe Browse API is: %s",
				harmfulSiteName, threatType);
		return msg;
	}

	private static String msgContentTestPass() {
		String msg = String.format("Test pass, no url's with harmful programs were detected.");
		return msg;
	}

	private static String msgContentLogs(String fileLocation, String fileName) {
		String msg = String.format("test failed see file details: %s: %s", fileLocation, fileName);
		return msg;
	}

	public static void sendTestPassMsg()
			throws IOException, ParserConfigurationException, SAXException {
		slack.notificationMassage("#228b22", msgContentTestPass(), UseDataFromXml.getSlackUrl(),
				UseDataFromXml.getSlackChannel());
	}

	public static void sendTestFailMsg(String harmfulSiteName, String threatType)
			throws IOException, ParserConfigurationException, SAXException {
		slack.notificationMassage("#FF0000", msgContentFoundBadUrls(harmfulSiteName, threatType),
				UseDataFromXml.getSlackUrl(), UseDataFromXml.getSlackChannel());
	}

	public static void sendLogMsg(String fileLocation, String fileName)
			throws IOException, ParserConfigurationException, SAXException {
		slack.notificationMassage("#FF0000", msgContentLogs(fileLocation, fileName), UseDataFromXml.getSlackUrl(),
				UseDataFromXml.getSlackChannel());
	}
}