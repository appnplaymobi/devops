package results;

import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONException;
import org.xml.sax.SAXException;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.services.safebrowsing.model.FindThreatMatchesRequest;
import com.google.api.services.safebrowsing.model.FindThreatMatchesResponse;
import safeBrowesApi.SafeBrowesActions;

public class BrowserSecurity {
	
	private DomainsGetter allMainDomainList;
	private SafeBrowesActions safeBrowes;
	
	public BrowserSecurity() throws IOException, ParserConfigurationException, SAXException {
		this.safeBrowes = new SafeBrowesActions();
		allMainDomainList = DomainsGetter.getInstance();
	}

	// Get list of URLs and check browser security with google API. @param domains
	private void checkBrowserSecurity(List<String> domains) throws IOException {
		try {
			safeBrowes.httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			FindThreatMatchesRequest findThreatMatchesRequest = safeBrowes.createFindThreatMatchesRequest(domains);
			FindThreatMatchesResponse findThreatMatchesResponse = safeBrowes.setKey(findThreatMatchesRequest);
			safeBrowes.returnResults(findThreatMatchesResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void checkSubDomainBrowserSecurity()
			throws JSONException, IOException, ParserConfigurationException, SAXException {
		List<List<String>> allDomains = allMainDomainList.finalDomainsToTest();
		for (List<String> subList : allDomains) {
			checkBrowserSecurity(subList);
		}
	}
}