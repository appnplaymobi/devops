package results;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.google.common.collect.Lists;
import projUtilities.UseDataFromXml;

public class DomainsGetter {

	private final static String postfixURL = "http://factory-service.cloudapp.net/domain/status?domain=";
	private static DomainsGetter singleTonObj = new DomainsGetter();
	private Map<String, String> allSubDomainsPostfix;
	private List<String> allDomainsUrl;

	private DomainsGetter() {
		try {
			this.allDomainsUrl = getDomains();
			this.allSubDomainsPostfix = createMapOfUrlsToExtract();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DomainsGetter getInstance() {
		if (singleTonObj == null) {
			synchronized (DomainsGetter.class) {
				singleTonObj = new DomainsGetter();
			}
		}
		return singleTonObj;
	}

	/**
	 * Get all Domains from page.
	 * 
	 * @return
	 * @throws IOException,
	 * @throws ParserConfigurationException,
	 * @throws SAXException
	 */

	private List<String> getDomainUrlsFromPage(String url)
			throws IOException, ParserConfigurationException, SAXException {
		BufferedReader in = getBufferedReader(url);
		List<String> allUrlsAsList = new ArrayList<String>();
		String line;
		while ((line = in.readLine()) != null) {
			line = line.replace("[", "").replace("]", "").replace("\"", "");
			allUrlsAsList.addAll(Arrays.asList(line.split(",")));
		}
		in.close();
		return allUrlsAsList;
	}

	private List<String> getDomains() {
		String url = UseDataFromXml.getAllActiveDomains();
		try {
			return getDomainUrlsFromPage(url);
		} catch (IOException | ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Map<String, String> createMapOfUrlsToExtract() {
		Map<String, String> urlsToExtract = new HashMap<String, String>();
		for (String domain : allDomainsUrl) {
			urlsToExtract.put(domain, UseDataFromXml.getAllActiveSubDomains() + domain);
		}
		return urlsToExtract;
	}

	public List<String> getAllDomainsUrl() {
		return allDomainsUrl;
	}

	public Map<String, String> gatMapOfUrlsToExtract() {
		return allSubDomainsPostfix;
	}

	// get sub domains For one Domain
	public List<String> getSingleSubDomain(String domain) {
		List<String> subDomains = new ArrayList<String>();
		return subDomains;
	}

	public List<List<String>> finalDomainsToTest()
			throws JSONException, IOException, ParserConfigurationException, SAXException {
		List<String> finalDomains = new ArrayList<String>();
		Map<String, List<String>> map = getSubDomains();
		for (Entry<String, List<String>> entry : map.entrySet()) {
			finalDomains.add(entry.getKey());
			finalDomains.addAll(entry.getValue());
		}
		List<List<String>> subListFinalDomains = Lists.partition(finalDomains, 50);
		return subListFinalDomains;
	}

	private BufferedReader getBufferedReader(String urlToExtractContent) throws MalformedURLException, IOException {
		URL url = new URL(urlToExtractContent);
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		return in;
	}

	private JSONArray getJsonDataPerUrl(String domain) {
		String url = postfixURL.concat(domain);
		try {
			BufferedReader in = getBufferedReader(url);
			String line = null;
			String jsonString = null;
			while ((line = in.readLine()) != null) {
				jsonString = line;
			}
			in.close();
			JSONArray jsonArray = getJsonArrayAString(jsonString);
			return jsonArray;
		} catch (Exception e) {
			return null;
		}
	}

	private JSONArray getJsonArrayAString(String jsonString) {
		String jsonAsString = "{\"obj\":".concat(jsonString).concat("}");
		JSONObject jsnobject = new JSONObject(jsonAsString);
		JSONArray jsonArray = jsnobject.getJSONArray("obj");
		return jsonArray;
	}

	private Map<String, List<String>> getSubDomains()
			throws JSONException, IOException, ParserConfigurationException, SAXException {
		Map<String, List<String>> allSubDomains = new HashMap<>();
		String subDomain = null;
		for (String domain : DomainsGetter.getInstance().getAllDomainsUrl()) {
			if (!UseDataFromXml.getDomainToIgnore().contains(domain)) {
				List<String> allSubDomainsPerUrl = new ArrayList<String>();
				JSONArray jsonArray = getJsonDataPerUrl(domain);
				for (Object object : jsonArray) {
					if (object instanceof JSONObject) {
						JSONObject jsonObj = ((JSONObject) object);
						String postFix = jsonObj.get("Name").toString();
						if (jsonObj.get("Type").equals("CNAME")
								&& !UseDataFromXml.getSubDomainToIgnore().contains(postFix)) {
							subDomain = postFix.concat(".").concat(domain);
							allSubDomainsPerUrl.add(subDomain);
						}
					}
					allSubDomains.put(domain, allSubDomainsPerUrl);
				}
			}
		}
		return allSubDomains;
	}
}