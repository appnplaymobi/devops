package projUtilities;

import java.util.Arrays;
import java.util.List;
import xmlConfiguration.UsingXml;

public class UseDataFromXml {

	
	private static String getConfigurationName() {
		String configurationFileName = "SafeBrowseProjConfiguration.xml";
		return configurationFileName;
	}

	public static String getAllActiveDomains() {
		String activeDomainUrl;
		try {
			activeDomainUrl = UsingXml.getXmlValue("allActiveDomains", getConfigurationName());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return activeDomainUrl;
	}

	public static String getAllActiveSubDomains() {
		String subDomainUrl;
		try {
			subDomainUrl = UsingXml.getXmlValue("allActiveSubDomainsUrl", getConfigurationName());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return subDomainUrl;
	}

	public static String getApiKey() {
		String key;
		try {
			key = UsingXml.getXmlValue("apiKey", getConfigurationName());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return key;
	}

	public static List<String> getDomainToIgnore() {
		List<String> ignoreDomainsAsList = null;
		try {
			String ignoreDomains = UsingXml.getXmlValue("domainsToIgnore", getConfigurationName());
			ignoreDomainsAsList = Arrays.asList(ignoreDomains.split(","));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return ignoreDomainsAsList;
	}

	public static List<String> getSubDomainToIgnore() {
		List<String> ignoreDomainsAsList;
		try {
			String ignoreDomains = UsingXml.getXmlValue("subDomainsToIgnore", getConfigurationName());
			ignoreDomainsAsList = Arrays.asList(ignoreDomains.split(","));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return ignoreDomainsAsList;
	}

	public static String getSlackUrl() {
		String slackBaseUrl = "";
		try {
			slackBaseUrl = UsingXml.getXmlValue("slackUrl", getConfigurationName());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return slackBaseUrl;
	}

	public static String getSlackChannel() {
		String slackChannel = "";
		try {
			slackChannel = UsingXml.getXmlValue("automationAlertsChannel", getConfigurationName());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return slackChannel;
	}
}