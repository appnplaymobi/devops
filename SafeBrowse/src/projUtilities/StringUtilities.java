package projUtilities;

import java.util.List;

public class StringUtilities {

	private static String removeLastCharOptional(StringBuilder s) {
		return s.substring(0, s.length() - 1);
	}

	public static String removeFirstAndLastChar(String s) {
		return s.substring(1, s.length() - 1);
	}

	public static String convertFromListToString(List<String> urls) {
		StringBuilder listString = new StringBuilder("");
		for (String singleUrl : urls) {
			listString.append(singleUrl).append(",");
		}
		return removeLastCharOptional(listString);
	}
}