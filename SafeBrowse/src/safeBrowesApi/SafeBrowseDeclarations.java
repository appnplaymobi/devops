package safeBrowesApi;

import java.util.Arrays;
import java.util.List;
import com.google.api.client.json.jackson2.JacksonFactory;
import projUtilities.UseDataFromXml;

public class SafeBrowseDeclarations {

	public final JacksonFactory GOOGLE_JSON_FACTORY = JacksonFactory.getDefaultInstance();
	public final String GOOGLE_API_KEY = UseDataFromXml.getApiKey();
	public final String GOOGLE_CLIENT_ID = "xx";
	public final String GOOGLE_CLIENT_VERSION = "0.0.1";
	public final String GOOGLE_APPLICATION_NAME = "xx";
	public final List<String> GOOGLE_THREAT_TYPES = getThreatTypes();
	public final List<String> GOOGLE_PLATFORM_TYPES = getPlatformTypes();
	public final List<String> GOOGLE_THREAT_ENTRYTYPES = getThreatEntryTypes();

	private List<String> getThreatTypes() {
		List<String> threatTypes = Arrays.asList(new String[] { "THREAT_TYPE_UNSPECIFIED", "MALWARE",
				"SOCIAL_ENGINEERING", "UNWANTED_SOFTWARE", "POTENTIALLY_HARMFUL_APPLICATION" });
		return threatTypes;
	}

	private List<String> getPlatformTypes() {
		List<String> threatTypes = Arrays.asList(new String[] { "ANY_PLATFORM" });
		return threatTypes;
	}

	private List<String> getThreatEntryTypes() {
		List<String> threatTypes = Arrays.asList(new String[] { "URL" });
		return threatTypes;
	}
}