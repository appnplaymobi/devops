package safeBrowesApi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.safebrowsing.Safebrowsing;
import com.google.api.services.safebrowsing.model.ClientInfo;
import com.google.api.services.safebrowsing.model.FindThreatMatchesRequest;
import com.google.api.services.safebrowsing.model.FindThreatMatchesResponse;
import com.google.api.services.safebrowsing.model.ThreatEntry;
import com.google.api.services.safebrowsing.model.ThreatInfo;
import com.google.api.services.safebrowsing.model.ThreatMatch;
import results.ErrorMessages;

public class SafeBrowesActions {

	public NetHttpTransport httpTransport;
	private SafeBrowseDeclarations safeBrowse;

	public SafeBrowesActions() {
		this.safeBrowse = new SafeBrowseDeclarations();
	}

	public void returnResults(FindThreatMatchesResponse findThreatMatchesResponse)
			throws IOException, ParserConfigurationException, SAXException {
		List<ThreatMatch> threatMatches = findThreatMatchesResponse.getMatches();
		if (threatMatches != null && threatMatches.size() > 0) {
			for (ThreatMatch threatMatch : threatMatches) {
				ErrorMessages.sendTestFailMsg(threatMatch.getThreat().getUrl(), threatMatch.getThreatType());
			}
		}
	}

	public FindThreatMatchesResponse setKey(FindThreatMatchesRequest findThreatMatchesRequest) throws IOException {

		Safebrowsing.Builder safebrowsingBuilder = new Safebrowsing.Builder(httpTransport,
				safeBrowse.GOOGLE_JSON_FACTORY, null).setApplicationName(safeBrowse.GOOGLE_APPLICATION_NAME);
		Safebrowsing safebrowsing = safebrowsingBuilder.build();
		FindThreatMatchesResponse findThreatMatchesResponse = safebrowsing.threatMatches()
				.find(findThreatMatchesRequest).setKey(safeBrowse.GOOGLE_API_KEY).execute();
		return findThreatMatchesResponse;
	}

	public FindThreatMatchesRequest createFindThreatMatchesRequest(List<String> urls) {
		FindThreatMatchesRequest findThreatMatchesRequest = new FindThreatMatchesRequest();
		ThreatInfo threatInfo = getThreatInfo(findThreatMatchesRequest);
		addThreatEntries(urls, findThreatMatchesRequest, threatInfo);
		return findThreatMatchesRequest;
	}

	private void addThreatEntries(List<String> urls, FindThreatMatchesRequest findThreatMatchesRequest,
			ThreatInfo threatInfo) {
		List<ThreatEntry> threatEntries = new ArrayList<ThreatEntry>();
		for (String url : urls) {
			ThreatEntry threatEntry = new ThreatEntry();
			threatEntry.set("url", url);
			threatEntries.add(threatEntry);
		}
		threatInfo.setThreatEntries(threatEntries);
		findThreatMatchesRequest.setThreatInfo(threatInfo);
	}

	private void getClientInfo(FindThreatMatchesRequest findThreatMatchesRequest) {
		ClientInfo clientInfo = new ClientInfo();
		clientInfo.setClientId(safeBrowse.GOOGLE_CLIENT_ID);
		clientInfo.setClientVersion(safeBrowse.GOOGLE_CLIENT_VERSION);
		findThreatMatchesRequest.setClient(clientInfo);
	}

	private ThreatInfo getThreatInfo(FindThreatMatchesRequest findThreatMatchesRequest) {
		getClientInfo(findThreatMatchesRequest);
		ThreatInfo threatInfo = new ThreatInfo();
		threatInfo.setThreatTypes(safeBrowse.GOOGLE_THREAT_TYPES);
		threatInfo.setPlatformTypes(safeBrowse.GOOGLE_PLATFORM_TYPES);
		threatInfo.setThreatEntryTypes(safeBrowse.GOOGLE_THREAT_ENTRYTYPES);
		return threatInfo;
	}

}
