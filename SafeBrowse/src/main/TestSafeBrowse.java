package main;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import VM.TurnOffVM;
import results.BrowserSecurity;

public class TestSafeBrowse {

	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		BrowserSecurity browserSecurity = new BrowserSecurity();
		browserSecurity.checkSubDomainBrowserSecurity();
		
		TurnOffVM turnOff = new TurnOffVM();
		turnOff.sendShutDownCommand();
	}
}