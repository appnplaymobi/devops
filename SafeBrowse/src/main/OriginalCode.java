package main;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.safebrowsing.Safebrowsing;
import com.google.api.services.safebrowsing.model.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OriginalCode {

	public static final JacksonFactory GOOGLE_JSON_FACTORY = JacksonFactory.getDefaultInstance();
	public static final String GOOGLE_API_KEY = "AIzaSyB7DMqNi5bSYbj5Zz24QWMdp0j5smL5BuE"; // Google API key
	public static final String GOOGLE_CLIENT_ID = "xx"; // client id
	public static final String GOOGLE_CLIENT_VERSION = "0.0.1"; // client version
	public static final String GOOGLE_APPLICATION_NAME = "xx"; // appication name
	public static final List<String> GOOGLE_THREAT_TYPES = Arrays.asList(new String[] { "THREAT_TYPE_UNSPECIFIED",
			"MALWARE", "SOCIAL_ENGINEERING", "UNWANTED_SOFTWARE", "POTENTIALLY_HARMFUL_APPLICATION" });
	public static final List<String> GOOGLE_PLATFORM_TYPES = Arrays.asList(new String[] { "ANY_PLATFORM" });
	public static final List<String> GOOGLE_THREAT_ENTRYTYPES = Arrays.asList(new String[] { "URL" });
	public static NetHttpTransport httpTransport;

	public static void main(String[] args) throws GeneralSecurityException, IOException {
		test();

	}

	public static void test() throws GeneralSecurityException, IOException {
		httpTransport = GoogleNetHttpTransport.newTrustedTransport();

		List<String> urls = Arrays.asList(new String[] { "2-pdf.com", "24-stream.com", "365-stream.com",
				"4thegamez.com", "aamobilemonetizer.com", "algochord.com", "allsportscore.com", "any-templates.com",
				"apis.services", "appaporter.com", "artab.co", "bakinu.com", "bazzsearch.com",
				"besidy.com", "bestradiostationsonline.com", "bezzona.xyz", "bholnews.com", "bhuygv.com",
				"bitcoin-converter.com", "brillianttab.com", "browse-search.com", "browserhunt.com", "browsersweep.com",
				"carryon.technology", "checkthespeed.com", "chill-tab.com", "chromewebapps.info", "chunckapp.co.uk",
				"chunckapp.com", "clashroyaletab.com", "cleanmac.info", "cleanmybrowser.com", "codeset.biz",
				"codeset.co", "combo-search.com", "convertowiz.com", "coupicon.com", "crypt-converter.com",
				"cryptoconvertertab.com", "cryptoverto.com", "cubokit.com", "cyberspacedata.com", "dogsrest.com",
				"dondelink.com", "easy-pdf.com", "ebooks-club.com", "elirozen.biz", "emoji-search.com",
				"escreengrab.com", "ethereum-converter.com", "fashionbox.online", "fghnbv.com", "fileupstick.com",
				"foofinders.com", "fooriza.com", "free-converterz.com", "free-streamz.com", "funappstudio.net",
				"funkystreams.com", "gag.cool", "game-jungle.com", "gamez4us.com", "gangnam.co", "get-maps.com",
				"getlive.news", "getsafefinder.com", "getscore.online", "getsignal.xyz", "getsportscore.com",
				"getvmucho.com", "gimojis.com", "giph-it.com", "globo-search.com", "goldenbrowse.com", "goprivate.co",
				"helperbar.com", "hkijngy.me", "horoscope-zone.com", "ijnewhb.com", "ijnygv.com", "imobileapp.xyz",
				"incognitosearches.com", "infows.com", "installbox.com", "installnext.com", "ispinner.online",
				"jamboxlive.com", "jpost-tab.com", "kakadubrowser.com", "kariloo.com", "keepmypixel.com",
				"leonardosearch.com", "lightboltvpn.com", "linkury.ag", "linkury.be", "linkury.bz", "linkury.cc",
				"linkury.co", "linkury.co.in", "linkury.co.nz", "linkury.co.uk", "linkury.com.ag", "linkury.la",
				"linkury.me", "linkury.mobi", "linkury.tv", "linkury.us", "linkury.ws", "linkury.xxx",
				"linkurysmartbar.org", "listenmusicnow.com", "live-streaming.online", "livebaseballstreamonline.com",
				"livebasketballstreamonline.com", "livecricketstreamonline.com", "livefootballstreamonline.com",
				"livesportstreamonline.com", "lyrixjunkie.com", "macapproduct.com", "macappsolutions.com",
				"maconomi.com", "mapsstars.com", "mecappstore.com", "memetab.com", "meshbrowse.com",
				"mobile-monetizer.com", "mobilemonetizer.net", "moccini.com", "moreabout.online", "mountainbrowse.com",
				"mountainsecurity.net", "movie-box.online", "movie-browse.com", "movie-canal.com", "movie-rank.com",
				"moviebox-online.com", "moviegoat.com", "moviehubdb.com", "musi-cally.com", "music-jar.com",
				"musiconlinepro.com", "musictabz.com", "muzic.io", "muzixstream.com", "my-newtab.com",
				"my-search-portal.com", "mymisr5.com", "mysporttab.com", "myvideotab.com", "navisurf.com",
				"navnsafe.com", "net-speed-test.com", "newtab.online", "nicetohave.world", "notify-service.com",
				"nroll.com", "obosearch.com", "online-adnetwork.com", "online-live-streaming.com", "oquro.com",
				"osxappdownload.com", "otadigital.com", "package-tracer.com", "pdf-maker.com", "pdfpros.com",
				"pgcollect.com", "pinwid.com", "piytrwd.com", "playliveradio.com", "playpacmen.com", "polimva.com",
				"portmdfmoon.com", "private-stream.com", "prospeedcheck.com", "protected-search.com", "protectium.com",
				"pxcollect.com", "qotura.com", "qpgriefi.com", "quick-converter.com", "radiostream.online",
				"raphaelsearch.com", "resoftcorp.com", "rgbcjfir.com", "rocketsads.com", "run-speed-check.com",
				"runspeedcheck.com", "rutiyo.me", "sadooma.com", "safeffinder.com", "safeffinder.info",
				"safefinder.biz", "safefinder.co", "safefinder.com", "safefinder.info", "safefinder.me",
				"safefinder.online", "safefinder.org", "safefinderfaq.com", "safefindermalware.com", "safefinders.com",
				"safefindersupport.com", "safefinderuninstall.com", "safefindervirus.com", "safensearch.com",
				"salah-time.com", "salah.live", "salahweb.com", "salodo.com", "score-stars.com", "scoresportal.com",
				"searchallweb.com", "searchboxlive.com", "searchemoji.online", "searchfrit.com", "seedsandpeels.com",
				"sendmepixel.com", "shieldads.com", "shop-it.online", "shopanoffer.com", "shopping-day.com",
				"shopthisdeal.com", "showmoreabout.com", "showstogo.today", "shuzzy.com", "side-do.com",
				"sidecubes.com", "sidecubes.info", "signaly.co", "snapdo.be", "snapdo.cc", "snapdo.com",
				"snapdo.com.co", "snapdo.cz", "snapdo.es", "snapdo.fm", "snapdo.fr", "snapdo.it", "snapdo.ms",
				"snapdo.net", "snapdo.org", "snapdo.ws", "snapdobar.com", "snapdoit.com", "snapdotool.com",
				"snapdowidget.com", "snapscreen.online", "snowbitt.com", "song-stream.com", "sonic-search.com",
				"sound-hd.com", "soundrad.net", "specialtab.com", "speed-exam.com", "speed-net-test.com",
				"speedomizer.com", "spinit.online", "splintersearch.com", "sportwatch.online", "stream-all.com",
				"stream-beat.com", "stream-it.online", "stream-me.com", "stream24hour.com", "stream24now.com",
				"streamall4k.com", "streamfrenzy.com", "streaming-time.com", "streamingworldcup.com",
				"streamit-online.com", "streamit4k.com", "streams-it.online", "streamymusic.com",
				"supportextension.com", "surfioapp.com", "surfisafe.com", "swanow.com", "swellvpn.com", "tabsmania.com",
				"tabzmania.com", "tagadin.com", "tapufind.com", "techappnetwork.com", "techappworlds.com",
				"thecooking.club", "thefashionroom.co.uk", "thegamesearcher.com", "thenewsnotifications.com",
				"thevideowiz.com", "thevrtubes.com", "thezeebusiness.com", "topradiostationsonline.com",
				"tracknfollow.com", "tv-tunnel.com", "uhbnji.com", "uhbvgy.com", "uhbygv.com", "ujmnhy.com",
				"uninstallsafefindercom-1299.com", "utili-site.com", "utilitool.co", "utilitooltech.com",
				"utyuytjn.com", "veristaff.com", "veristuff.com", "video-browse.com", "videoconverterz.com",
				"volfind.com", "vrvrvrapp.com", "vudusearch.com", "wallpapers-mania.com", "weather-genie.com",
				"weathertabs.com", "webcoapps.com", "webnotfound.com", "webworldway.com", "wizesearch.com",
				"ygcvbn.com", "yhnbgt.com", "yhnmju.com", "ynetexplore.com", "zoomifyapp.com", "zoomifyme.com",
				"zxcdsa.com", "downchecker.com", "eazymount.com", "exomode.com", "badolina.com", "grbit.link",
				"baboom.audio", "wiki-search.me", "dealsquib.com", "jellyhop.com", "amazingtab.com", "bobydeals.com",
				"advertisetoday.online", "advertisenowonline.com", "webtracky.com", "viralizeit.net", "prizebuzz.me",
				"prize4buzz.com", "snapbuzz.me", "musicdiscoverytab.com", "foodiegram.net", "mothervista.com",
				"fupstick.com", "safeweb.co", "dailyvid.co", "privacy-search.com", "searchenhanced.co",
				"searchalink.com", "relatedsearchlinks.com", "appslift.com" });

		FindThreatMatchesRequest findThreatMatchesRequest = createFindThreatMatchesRequest(urls);

		Safebrowsing.Builder safebrowsingBuilder = new Safebrowsing.Builder(httpTransport, GOOGLE_JSON_FACTORY, null)
				.setApplicationName(GOOGLE_APPLICATION_NAME);
		Safebrowsing safebrowsing = safebrowsingBuilder.build();
		FindThreatMatchesResponse findThreatMatchesResponse = safebrowsing.threatMatches()
				.find(findThreatMatchesRequest).setKey(GOOGLE_API_KEY).execute();

		List<ThreatMatch> threatMatches = findThreatMatchesResponse.getMatches();

		if (threatMatches != null && threatMatches.size() > 0) {
			for (ThreatMatch threatMatch : threatMatches) {
				System.out.println(String.format(
						"The Url: %s, was marked as site with harmful programs. The threat type from Safe Browse API is: %s",
						threatMatch.getThreat().getUrl(), threatMatch.getThreatType()));
			}
		}
	}

	private static FindThreatMatchesRequest createFindThreatMatchesRequest(List<String> urls) {
		FindThreatMatchesRequest findThreatMatchesRequest = new FindThreatMatchesRequest();

		ClientInfo clientInfo = new ClientInfo();
		clientInfo.setClientId(GOOGLE_CLIENT_ID);
		clientInfo.setClientVersion(GOOGLE_CLIENT_VERSION);
		findThreatMatchesRequest.setClient(clientInfo);

		ThreatInfo threatInfo = new ThreatInfo();
		threatInfo.setThreatTypes(GOOGLE_THREAT_TYPES);
		threatInfo.setPlatformTypes(GOOGLE_PLATFORM_TYPES);
		threatInfo.setThreatEntryTypes(GOOGLE_THREAT_ENTRYTYPES);

		List<ThreatEntry> threatEntries = new ArrayList<ThreatEntry>();

		for (String url : urls) {
			ThreatEntry threatEntry = new ThreatEntry();
			threatEntry.set("url", url);
			threatEntries.add(threatEntry);
		}
		threatInfo.setThreatEntries(threatEntries);
		findThreatMatchesRequest.setThreatInfo(threatInfo);

		return findThreatMatchesRequest;
	}

}
