package testCaseChecker;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;

public class getOptions {

	private static Options options;
	private static CommandLineParser cmdParser;
	private static CommandLine cmdLine;

	private static Options argumantOptions() {
		options = new Options();
		options.addOption("configPath", true, "");
		return options;
	}

	public static String getArgumanet(String[] args) {
		cmdParser = new DefaultParser();
		options = argumantOptions();
		try {
			cmdLine = cmdParser.parse(options, args, true);
			if (cmdLine.hasOption("configPath")) {
				return cmdLine.getOptionValue("configPath");
			}
		} catch (Exception e) {
			System.out.println("the correct way to run the test is: 'java -jar NAME_OF_JAR.jar -configPath PASSED_PARAM' ");
		}
		return null;
	}
}