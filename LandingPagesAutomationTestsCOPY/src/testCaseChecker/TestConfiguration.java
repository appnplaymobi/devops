package testCaseChecker;

import com.microsoft.sqlserver.jdbc.StringUtils;

public class TestConfiguration {

	public String ProductId;
	public String Provider;
	public String Domain;
	public String PID;
	public String Hours;
	public String CampaignsLimit;

	public String getProductId() {
		if (StringUtils.isEmpty(ProductId)) {
			return "";
		}

		return ProductId;
	}

	public int getProvider() {
		if (StringUtils.isEmpty(Provider)) {
			return 1;
		}

		return Integer.valueOf(Provider);
	}

	public String getDomain() {
		if (StringUtils.isEmpty(Domain)) {
			return "%install.%";
		}

		return Domain;
	}

	public String getPid() {
		if (StringUtils.isEmpty(PID)) {
			return "";
		}

		return PID;
	}

	public int getHours() {
		if (StringUtils.isEmpty(Hours)) {
			return 0;
		}

		return Integer.valueOf(Hours);
	}

	public String getBinaLimit() {
		if (StringUtils.isEmpty(CampaignsLimit)) {
			return "";
		}

		return CampaignsLimit;
	}
}