package testCaseChecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

public class ParsingJson {
	private static final Gson gson = new Gson();

	private static String argumentValidation(String argument) throws MalformedURLException, IOException {
		if (argument.toLowerCase().contains("http")) {
			return readUrl(argument);
		} else {
			if (argument == null || Files.notExists(Paths.get(argument), LinkOption.NOFOLLOW_LINKS)) {
				System.out.println("parameters are missing");
			} else {
				return new String(Files.readAllBytes(Paths.get(argument)));
			}
		}
		return null;
	}

	private static boolean jsonAsStringValidation(String content) throws IOException {
		try {
			new JsonParser().parse(content);
			return true;
		} catch (JsonParseException e) {
			System.out.println("Invalid json content");
			return false;
		}
	}

	private static BufferedReader getBufferedReader(String urlToExtractContent)
			throws MalformedURLException, IOException {
		URL url = new URL(urlToExtractContent);
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		return in;
	}

	private static String readUrl(String url) throws MalformedURLException, IOException {
		String jsonString = "";
		try {
			BufferedReader in = getBufferedReader(url);
			String line = null;
			while ((line = in.readLine()) != null) {
				jsonString += line;
			}
			in.close();
		} catch (Exception e) {
		}
		return jsonString;
	}

	public static TestOptions mapJsonParams(String url) {
		TestOptions jsonObj = null;
		try {
			String jsonAsString = argumentValidation(url);
			if (jsonAsString == null) {
				System.out.println("cannot parse the json content");
			} else if (jsonAsStringValidation(jsonAsString)) {
				jsonAsString = jsonAsString.replaceAll("<.*?>|\u00a0", "");
				jsonObj = gson.fromJson(jsonAsString, TestOptions.class);
			}
		} catch (Exception e) {
			// TBD send message to slack regarding the exception - not valid json string
			e.printStackTrace();
		}
		return jsonObj;
	}
}