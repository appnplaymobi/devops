package testCaseChecker;

public class TestOptions {

	public String TestName;
	public TestConfiguration TestConfiguration;

	public String getTestName() {
		return TestName;
	}

	public TestConfiguration getTestConfiguration() {
		return TestConfiguration;
	}
}