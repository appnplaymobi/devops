package mailResults;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import externalFiles.ManagingXML;
import utilities.Time;

public class MailContent {

	private Time util;

	public MailContent(Time util) {
		this.util = util;
	}

	public String getReportPathInAsure() throws ParserConfigurationException, SAXException, IOException {
		String retpotPath = ManagingXML.getDataFromXML("azureReportFolder");
		String todayDate = "/reports/LP_Reports/" + util.returnTodayDate(); 
		String fileName = ManagingXML.getDataFromXML("reportName");

		String pathToReport = retpotPath + todayDate + fileName;
		return pathToReport;
	}

	public String setContentSuccessTest() throws ParserConfigurationException, SAXException, IOException {
		String asureReportPath = getReportPathInAsure();
		String finalUrl = "<a href=" + asureReportPath + ">" + asureReportPath + "</a>";
		String mesageContent = setMessageColor("<b>Landing Pages test result: </b> ", "black") + finalUrl;
		return mesageContent;

	}

	public String setMessageColor(String message, String color) {
		String MessageColor = "<font color=" + color + ">" + message + "</font>";
		return MessageColor;
	}
	
	public String setContentMessageToSlack(String testName) throws ParserConfigurationException, SAXException, IOException {
		String asureReportPath = getReportPathInAsure();
		String mesageContent = String.format("Landing Pages test result for: %s. \n %s", testName, asureReportPath) ;
		return mesageContent;
	}
}