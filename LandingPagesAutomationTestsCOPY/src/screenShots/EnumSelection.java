package screenShots;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import utilities.FilesAndPathManager;

public class EnumSelection {
	private TestLocation testLocation;
	private FilesAndPathManager pathTo;
	
	public EnumSelection(TestLocation testLocation, FilesAndPathManager pathTo) {
		this.testLocation = testLocation;
		this.pathTo = pathTo;
	}

	public String getLocalLocationForTest() throws ParserConfigurationException, SAXException, IOException {
		switch (testLocation) {
		case BINA:
			return pathTo.getLocalBigQueryResultsFolder();
		case LP:
			return pathTo.getLpLocalLPresultsFolder();
		default:
			return null;
		}
	}

	public String getAzureLocationForTest() throws ParserConfigurationException, SAXException, IOException {
		switch (testLocation) {
		case BINA:
			return pathTo.getAzureBigQueryResultsFolder();
		case LP:
			return pathTo.getAzureLPresultsFolder();
		default:
			return null;
		}
	}
}