package dataBaseClasess.dbTools;

import java.io.IOException;
import java.sql.Connection;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import dataBaseClasess.bigQuery.SQLdata;
import externalFiles.ManagingXML;

public class SetConnectionStringToSqlDb {
	
	public static Connection getConnectionString() throws ParserConfigurationException, SAXException, IOException {
		String cConnectionString = ManagingXML.getDataFromXML("dbConnectionString");
		SQLdata sqlData = new SQLdata(cConnectionString);
		return (Connection) sqlData;
	}

}
