package dataBaseClasess.bigQuery;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.google.cloud.bigquery.JobException;
import externalFiles.ManagingXML;

public class RetriveDataResults {

	private SQLdata sqlData;
	private BigQueryData bigData;

	private List<String> getActivePids(String limit) throws FileNotFoundException, JobException, IOException,
			ParserConfigurationException, SAXException, InterruptedException {

		bigData = new BigQueryData();
		List<Map<String, String>> data = bigData.getActiveCampaigns(limit);
		List<String> allPids = new ArrayList<String>();

		for (Map<String, String> row : data) {
			allPids.add(row.get("PID"));
		}
		return allPids;
	}

	private SQLdata getConnectionString() throws ParserConfigurationException, SAXException, IOException {
		String connectionString = ManagingXML.getDataFromXML("dbConnectionString");
		sqlData = new SQLdata(connectionString);
		return sqlData;
	}

	public List<String> createUrlToTest(String limit) throws FileNotFoundException, JobException, IOException,
			ParserConfigurationException, SAXException, InterruptedException {

		List<String> pids = getActivePids(limit);
		sqlData = getConnectionString();

		List<String> collectUrls = new ArrayList<String>();
		List<String> urlsFromSQL = sqlData.getUrlsByPids(pids);
		for (String singleUrl : urlsFromSQL) {
			collectUrls.add(singleUrl);
		}
		return collectUrls;
	}
}