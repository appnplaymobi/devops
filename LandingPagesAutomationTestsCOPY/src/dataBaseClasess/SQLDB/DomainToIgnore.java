package dataBaseClasess.SQLDB;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DomainToIgnore {
	
	public static List<String> domainList() {
		List<String> ignoreList = new ArrayList<String>(Arrays.asList("stream24hour.com", "streamingworldcup.com"));
		return ignoreList;
	}
}