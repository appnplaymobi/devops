package handleUrl;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import com.relevantcodes.extentreports.LogStatus;

import drivers.UsingDriver;
import report.reportManager;

public class checkUrlAndOpenConnection {

	private reportManager report;
	private UsingDriver driver;

	public checkUrlAndOpenConnection(reportManager report, UsingDriver driver) throws MalformedURLException {
		this.report = report;
		this.driver = driver;
	}

	public boolean ifUnknownSource(String urlToCheck) throws IOException {
		try {
			URL u = new URL(urlToCheck);
			HttpURLConnection huc = (HttpURLConnection) u.openConnection();
			huc.setRequestMethod("GET");
			huc.connect();
			huc.getResponseMessage();
			return true;
		} catch (Exception e) {
			report.getTest().log(LogStatus.FAIL, String
					.format("The Url: <b>%s</b> cannot be tested. The error is: <b>%s</b>", urlToCheck, e.getMessage()));
			return false;
		}
	}
	
	public boolean pageReturns404(String url) {
		String pageTitle = driver.getPageTitle(url);
		if (!pageTitle.contains("Page Not Found")) {
			return false; // not 404
		}
		report.getTest().log(LogStatus.FAIL,
				String.format("The Url: <b>%s</b> cannot be tested. It returns <b>404 Page Not Found</b>", url));
		return true;// 404
	}
	
	public String connection(String url) throws IOException {
		String response = "";
		URL urlToConnect = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) urlToConnect.openConnection();
		connection.connect();
		response = connection.getResponseMessage();
		connection.disconnect();
		return response;
	}
	
	
}