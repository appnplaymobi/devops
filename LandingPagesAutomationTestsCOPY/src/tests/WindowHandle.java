package tests;

import drivers.UsingDriver;

public interface WindowHandle {

	public void winHandle(UsingDriver driver, String windowName);
}