package tests;

import java.net.MalformedURLException;
import com.relevantcodes.extentreports.LogStatus;
import drivers.UsingDriver;
import report.reportManager;

public class CreativeName {

	private UsingDriver driver;
	private reportManager report;

	public CreativeName(UsingDriver driver, reportManager report) throws MalformedURLException {
		this.driver = driver;
		this.report = report;
	}

	public boolean ifJsValueEquals(String jsToCheck, String jsToExtract) throws MalformedURLException {
		String exctractedJsValue = driver.extractJsValueFromPage(jsToExtract);
		boolean result = jsToCheck.equals(exctractedJsValue);
		if (!result) {
			report.getTest().log(LogStatus.FAIL, String.format(
					"The extracted creative number: <b>%s</b> in the page, did not mach the creative number in the DB: <b>%s</b> ",
					exctractedJsValue, jsToCheck));
		} else {
			report.getTest().log(LogStatus.PASS, String.format(
					"The extracted creative number: <b>%s</b> mach the creative number in the DB", exctractedJsValue));
		}
		return result;
	}

	public void ifCreativeNameEqual(String creativeNameToCheck) throws MalformedURLException {
		ifJsValueEquals(creativeNameToCheck, "creativenumber");
	}
}