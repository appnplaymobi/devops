package tests;

import java.net.MalformedURLException;
import java.util.List;
import org.openqa.selenium.WebElement;
import com.relevantcodes.extentreports.LogStatus;
import report.reportManager;
import webElementsLocators.ElementsInPage;

public class Click {

	private ElementsInPage element;
	private reportManager report;

	public Click(ElementsInPage element, reportManager report) throws MalformedURLException {
		this.element = element;
		this.report = report;
	}

	public WebElement findHowToLink() {
		return element.elementByClassName("how-to-link");
	}

	public WebElement findAddToChrome() {
		return element.elementByClassName("AddToBrowser");
	}

	public WebElement getClickableAddToChrome() throws InterruptedException {
		List<WebElement> elements = element.elementsByCssSelector(".AddToBrowser");
		if (elements.equals(null)) {
			return null;
		}
		for (WebElement element : elements) {
			if (element.isDisplayed() && element.isEnabled()) {
				return element;
			}
		}
		return null;
	}

	public boolean waitForHowToLink() {
		if (element.waitByClassName("how-to-link") != null) {
			return true;
		}
		return false;
	}

	public void clickOnAddToChromeBtn() {
		boolean howItWorksMsgDisplay = waitForHowToLink();
		try {
			if (howItWorksMsgDisplay) {
				findHowToLink().click();
				element.scrollIntoView();
				findAddToChrome().click();
			} else {
				getClickableAddToChrome().click();
			}
		} catch (Exception e) {
			report.getTest().log(LogStatus.FATAL, "install button is not clickeble");
		}
	}
}