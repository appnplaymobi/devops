package report;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.conn.util.PublicSuffixMatcher;
import org.apache.http.conn.util.PublicSuffixMatcherLoader;

public class UrlAppearanceInReport {

	private static PublicSuffixMatcher _publicSuffixMatcher;

	public UrlAppearanceInReport() {
		_publicSuffixMatcher = PublicSuffixMatcherLoader.getDefault();
	}

	private Map<String, String> hashMapQuery(String query) {
		Map<String, String> result = new HashMap<String, String>();
		if (query != null) {
			String[] allQuery = query.split("&");
			for (String singleQueryString : allQuery) {
				String[] pair = singleQueryString.split("=");

				String val = null;
				if (pair.length == 2) {
					val = pair[1];
				}
				result.put(pair[0], val);
			}
		}
		return result;
	}

	public String sqlTestName(String url) throws MalformedURLException {
		URL aURL = new URL(url);
		String query = aURL.getQuery();
		Map<String, String> map = hashMapQuery(query);
		String finalName = getDomainName(url) + "-" + map.get("c");
		return finalName.replace("%20", "");
	}
	
	public String binaTestName(String url) throws MalformedURLException {
		String finalName = getDomainName(url);
		return finalName.replace("%20", "");
	}

	public String clickableUrlInReport(String url) throws MalformedURLException {
		String openInNewTab = " target=" + '"' + "_blank" + '"';
		url = url.replace(" ", "%20");
		String finalLink = String.format("<a href=%s%s<b>%s</b></a>", url, openInNewTab, url);
		return finalLink;
	}

	public String getDomainName(String url) throws MalformedURLException {
		String domainName = _publicSuffixMatcher.getDomainRoot(getHostUrl(url));
		return domainName;
	}

	private String getHostUrl(String url) throws MalformedURLException {
		URL aURL = new URL(url);
		String domainName = aURL.getHost();
		return domainName;
	}
}