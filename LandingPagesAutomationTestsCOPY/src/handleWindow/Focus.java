package handleWindow;

import java.awt.AWTException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Set;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.WebDriver;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import com.relevantcodes.extentreports.LogStatus;
import drivers.UsingDriver;
import report.GrabScreenShot;
import report.reportManager;
import screenShots.TestLocation;
import webElementsLocators.ElementsInPage;

public class Focus {
	
	private TestLocation testLocation;
	private reportManager report;
	private ElementsInPage element;
	private UsingDriver driver;
	private GrabScreenShot take;

	public Focus(UsingDriver driver, ElementsInPage element, GrabScreenShot take, reportManager report, TestLocation testLocation) {
		this.driver = driver;
		this.element = element;
		this.take = take;
		this.report = report;
		this.testLocation = testLocation;
	}

	public void takeChromeStoreScreenShot(String description) throws MalformedURLException, IOException,
			ParserConfigurationException, SAXException, URISyntaxException, AWTException, StorageException {
		String originalWindowHandle = driver.getDriver().getWindowHandle();
		WebDriver popup = null;
		Set<String> windowsList = driver.getDriver().getWindowHandles();

		for (String window : windowsList) {
			if (!window.equals(originalWindowHandle)) {
				popup = driver.getDriver().switchTo().window(window);
				if (popup.getTitle().contains("Chrome Web Store")) {
					element.waitByClassName("e-f-pa");
					take.scrrenShot(testLocation, description);
					popup.close();
				} else {
					String chromeStoreTitle = driver.getDriver().getTitle();
					take.scrrenShot(testLocation,chromeStoreTitle);
					popup.close();
					report.getTest().log(LogStatus.FAIL,
							String.format("The chrome store open with: <b>%s</b>", chromeStoreTitle));
				}
			}
		}
		driver.getDriver().switchTo().window(originalWindowHandle);
	}
}