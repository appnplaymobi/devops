package mainPkgLPautomation;

import java.awt.AWTException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.google.cloud.bigquery.JobException;
import com.microsoft.azure.storage.StorageException;
import drivers.UsingDriver;
import handleUrl.checkUrlAndOpenConnection;
import handleWindow.Focus;
import report.Azure;
import report.GrabScreenShot;
import report.UrlAppearanceInReport;
import report.reportManager;
import screenShots.EnumSelection;
import screenShots.TestLocation;
import slack.SlackManager;
import testCaseChecker.ParsingJson;
import testCaseChecker.TestConfiguration;
import testCaseChecker.TestOptions;
import testCases.LPBina;
import testCases.LPSql;
import tests.BrokenLinksInPage;
import tests.Click;
import tests.MissingResourceInPage;
import tests.PageLoad;
import tests.TestPgSegment;
import utilities.FilesAndPathManager;
import utilities.Time;
import webElementsLocators.ElementsInPage;

public class RunLPAutomationTest {

	public static void main(String[] args)
			throws ParserConfigurationException, SAXException, IOException, InvalidKeyException, URISyntaxException,
			StorageException, AWTException, JobException, InterruptedException {

		UsingDriver driver = new UsingDriver();
		Time time = new Time();
		FilesAndPathManager pathManager = new FilesAndPathManager(time);
		pathManager.createLocalReportFolders();
		Azure azure = new Azure(pathManager);
		azure.initAzureConnection();
		ElementsInPage element = new ElementsInPage(driver);
		SlackManager slack = new SlackManager();
		UrlAppearanceInReport extractedUrl = new UrlAppearanceInReport();
		// end of general declarations

		reportManager reportResultsFromBigQuery = new reportManager(pathManager.getPathToLocalBigQueryResultsFile());
		PageLoad getPageLoadUrlsFromBigQuery = new PageLoad(reportResultsFromBigQuery);
		EnumSelection enumSelectionBina = new EnumSelection(TestLocation.BINA, pathManager);
		GrabScreenShot grabScreenShotUrlsFromBigQuery = new GrabScreenShot(reportResultsFromBigQuery, time, azure,
				enumSelectionBina);
		checkUrlAndOpenConnection urlValidationUrlsBigQuery = new checkUrlAndOpenConnection(reportResultsFromBigQuery,
				driver);
		BrokenLinksInPage testBrokenLinkstUrlsFromBigQuery = new BrokenLinksInPage(element, reportResultsFromBigQuery,
				urlValidationUrlsBigQuery);
		MissingResourceInPage testMissingResourceInPageUrlsFromBigQuery = new MissingResourceInPage(driver,
				reportResultsFromBigQuery);
		Click clickOnAddToChromeUrlsFromBigQuery = new Click(element, reportResultsFromBigQuery);

		Focus setFocusOnChromeStoreWindowUrlsFromBigQuery = new Focus(driver, element, grabScreenShotUrlsFromBigQuery,
				reportResultsFromBigQuery, TestLocation.BINA);
		TestPgSegment testPgSegment = new TestPgSegment(driver, reportResultsFromBigQuery);

		LPBina testBigQueryResults = new LPBina(driver, time, getPageLoadUrlsFromBigQuery,
				grabScreenShotUrlsFromBigQuery, testBrokenLinkstUrlsFromBigQuery,
				testMissingResourceInPageUrlsFromBigQuery, clickOnAddToChromeUrlsFromBigQuery,
				setFocusOnChromeStoreWindowUrlsFromBigQuery, reportResultsFromBigQuery, azure, slack, pathManager,
				extractedUrl, testPgSegment);
		// end of big query links test

		reportManager reportResultsFromSQL = new reportManager(pathManager.getPathToLocalSQLResultsFile());
		PageLoad getPageLoadUrlsFromSQL = new PageLoad(reportResultsFromSQL);
		EnumSelection enumSelectionSQL = new EnumSelection(TestLocation.LP, pathManager);
		GrabScreenShot grabScreenShotUrlsFromSQL = new GrabScreenShot(reportResultsFromSQL, time, azure,
				enumSelectionSQL);
		checkUrlAndOpenConnection urlValidationUrlsFromSQL = new checkUrlAndOpenConnection(reportResultsFromSQL,
				driver);
		BrokenLinksInPage testBrokenLinkstUrlsFromSQL = new BrokenLinksInPage(element, reportResultsFromSQL,
				urlValidationUrlsFromSQL);
		MissingResourceInPage testMissingResourceInPageUrlsFromSQL = new MissingResourceInPage(driver,
				reportResultsFromSQL);
		Click clickOnAddToChromeUrlsFromSQL = new Click(element, reportResultsFromSQL);
		Focus setFocusOnChromeStoreWindowUrlsFromSQL = new Focus(driver, element, grabScreenShotUrlsFromSQL,
				reportResultsFromSQL, TestLocation.LP);

		LPSql testSQLResults = new LPSql(driver, time, getPageLoadUrlsFromSQL, grabScreenShotUrlsFromSQL,
				testBrokenLinkstUrlsFromSQL, testMissingResourceInPageUrlsFromSQL, clickOnAddToChromeUrlsFromSQL,
				setFocusOnChromeStoreWindowUrlsFromSQL, reportResultsFromSQL, extractedUrl, urlValidationUrlsFromSQL,
				azure, slack, pathManager);

		String CustomLPs = "https://linktestfiles.blob.core.windows.net/qa-reports/lp-test-files/CustomLPs.json";
		String BinaTest = "https://linktestfiles.blob.core.windows.net/qa-reports/lp-test-files/BinaTest.json";
		String DailyTest = "https://linktestfiles.blob.core.windows.net/qa-reports/lp-test-files/DailyTest.json";
		String badJson = "https://linktestfiles.blob.core.windows.net/qa-reports/lp-test-files/DailyTestBadJson.json";
		
		TestOptions jsonObj = ParsingJson.mapJsonParams(args[1]);
		try {
			TestConfiguration testConfiguration = jsonObj.getTestConfiguration();
			if (jsonObj.getTestName().equals("BinaTest")) {
				System.out.println(testConfiguration.getBinaLimit());
				testBigQueryResults.testsForBina(TestLocation.BINA, testConfiguration.getBinaLimit());
			} else {
				testSQLResults.testsForLPSQL(TestLocation.LP, testConfiguration);
			}
		} catch (Exception e) {
			System.out.println("cannot test with given json");
		}

	}
}