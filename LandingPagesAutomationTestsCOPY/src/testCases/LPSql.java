package testCases;

import java.awt.AWTException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.google.cloud.bigquery.JobException;
import com.microsoft.azure.storage.StorageException;
import dataBaseClasess.SQLDB.DBSelection;
import dataBaseClasess.SQLDB.DomainToIgnore;
import dataBaseClasess.dbTools.SqlDataParserForSQL;
import drivers.UsingDriver;
import externalFiles.ManagingXML;
import handleUrl.checkUrlAndOpenConnection;
import handleWindow.Focus;
import report.Azure;
import report.GrabScreenShot;
import report.UrlAppearanceInReport;
import report.reportManager;
import screenShots.TestLocation;
import slack.SlackManager;
import testCaseChecker.TestConfiguration;
import tests.BrokenLinksInPage;
import tests.Click;
import tests.MissingResourceInPage;
import tests.PageLoad;
import utilities.FilesAndPathManager;
import utilities.Time;

public class LPSql {

	private reportManager report;
	private UsingDriver driver;
	private Time time;
	private PageLoad getPageLoad;
	private GrabScreenShot grabScreenShot;
	private BrokenLinksInPage testBrokenLinks;
	private MissingResourceInPage missingResourceInPage;
	private Click click;
	private Focus setFocusOnChromeStoreWindow;
	private checkUrlAndOpenConnection urlValidation;
	private UrlAppearanceInReport urlInReport;
	private Azure azureUtils;
	private SlackManager slack;
	private FilesAndPathManager pathTo;

	public LPSql(UsingDriver driver, Time time, PageLoad getPageLoad, GrabScreenShot grabScreenShot,
			BrokenLinksInPage testBrokenLinks, MissingResourceInPage missingResourceInPage, Click click,
			Focus setFocusOnChromeStoreWindow, reportManager report, UrlAppearanceInReport urlInReport,
			checkUrlAndOpenConnection urlValidation, Azure azureUtils, SlackManager slack, FilesAndPathManager pathTo)
			throws ParserConfigurationException, SAXException, IOException {

		this.driver = driver;
		this.time = time;
		this.report = report;
		this.getPageLoad = getPageLoad;
		this.grabScreenShot = grabScreenShot;
		this.testBrokenLinks = testBrokenLinks;
		this.missingResourceInPage = missingResourceInPage;
		this.urlInReport = urlInReport;
		this.click = click;
		this.setFocusOnChromeStoreWindow = setFocusOnChromeStoreWindow;
		this.urlValidation = urlValidation;
		this.slack = slack;
		this.pathTo = pathTo;
		this.azureUtils = azureUtils;
	}

	public void testsForLPSQL(TestLocation testLocation, TestConfiguration testConf)
			throws ParserConfigurationException, SAXException, IOException, URISyntaxException, AWTException,
			StorageException, JobException, InterruptedException {

		String dbConnectionString = ManagingXML.getDataFromXML("dbConnectionString");
		DBSelection query = new DBSelection(dbConnectionString);

		List<String> domainToIgnoreList = DomainToIgnore.domainList();
		Map<String, SqlDataParserForSQL> map = query.select(testConf);

		driver.initDriver();
		for (Entry<String, SqlDataParserForSQL> entry : map.entrySet()) {
			String url = entry.getKey();
			System.out.println(url);
//			report.initTestCase(urlInReport.sqlTestName(url), urlInReport.clickableUrlInReport(url));
//			String ignoreDomain = urlInReport.getDomainName(url);
//			boolean pageCanBeTested = urlValidation.ifUnknownSource(url);
//			if (pageCanBeTested && !domainToIgnoreList.contains(ignoreDomain)) {
//				long pageLoadStart = time.startNanoTime();
//				System.out.println(String.format("Working on: %s", url));
//				driver.launchPage(url);
//				driver.waitForPageToLoad();
//				long pageLoadEnd = time.endNanoTime();
//				getPageLoad.reportPageLoad(pageLoadStart, pageLoadEnd);
//				grabScreenShot.scrrenShot(TestLocation.LP, "main page screenshot");
//				testBrokenLinks.checkLinksInPage("a", "href");
//				testBrokenLinks.checkLinksInPage("img", "src");
//				missingResourceInPage.testMissingResourceInPage(url);
//				click.clickOnAddToChromeBtn();
//				setFocusOnChromeStoreWindow.takeChromeStoreScreenShot("chrome web store");
//				driver.deleteCookeisAndLogs();
//				report.endTestReport();
//				report.flushTest();
//			}
		}
		driver.quitDriver();
//		azureUtils.uploadLPTest();
//		slack.postMassage("Click here for test results of urls from the SQL", pathTo.getLinkToSQLreport());
	}
}