package testCases;

import java.awt.AWTException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.google.cloud.bigquery.JobException;
import com.microsoft.azure.storage.StorageException;
import drivers.UsingDriver;
import handleWindow.Focus;
import report.Azure;
import report.GrabScreenShot;
import report.UrlAppearanceInReport;
import report.reportManager;
import screenShots.TestLocation;
import slack.SlackManager;
import dataBaseClasess.bigQuery.RetriveDataResults;
import tests.BrokenLinksInPage;
import tests.Click;
import tests.MissingResourceInPage;
import tests.PageLoad;
import tests.TestPgSegment;
import utilities.FilesAndPathManager;
import utilities.Time;

public class LPBina {

	private reportManager report;
	private UsingDriver driver;
	private Time time;
	private PageLoad getPageLoad;
	private GrabScreenShot grabScreenShot;
	private BrokenLinksInPage testBrokenLinks;
	private MissingResourceInPage missingResourceInPage;
	private Click click;
	private Focus setFocusOnChromeStoreWindow;
	private Azure azureUtils;
	private SlackManager slack;
	private FilesAndPathManager pathTo;
	private UrlAppearanceInReport urlInReport;
	private TestPgSegment testPgSegment;

	public LPBina(UsingDriver driver, Time time, PageLoad getPageLoad, GrabScreenShot grabScreenShot,
			BrokenLinksInPage testBrokenLinks, MissingResourceInPage missingResourceInPage, Click click,
			Focus setFocusOnChromeStoreWindow, reportManager report, Azure azureUtils, SlackManager slack,
			FilesAndPathManager pathTo, UrlAppearanceInReport urlInReport, TestPgSegment testPgSegment)
			throws ParserConfigurationException, SAXException, IOException {

		this.driver = driver;
		this.time = time;
		this.report = report;
		this.getPageLoad = getPageLoad;
		this.grabScreenShot = grabScreenShot;
		this.testBrokenLinks = testBrokenLinks;
		this.missingResourceInPage = missingResourceInPage;
		this.click = click;
		this.setFocusOnChromeStoreWindow = setFocusOnChromeStoreWindow;
		this.slack = slack;
		this.pathTo = pathTo;
		this.azureUtils = azureUtils;
		this.urlInReport = urlInReport;
		this.testPgSegment = testPgSegment;
	}

	public void testsForBina(TestLocation test, String limit) throws ParserConfigurationException, SAXException,
			IOException, URISyntaxException, AWTException, StorageException, JobException, InterruptedException {

		RetriveDataResults dataResults = new RetriveDataResults();
		List<String> allUrls = dataResults.createUrlToTest(limit);
		driver.initDriver();
		for (String url : allUrls) {
			System.out.println(String.format("testing: %s", url));
//			report.initTestCase(urlInReport.binaTestName(url), urlInReport.clickableUrlInReport(url));
//			long pageLoadStart = time.startNanoTime();
//			driver.launchPage(url);
//			driver.waitForPageToLoad();
//			long pageLoadEnd = time.endNanoTime();
//			getPageLoad.reportPageLoad(pageLoadStart, pageLoadEnd);
//			grabScreenShot.scrrenShot(TestLocation.BINA, "main page screenshot");
//			testBrokenLinks.checkLinksInPage("a", "href");
//			testBrokenLinks.checkLinksInPage("img", "src");
//			missingResourceInPage.testMissingResourceInPage(url);
//			click.clickOnAddToChromeBtn();
//			setFocusOnChromeStoreWindow.takeChromeStoreScreenShot("chrome web store");
//			testPgSegment.TestglobalParameterPgSegment(url);
//			driver.deleteCookeisAndLogs();
//			report.endTestReport();
//			report.flushTest();
		}
		driver.quitDriver();
//		azureUtils.uploadBinaTest();
//		slack.postMassage("Click here for test results of urls from the BigQuery", pathTo.getLinkToBigQueryReport());
	}
}