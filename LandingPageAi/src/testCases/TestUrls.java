package testCases;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.microsoft.azure.storage.StorageException;
import com.relevantcodes.extentreports.LogStatus;

import extentTools.Base;
import extentTools.ChromTools;
import extentTools.Tools;
import reports.Report;
import reports.ScrrenShotsAzure;

public class TestUrls extends Base{
	
	public static List<String> allTestedUrls() {

		List<String> urlToCheck = new ArrayList<String>();
		urlToCheck.add("http://testing.streamit-online.com/?pid=51548");
		urlToCheck.add("http://testing.365-stream.com/?pid=52896");
		urlToCheck.add("http://testing.utilitooltech.com/?pid=52286");
		urlToCheck.add("http://testing.mysporttab.com/?pid=52879");
		return urlToCheck;
	}
	
	public static void test() throws ParserConfigurationException, SAXException, IOException, InterruptedException, URISyntaxException, StorageException{
		
		List<String> allUrls = allTestedUrls();
		for (int i = 0; i<2; i++){
		for (String baseUrl : allUrls){
			ChromTools.getPage(baseUrl);
			long start = Tools.startNanoTime();
			String resultUrl = ChromTools.getPageUrl();
			ChromTools.cleanAllCookies();
			long endTime = Tools.endNanoTime();
			Report.initTestCase(ChromTools.getHostUrl(baseUrl), baseUrl);
			Tools.calculateResponcePageDuration(endTime, start);
			test.log(LogStatus.INFO, "The base url was redirected to: " + Tools.openUrlInNewTab(resultUrl));
			ScrrenShotsAzure.takeScrrenShot(resultUrl);
		}
		}
		Report.endTestReport();
		extentReport.flush();
		
	}

	

}
