package reports;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.microsoft.azure.storage.StorageException;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import extentTools.Base;
import extentTools.Tools;

public class Report extends Base {
	public static void initReport() throws ParserConfigurationException, SAXException, IOException, URISyntaxException, StorageException {
		String reportFilePath = reportFile();
		extentReport = new ExtentReports(reportFilePath);
	}

	public static String createTestName(String testName) {
		testName = "<b>" + testName + "</b>";
		return testName;
	}

	public static void initTestCase(String testName, String pageUrl) {
		String _testName = createTestName(testName);
		String _testUrl = "For the base url: " + Tools.openUrlInNewTab(pageUrl);
		test = extentReport.startTest(_testName, _testUrl);
	}

	public static void uploadReports()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		uploadHtmlReport();
	}

	public static void endTestReport() throws URISyntaxException, StorageException, IOException {
		endTestReport(test);
	}

	public static void endTestReport(ExtentTest testName) throws URISyntaxException, StorageException, IOException {
		extentReport.endTest(testName);
	}

	public static void uploadHtmlReport()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		String reportFilePathToload = reportFile();
		Path path = Paths.get(reportFilePathToload);
		String pathToUploadTo = _reportedFolderTest + Tools.returnTodayDate() + "/" + path.getFileName();
		ScrrenShotsAzure.uploadToAzure(pathToUploadTo, reportFilePathToload);
	}

	public static String reportFile() throws ParserConfigurationException, SAXException, IOException {
		String retpotPath = Tools.getDataFromXML("reportPath");
		String todayDate = Tools.returnTodayDate();
		String fileName = Tools.getDataFromXML("reportName");

		String pathToReport = retpotPath + _reportedFolderTest + todayDate + fileName;
		return pathToReport;
	}
}
