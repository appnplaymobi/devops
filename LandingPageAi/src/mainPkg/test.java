//package mainPkg;
//
//import java.io.IOException;
//import java.net.URISyntaxException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import javax.xml.parsers.ParserConfigurationException;
//import org.xml.sax.SAXException;
//
//import com.microsoft.azure.storage.StorageException;
//import com.relevantcodes.extentreports.LogStatus;
//
//import extentTools.Base;
//import extentTools.ChromTools;
//import extentTools.Tools;
//import reports.Report;
//import reports.ScrrenShotsAzure;
//
//public class test extends Base {
//
//	public static List<String> collectTestedUrls() {
//
//		List<String> urlToCheck = new ArrayList<String>();
//		urlToCheck.add("http://testing.streamit-online.com/?pid=51548");
//		urlToCheck.add("http://testing.365-stream.com/?pid=52896");
//		urlToCheck.add("http://testing.utilitooltech.com/?pid=52286");
//		urlToCheck.add("http://testing.mysporttab.com/?pid=52879");
//		return urlToCheck;
//	}
//
//	public static HashMap<String, List<String>> testUrls() throws URISyntaxException, StorageException, IOException,
//			ParserConfigurationException, SAXException, InterruptedException {
//		List<String> urls = test.collectTestedUrls();
//
//		HashMap<String, List<String>> map = new HashMap<String, List<String>>();
//		List<String> timeR = new ArrayList<String>();
//		for (int i = 0; i < 3; i++) {
//			for (String origUrl : urls) {
//
//				List<String> resultsUrls = new ArrayList<String>();
//
//				ChromTools.getPage(origUrl);
//				long start = Tools.startNanoTime();
//				String resultUrl = ChromTools.getPageUrl();
//				resultsUrls.add(resultUrl);
//				ChromTools.cleanAllCookies();
//				long endTime = Tools.endNanoTime();
//				timeR.add(Tools.calculateResponcePageDuration(endTime, start));
//				
//				for (String s : timeR){
//					System.out.println(s);
//				}
//				
//				if (!map.containsKey(origUrl) || map.get(origUrl) == null)
//					map.put(origUrl, new ArrayList<String>());
//
//				map.get(origUrl).addAll(resultsUrls);
//			}
//		}
//		return map;
//	}
//
//	public static void sss() throws URISyntaxException, StorageException, IOException, ParserConfigurationException,
//			SAXException, InterruptedException {
//		HashMap<String, List<String>> map = testUrls();
//
//		for (String name : map.keySet()) {
//
//			String key = name.toString();
//			Report.initTestCase(ChromTools.getHostUrl(key), key);
//			List<String> value = map.get(key);
//
//			for (String s : value) {
//				System.out.println(key + " " + s);
//
//				test.log(LogStatus.INFO, Tools.openUrlInNewTab(s));
//			}
//		}
//		Report.endTestReport();
//		extentReport.flush();
//		Report.uploadReports();
//	}
//}
