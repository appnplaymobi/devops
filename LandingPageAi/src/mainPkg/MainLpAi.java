package mainPkg;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;

import javax.xml.parsers.ParserConfigurationException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import extentTools.Base;
import extentTools.ChromTools;
import reports.Report;
import reports.ScrrenShotsAzure;
import testCases.TestUrls;


public class MainLpAi extends Base {

	@BeforeClass
	public static void startSession() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {
		ChromTools.initChrome();
		ScrrenShotsAzure.initAzureConnection();
		Report.initReport();

	}

	@AfterClass
	public static void endSession()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		driver.quit();
	}
	
	@Test
	public void test01() throws ParserConfigurationException, SAXException, IOException, InterruptedException, URISyntaxException, StorageException{
		TestUrls.test();

	}

}
