package extentTools;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogType;
import org.xml.sax.SAXException;

import com.relevantcodes.extentreports.LogStatus;

import extentTools.Tools;

public class ChromTools extends Base {

	public static WebDriver initChrome() throws ParserConfigurationException, SAXException, IOException {
		System.setProperty("webdriver.chrome.driver", Tools.getDataFromXML("driverPath"));
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		return driver;
	}
	
	public static void cleanAllCookies(){
		driver.manage().logs().get(LogType.BROWSER).getAll();
		driver.manage().deleteAllCookies();
		}
	
	public static WebDriver getPage(String pageUrl)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		driver.get(pageUrl);
		ChromTools.cleanAllCookies();
		return driver;
	}
	
	public static String getPageUrl() {
		String pageUrl = driver.getCurrentUrl();
		return pageUrl;
	}
	
	public static String getHostUrl(String url) throws MalformedURLException {
		URL aURL = new URL(url);
		String domainName = aURL.getHost();
		return domainName;
	}
	
	public static String getJsValue(String jsPageValue) {
		try {
			String creativeResult = ((JavascriptExecutor) driver).executeScript("return " + jsPageValue).toString();
			test.log(LogStatus.INFO, "The creative number value from the page is: : " + jsPageValue);
			return creativeResult;
		} catch (Exception e) {
			test.log(LogStatus.FATAL,
					"js value function exception: " + e);
		}
		return null;
	}

}
