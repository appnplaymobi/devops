package extentTools;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import java.util.Date;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.relevantcodes.extentreports.LogStatus;



public class Tools extends Base{
	
	public static String getDataFromXML(String elementName)
			throws ParserConfigurationException, SAXException, IOException {
		File fXmlFile = new File("D:/Bitbucket/devops/LandingPageAi/src/extentTools/LP_AI.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		
		NodeList element = doc.getElementsByTagName(elementName);
		Node firstItem = element.item(0);
		return firstItem.getTextContent();
	}

	public static long startNanoTime() throws IOException {
		long start = System.nanoTime();
		return start;
	}

	public static long endNanoTime() throws IOException {
		long end = System.nanoTime();
		return end;
	}
	
	public static void calculateResponcePageDuration(long endTime, long start) {
		long duration = endTime - start;
		double seconds = ((double) duration / 1000000000.0);
		String doubleToStr = String.valueOf(seconds);
		test.log(LogStatus.INFO, String.format("The response of the page took:  %s ", doubleToStr + " seconds"));
	}

	public static String returnTodayDate() {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		String todayDate = dateFormat.format(now);
		return todayDate;
	}

	public static void createTodayDir() {
		String todayDir = returnTodayDate();
		File dir = new File(todayDir);
		dir.mkdir();
	}
	
	public static String openUrlInNewTab(String urlPage) {
		urlPage = urlPage.replace(" ", "%20");
		String targetBlank = " target=" + '"' + "_blank" + '"';
		String finalUrl = "<a href=" + urlPage + targetBlank + ">" + urlPage + "</a>";
		return finalUrl;
	}
}