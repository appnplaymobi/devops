package extentTools;

import java.sql.Connection;

import org.openqa.selenium.WebDriver;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class Base {
	
	// Reports
	public static ExtentReports extentReport;
	public static ExtentTest test;
	static String _reportedFolderProd = "reports/LpAi/Prod/";
	protected static String _reportedFolderTest = "reports/LpAi/Test/";
	// Azure
	public static CloudStorageAccount storageAccount;
	public static CloudBlobClient client;
	public static CloudBlobContainer container;
	public static CloudBlockBlob block;
	// Driver
	protected static WebDriver driver;
	// DB
	protected String _connectionString;
	protected Connection con;
}
