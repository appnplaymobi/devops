package dbUtils;


import java.sql.*;
import extentTools.Base;

public class DBqueries extends Base{
	
	public DBqueries(String connectionString){
		_connectionString = connectionString;
	}
	
	public void insrtIntoDllTest(String streamit, String stream, String utilitooltech, String mysporttab)
	{
	    try
	    {   
	    	con = DriverManager.getConnection(_connectionString); 
	        PreparedStatement statement = con.prepareStatement("INSERT INTO LpAi(streamit, stream, utilitooltech, mysporttab) VALUES( ?, ?, ?, ?)");
	        statement.setString(1, streamit);
	        statement.setString(2, stream);
	        statement.setString(3, utilitooltech);
	        statement.setString(4, mysporttab);
	        statement.execute();
	        con.close();
	    }
	    catch (Exception e)
	    {
	        System.err.println("Problem Connecting! " + e.getStackTrace());
	    }
	}
	

}
