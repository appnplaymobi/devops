package sacnFunctions;

import java.awt.AWTException;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import externalFiles.manageExternalFiles;
import tools.Declaration;
import webElements.ScanPage;
import webElements.SiteMessages;

public class ScanRelatedFunctions extends Declaration {

	public static String getPathToFile() throws ParserConfigurationException, SAXException, IOException {
		String fileToScan = manageExternalFiles.getDataFromXML("exePathLocation");
		return fileToScan;
	}

	public static void getPathToClipboard() throws ParserConfigurationException, SAXException, IOException {
		String path = getPathToFile();
		StringSelection stringSelection = new StringSelection(path);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, stringSelection);
	}

	public static void pasteThePath()
			throws ParserConfigurationException, SAXException, IOException, AWTException, InterruptedException {
		getPathToClipboard();
		robot.controlV();
	}

	public static void browseToFile()
			throws Exception {

		ScanPage.clickOnBrowseBtn();
		Thread.sleep(1000);
		robot.altD();
		pasteThePath();
		robot.enter();
		robot.chooseFile();
		System.out.println("file chosen");
		Thread.sleep(4000);
		scanTheFile();
		System.out.println("done woth scan function");
		Thread.sleep(4000);
	}

	public static void scanTheFile() throws Exception {
		SiteMessages.ContinuationContent();
	}
}