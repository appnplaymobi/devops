package readJsonResponce;

import java.util.List;

public class ResultTypes {

	public class AVResult {
		public String id;
		public String type_check;
		public String name;
		public String start_scan;
		public String end_scan;
		public String sha256;
		public Result result;
		public List<Container> containers;
	}

	public class Result {
		public String error;
		public String message;
		public FileInfo file_info;
		public String status_check;
		public List<Container> containers;
	}

	public class FileInfo {
		public String id;
		public String type_check;
		public String name;
		public String start_scan;
		public String end_scan;
		public String sha256;
	}

	public class Container {
		public String name;
		public String check_result;
		public String dbgview;
	}
}
