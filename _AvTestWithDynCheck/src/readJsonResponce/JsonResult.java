package readJsonResponce;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.xml.sax.SAXException;
import com.google.gson.Gson;
import readJsonResponce.ResultTypes.Container;
import readJsonResponce.ResultTypes.Result;
import tools.Declaration;
import tools.GeneralFunctions;
import externalFiles.manageExternalFiles;
import manageReport.ExtentReport;
import com.relevantcodes.extentreports.LogStatus;

public class JsonResult extends Declaration {

	public static Map<String, String> buildJsonToSend()
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		Thread.sleep(15000);
		String apiKey = manageExternalFiles.getDataFromXML("apiKey");
		String fileId = GeneralFunctions.getFileUploadId();
		Map<String, String> JsonMap = new HashMap<String, String>();
		JsonMap.put("data", "{\"api\":\"" + apiKey + '"' + ",\"file_id\":\"" + fileId + '"' + "}");
		return JsonMap;
	}

	public static Result returnPostResult()
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		Map<String, String> map = buildJsonToSend();
		Gson gson = new Gson();

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(manageExternalFiles.getDataFromXML("apiUrl"));

		StringEntity postedString = new StringEntity(gson.toJson(map));

		post.setHeader("Content-type", "application/json");
		post.addHeader("Content-Type", "application/json");
		post.addHeader("cache-control", "no-cache");
		post.addHeader("Accept", "*/*");
		post.addHeader("User-Agent", "PostmanRuntime/7.1.5");
		post.setEntity(postedString);

		HttpResponse response = httpClient.execute(post);
		InputStream in = response.getEntity().getContent();

		String stringResult = IOUtils.toString(in, "UTF-8");
		Result result = gson.fromJson(stringResult, Result.class);
		return result;
	}

	public static void getResults() throws Exception {
		List<String> clean = new ArrayList<String>();
		List<String> detected = new ArrayList<String>();
		
		Result res = returnPostResult();

		while (!res.status_check.equals("done")) {
			Thread.sleep(180000);
			res = returnPostResult();
		}

		for (Container c : res.containers) {
			if (c.check_result.equals("Clean")) {

				clean.add(c.name);

			} else if (c.check_result.equals("Detected")) {
				detected.add(c.name);
			}
		}
		
		ExtentReport.initTestCase("AV test clean");
		for (String cleanAVname : clean) {
			test.log(LogStatus.PASS, cleanAVname);
		}
		ExtentReport.endTestReport();

		ExtentReport.initTestCase("AV test failed");
		for (String detectedAVname : detected) {
			test.log(LogStatus.FAIL, detectedAVname);
		}
		ExtentReport.endTestReport();

		extentReport.flush();
		ExtentReport.uploadReports();
	}
}