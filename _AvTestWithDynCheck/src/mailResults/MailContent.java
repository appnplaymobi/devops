package mailResults;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import externalFiles.manageExternalFiles;

public class MailContent {

	public static String setContentSuccessTest() throws ParserConfigurationException, SAXException, IOException {
		
		String asureReportPath = manageExternalFiles.getReportPathInAsure();
		
		String finalUrl = "<a href=" + asureReportPath + ">" + asureReportPath + "</a>";
		String mesageContent = setMessageColor("<b>Anti virus test result: </b> ", "black") + finalUrl;
		return mesageContent;

	}
	
	public static String setMessageColor(String message, String color){
		String MessageColor = "<font color=" + color + ">" + message + "</font>";
		return MessageColor;
	}

	public static String setContentForFileScanFailure() throws ParserConfigurationException, SAXException, IOException {

		String mesageContent = "<b>The Linkury exe cannt be scand</b> ";
		return mesageContent;
	}

	public static String setContentForLowBalance() throws ParserConfigurationException, SAXException, IOException {

		String mesageContent = setMessageColor("<b>Low Balance message was detected. Test was not preorme </b> ", "red");
		return mesageContent;
	}

	public static String setContentForSiteIsDown() throws ParserConfigurationException, SAXException, IOException {

		String mesageContent = setMessageColor("<b>Test was not preformed, the site is down</b> ", "red");
		return mesageContent;
	}
	
	public static String setContentForLessThenTenDolars(String moneyAmount) throws ParserConfigurationException, SAXException, IOException {

		String mesageContent = setMessageColor("<b>Low Balance message was detected. There is: " + moneyAmount +"</b> ", "red");
		return mesageContent;
	}
}