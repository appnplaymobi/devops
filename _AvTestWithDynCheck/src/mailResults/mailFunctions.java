package mailResults;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import externalFiles.manageExternalFiles;

public class mailFunctions {

	public static Properties _mailProperties(String sender, String password)
			throws ParserConfigurationException, SAXException, IOException {

		String _smtpHost = manageExternalFiles.getDataFromXML("smtpHost");
		String _smtpPort = manageExternalFiles.getDataFromXML("smtpPort");
		String _transportProtocol = manageExternalFiles.getDataFromXML("transportProtocol");

		Properties properties = new Properties();
		properties.put("mail.transport.protocol", _transportProtocol);
		properties.put("mail.smtp.host", _smtpHost);
		properties.put("mail.smtp.port", _smtpPort);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.user", sender);
		properties.put("mail.smtp.password", password);
		properties.put("mail.smtp.starttls.enable", "true");
		return properties;
	}

	public static Message _mailMessage(Session mailSession, InternetAddress[] address, String _sender, String mailSubject,
			Multipart multiPart) throws MessagingException {
		Message message = mailFunctions.createMessage(mailSession);
		message.setRecipients(Message.RecipientType.TO, address);
		message.setFrom(new InternetAddress(_sender));
		message.setSubject(mailSubject);
		message.setContent(multiPart);
		return message;

	}

	public static List<String> collectRecipientsForScanReport()
			throws SAXException, IOException, ParserConfigurationException {
		String Recipients = manageExternalFiles.getDataFromXML("recipients");
		List<String> recipientsAsList = Arrays.asList(Recipients.split(","));
		return recipientsAsList;
	}

	public static List<String> collectRecipientsForLowBalanceMail()
			throws SAXException, IOException, ParserConfigurationException {
		String Recipients = manageExternalFiles.getDataFromXML("recipientsForLowbalanceMail");
		List<String> recipientsAsList = Arrays.asList(Recipients.split(","));
		return recipientsAsList;
	}

	public static Message createMessage(Session mailSession) {
		Message message = new MimeMessage(mailSession);
		return message;
	}

	public static Multipart createMultipart() {
		Multipart multiPart = new MimeMultipart();
		return multiPart;
	}
}
