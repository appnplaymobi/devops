package tests;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import externalFiles.manageExternalFiles;
import manageReport.AsureRelatedFunctions;
import manageReport.ExtentReport;
import tools.Declaration;
import tools.GeneralFunctions;

public class TestFunctions extends Declaration {

	public static void downloadLinker() throws ParserConfigurationException, SAXException, IOException {

		String toFile = GeneralFunctions.returnExeFullPath();

		String fromFile = manageExternalFiles.getDataFromXML("exeFileFromAzure");

		manageExternalFiles.downLoadFile(fromFile, toFile);
	}

	public static void startSession() throws ParserConfigurationException, SAXException, IOException,
			URISyntaxException, StorageException, InvalidKeyException {
		GeneralFunctions.initChrome();
		ExtentReport.initReport();
		AsureRelatedFunctions.initAzureConnection();
	}
}