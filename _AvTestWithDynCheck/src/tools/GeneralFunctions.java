package tools;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.xml.sax.SAXException;

import externalFiles.manageExternalFiles;

import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.HWND;

public class GeneralFunctions extends Declaration {

	public static WebDriver initChrome() throws ParserConfigurationException, SAXException, IOException {
		System.setProperty("webdriver.chrome.driver", manageExternalFiles.getDataFromXML("driverPath"));
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		return driver;
	}

	public static String getPageUrl() {
		String pageUrl = driver.getCurrentUrl();
		return pageUrl;
	}

	public static String getFileUploadId() {
		String urlToSplit = getPageUrl();
		String[] splitedUrl = urlToSplit.split("/");
		String fileId = splitedUrl[splitedUrl.length - 1];
		return fileId;
	}

	public static void deleteFileAfterTest() throws ParserConfigurationException, SAXException, IOException {
		String exeToDelete = returnExeFullPath();
		File file = new File(exeToDelete);
		file.delete();
		driver.quit();
	}

	public static String returnExeFullPath() throws ParserConfigurationException, SAXException, IOException {
		String exePathLocation = manageExternalFiles.getDataFromXML("exePathLocation");
		String exeName = manageExternalFiles.getDataFromXML("exeName");
		String fullPath = exePathLocation + "/" + exeName;
		return fullPath;
	}

	public static void setWindowFocusByCationName(String caption)
			throws AWTException, ParserConfigurationException, SAXException, IOException, InterruptedException {
		HWND hwnd = User32.INSTANCE.FindWindow(null, caption);

		User32.INSTANCE.ShowWindow(hwnd, User32.SW_SHOW);
		User32.INSTANCE.SetForegroundWindow(hwnd);
	}
	
	public static boolean checkIfSiteIsUp() throws IOException, ParserConfigurationException, SAXException, InterruptedException {
		Thread.sleep(7000);
		boolean reachable = InetAddress.getByName("www.dyncheck.com").isReachable(80);
		return reachable;
	}
	
}
