package tools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class Declaration {
	public static ExtentReports extentReport;
	public static ExtentTest test;
	protected static String _reportedFolder = "reports/AVreport/";
	// Azure
	public static CloudStorageAccount storageAccount;
	public static CloudBlobClient client;
	public static CloudBlobContainer container;
	public static CloudBlockBlob block;

	// Driver
	protected static WebDriver driver;
	protected static WebElement element;

}
