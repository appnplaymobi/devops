package mainPkg;

import java.util.List;
import org.junit.Test;
import mailResults.MailContent;
import mailResults.mailFunctions;
import mailResults.mailSendingReport;
import readJsonResponce.JsonResult;
import sacnFunctions.ScanRelatedFunctions;
import tests.TestFunctions;
import tools.Declaration;
import tools.GeneralFunctions;
import usingVpn.UsingVPN;
import webElements.LoginPage;
import webElements.ScanPage;
import webElements.SiteMessages;

public class mainAVCheck extends Declaration {

	@Test
	public void test01() throws Exception {
		List<String> sendToRecipientsForScanReport = mailFunctions.collectRecipientsForScanReport();
		List<String> sendToRecipientsLowBalance = mailFunctions.collectRecipientsForLowBalanceMail();
		TestFunctions.downloadLinker();
		UsingVPN use = new UsingVPN();
		use.trunVpnOn();
		TestFunctions.startSession();
		LoginPage.getToLoginPage();
		boolean siteIsUp = SiteMessages.checkIfSiteIsUp();
		System.out.println(siteIsUp);
		if (siteIsUp) {
			LoginPage.preformLogin();
			boolean haveMonyInAccount = SiteMessages.checkAmountMoneyInTheAccount();
			if (haveMonyInAccount) {
				ScanPage.getToScanPage();
				ScanRelatedFunctions.browseToFile();
				
//				SiteMessages.ContinuationContent();
				JsonResult.getResults();

				use.turnVpnOff();
				GeneralFunctions.deleteFileAfterTest();
				mailSendingReport.createAndSendMailResults(MailContent.setContentSuccessTest(),
						"[test] Linker anti virus scan report", sendToRecipientsForScanReport);
				driver.quit();
			} else {
				use.turnVpnOff();
				mailSendingReport.createAndSendMailResults(MailContent.setContentForLowBalance(),
						"[test] Low balance was detected", sendToRecipientsLowBalance);
			}

		} else {
			use.trunVpnOn();
			mailSendingReport.createAndSendMailResults(MailContent.setContentForSiteIsDown(),
					"[test] Linker anti virus scan test did not work", sendToRecipientsForScanReport);
		}
	}
}