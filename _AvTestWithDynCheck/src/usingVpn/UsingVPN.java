package usingVpn;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import externalFiles.manageExternalFiles;

public class UsingVPN {

	private List<String> _vpnList;
	private VpnTools _tool;

	public UsingVPN() throws ParserConfigurationException, SAXException, IOException {
		_vpnList = vpnNames();
		_tool = new VpnTools();
	}

	private List<String> vpnNames() throws ParserConfigurationException, SAXException, IOException {
		String allVpns = manageExternalFiles.getDataFromXML("vpnNames");
		List<String> vpnList = Arrays.asList(allVpns.split(","));
		return vpnList;
	}

	public void trunVpnOn() throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		String userName = manageExternalFiles.getDataFromXML("vpnUser");
		String password = manageExternalFiles.getDataFromXML("vpnPassword");

		for (String singleVpn : _vpnList) {
			Process p = _tool.RunRasdial(singleVpn, userName, password);
			p.waitFor();
			String countryResult = _tool.parseMaxMindJsonResult();
			if (!countryResult.equals("IL")) {
				break;
			}
		}
	}
	
	public void turnVpnOff() throws IOException, ParserConfigurationException, SAXException, InterruptedException{
		String disconnect = manageExternalFiles.getDataFromXML("disconnectRasdial");
		Process p = _tool.RunRasdial(disconnect);
		p.waitFor();
	}
}
