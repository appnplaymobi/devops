package manageReport;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.microsoft.azure.storage.StorageException;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import externalFiles.manageExternalFiles;
import manageReport.AsureRelatedFunctions;
import tools.Declaration;

public class ExtentReport extends Declaration {
	public static void initReport()
			throws ParserConfigurationException, SAXException, IOException, URISyntaxException, StorageException {
		String reportFilePath = manageExternalFiles.getReportFilePath();
		extentReport = new ExtentReports(reportFilePath);
	}

	public static String createTestName(String testName) {
		testName = "<b>" + testName + "</b>";
		return testName;
	}

	public static void initTestCase(String avName) {
		String _avName = createTestName(avName);
		test = extentReport.startTest(_avName);
	}

	public static void uploadReports()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		uploadHtmlReport();
	}

	public static void endTestReport() throws URISyntaxException, StorageException, IOException {
		endTestReport(test);
	}

	public static void endTestReport(ExtentTest testName) throws URISyntaxException, StorageException, IOException {
		extentReport.endTest(testName);
	}

	public static void uploadHtmlReport()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		String reportFilePathToload = manageExternalFiles.getReportFilePath();
		Path path = Paths.get(reportFilePathToload);
		String pathToUploadTo = _reportedFolder + manageExternalFiles.returnTodayDate() + "/" + path.getFileName();
		AsureRelatedFunctions.uploadToAzure(pathToUploadTo, reportFilePathToload);
	}
}