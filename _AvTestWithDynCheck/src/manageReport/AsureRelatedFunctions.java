package manageReport;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;

import externalFiles.manageExternalFiles;
import tools.Declaration;

public class AsureRelatedFunctions extends Declaration {

	public static void initAzureConnection() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {
		String AccountName = manageExternalFiles.getDataFromXML("AccountName");
		String AccountKey = manageExternalFiles.getDataFromXML("AccountKey");
		final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=" + AccountName
				+ ";AccountKey=" + AccountKey;

		storageAccount = CloudStorageAccount.parse(storageConnectionString);
		client = new CloudBlobClient(storageAccount.getBlobStorageUri(), storageAccount.getCredentials());
		container = client.getContainerReference("qa-reports");
		manageExternalFiles.createTodayDir();
	}

	public static void uploadToAzure(String pathToUploadTo, String fileToUpload)
			throws URISyntaxException, StorageException, IOException {
		block = container.getBlockBlobReference(pathToUploadTo);
		block.getProperties().setContentType("text/html");
		block.uploadFromFile(fileToUpload);
	}
}