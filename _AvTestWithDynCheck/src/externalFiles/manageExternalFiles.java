package externalFiles;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import tools.Declaration;

public class manageExternalFiles extends Declaration {
	
	public static String getDataFromXML(String elementName)
			throws ParserConfigurationException, SAXException, IOException {
		File fXmlFile = new File("D:/Bitbucket/devops/AvTestWithDynCheck/src/externalFiles/AV.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		return doc.getElementsByTagName(elementName).item(0).getTextContent();
	}

	public static void downLoadFile(String fromFile, String toFile) throws MalformedURLException, IOException {
		try {
			FileUtils.copyURLToFile(new URL(fromFile), new File(toFile), 60000, 60000);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static String returnTodayDate() {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		String todayDate = dateFormat.format(now);
		return todayDate;
	}

	public static void createTodayDir() throws ParserConfigurationException, SAXException, IOException {
		String pathToReport = createReportFolder();
		File dir = new File(pathToReport);
		dir.mkdir();
	}
	
	public static String createReportFolder() throws ParserConfigurationException, SAXException, IOException{
		String retpotPath = manageExternalFiles.getDataFromXML("reportPath");
		String todayDate = manageExternalFiles.returnTodayDate();
		String fullPath = retpotPath + todayDate;
		return fullPath;
		
	}
	
	public static String getReportFilePath() throws ParserConfigurationException, SAXException, IOException {
		String retpotPath = manageExternalFiles.getDataFromXML("reportPath");
		String todayDate = manageExternalFiles.returnTodayDate();
		String fileName = manageExternalFiles.getDataFromXML("reportName");

		String pathToReport = retpotPath + _reportedFolder + todayDate + fileName;
		return pathToReport;
	}
	
	public static String getReportPathInAsure() throws ParserConfigurationException, SAXException, IOException{
		String retpotPath = manageExternalFiles.getDataFromXML("AzureBlob");
		String todayDate = manageExternalFiles.returnTodayDate();
		String fileName = manageExternalFiles.getDataFromXML("reportName");

		String pathToReport = retpotPath + _reportedFolder + todayDate + fileName;
		return pathToReport;
	}
	
	public static boolean ifFileExist() throws ParserConfigurationException, SAXException, IOException{
		String reportFilePath = getReportFilePath();
		boolean ifExist = new File(reportFilePath).exists();
		return ifExist;
	}
}