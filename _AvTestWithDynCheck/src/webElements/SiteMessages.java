package webElements;

import java.io.IOException;
import java.util.List;
import javax.mail.MessagingException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.mail.EmailException;
import org.openqa.selenium.By;
import org.xml.sax.SAXException;
import mailResults.MailContent;
import mailResults.mailFunctions;
import mailResults.mailSendingReport;
import tools.Declaration;

public class SiteMessages extends Declaration {

	public static boolean checkIfSiteIsUp() {
		try {
			String Sign = ElementsInPage
					.waitUntilByXpath(driver, "//*[@id='bs-example-navbar-collapse-1']/div/a[2]/button").getText();
			if (Sign.equals("Sign up")) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public static boolean messageOption(String message) {
		return ElementsInPage.waitUntilByXpath(driver, "//span[text()='" + message + "']") != null;
	}

	public static String accountBalance() {
		String moneyAmountInString = driver.findElement(By.xpath("//*[@id='dropdownMenu1']/span[1]")).getText();
		return moneyAmountInString.replace("$", "");
	}

	public static boolean checkAmountMoneyInTheAccount() {
		String blance = accountBalance();
		double blanceAsInt = Double.parseDouble(blance);
		System.out.println(blanceAsInt);
		if (blanceAsInt >= 10) {
			return true;
		} else if (blanceAsInt <= 10) {
			return false;
		}
		return false;
	}

	public static void clickTheScanButtun() {
		ScanPage.clickOnScanBtn();
	}

	public static void fileNotFoundErrorMessage() throws EmailException, MessagingException,
			ParserConfigurationException, SAXException, IOException, InterruptedException {
		List<String> sendToRecipientsForScanReport = mailFunctions.collectRecipientsForScanReport();
		for (int i = 0; i < 3; i++) {
			if (i < 3) {
				Thread.sleep(2000);
				clickTheScanButtun();
			}
		}
		mailSendingReport.createAndSendMailResults(MailContent.setContentForFileScanFailure(),
				"Linker anti virus scan test did not work", sendToRecipientsForScanReport);
	}

	public static void ContinuationContent() throws Exception {
		boolean successMessage = messageOption("Upload success!");
		boolean fileNotFound = messageOption("File not found! Please browse file to start scan.");

		if (successMessage) {
			System.out.println("yes");
			clickTheScanButtun();
			Thread.sleep(10000);
		} else if (fileNotFound) {
			System.out.println("no");
			fileNotFoundErrorMessage();
		}
	}

	public static boolean chekingForLowBalanceAfterClickingScanButten()
			throws EmailException, MessagingException, ParserConfigurationException, SAXException, IOException {
		List<String> sendToRecipientsForScanReport = mailFunctions.collectRecipientsForScanReport();
		boolean lowBalance = messageOption("Low balance!");
		if (lowBalance) {
			mailSendingReport.createAndSendMailResults(MailContent.setContentForLowBalance(),
					"Linker anti virus scan test did not preform, low balance message was detected",
					sendToRecipientsForScanReport);
		}
		return lowBalance;
	}

	public static void CheckDyncheckAccountBalance()
			throws SAXException, IOException, ParserConfigurationException, EmailException, MessagingException {
		List<String> sendToRecipientsForAcountBalance = mailFunctions.collectRecipientsForLowBalanceMail();
		boolean moneyAmount = SiteMessages.checkAmountMoneyInTheAccount();
		if (!moneyAmount) {
			mailSendingReport.createAndSendMailResults(
					MailContent.setContentForLessThenTenDolars(accountBalance() + "$"), "dyncheck account balance",
					sendToRecipientsForAcountBalance);
		}
	}
}
