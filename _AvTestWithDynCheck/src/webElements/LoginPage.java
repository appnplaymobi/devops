package webElements;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import externalFiles.manageExternalFiles;
import tools.Declaration;

public class LoginPage extends Declaration {

	public static void getToLoginPage() throws ParserConfigurationException, SAXException, IOException {
		String dynCheckLoginPage = manageExternalFiles.getDataFromXML("loginPage");
		driver.get(dynCheckLoginPage);
	}

	public static void fillUserName(String userName) throws IOException, InterruptedException {
		ElementsInPage.findElementByXpath(driver, "//*[@type='email']").sendKeys(userName);
	}

	public static void fillPassword(String password) throws IOException, InterruptedException {
		ElementsInPage.findElementByXpath(driver, "//*[@type='password']").sendKeys(password);
	}

	public static void fillUserNameAndPassWord()
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		getToLoginPage();
		String Uname = manageExternalFiles.getDataFromXML("userName");
		String Upassword = manageExternalFiles.getDataFromXML("passWord");
		fillUserName(Uname);
		fillPassword(Upassword);
	}

	public static void clickOnLoginBtn() throws IOException, InterruptedException {
		ElementsInPage.checkBtnByXpath(driver, "//*[@type='submit']").click();
	}

	public static void preformLogin()
			throws IOException, InterruptedException, ParserConfigurationException, SAXException {
		fillUserNameAndPassWord();
		clickOnLoginBtn();
	}
	
}
