package webElements;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import externalFiles.manageExternalFiles;
import tools.Declaration;

public class ScanPage extends Declaration {

	public static void getToScanPage() throws ParserConfigurationException, SAXException, IOException {
		String dynCheckScanPage = manageExternalFiles.getDataFromXML("scanPage");
		driver.navigate().to(dynCheckScanPage);
	}

	public static void clickOnBrowseBtn() throws IOException, InterruptedException {
		ElementsInPage.findElementByXpath(driver, "//*[@class='btn btn-default']").click();
	}

	public static void clickOnScanBtn() {
		ElementsInPage.findElementById(driver, "start_scan_av").click();
	}
}
