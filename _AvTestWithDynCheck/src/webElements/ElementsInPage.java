package webElements;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import tools.Declaration;

public class ElementsInPage extends Declaration {

	public static WebElement findElementById(WebDriver driver, String id) {
		try {
			WebElement element = driver.findElement(By.id(id));
			return element;
		} catch (Exception e) {
			System.out.println("findElementById: " + e.getMessage());
			return null;
		}
	}

	public static WebElement checkBtnByXpath(WebDriver driver, String xpathToClick)
			throws IOException, InterruptedException {
		try {
			WebElement element = driver.findElement(By.xpath(xpathToClick));
			return element;
		} catch (Exception e) {
			return null;
		}
	}

	public static WebElement findElementByXpath(WebDriver driver, String xpathValueName)
			throws IOException, InterruptedException {
		try {
			WebElement element = driver.findElement(By.xpath(xpathValueName));
			return element;
		} catch (Exception e) {
			return null;
		}
	}

	public static WebElement waitUntilById(WebDriver driver, String elementValue) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.id(elementValue))));
			return element;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static WebElement waitByClassName(WebDriver driver, String elementValue) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try{
			WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.className(elementValue))));
			return element;
		}catch (Exception e) {
			return null;
		}
		
	}

	public static WebElement waitUntilByXpath(WebDriver driver, String elementValue) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath(elementValue))));
			return element;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String getPageTitle(){
		String pageTile = driver.getTitle();
		return pageTile;
	}
}