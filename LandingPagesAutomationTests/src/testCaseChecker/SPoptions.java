package testCaseChecker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SPoptions {

	private String prodId;
	private int provider;
	private String domain;
	private String PID;
	private int hours;
	private boolean testOnlyBina;
	private String binaLimit;

	public SPoptions(String prodId, int provider, String domain, String PID, int hours, Boolean testOnlyBina,
			String binaLimit) {
		this.prodId = prodId;
		this.provider = provider;
		this.domain = domain;
		this.PID = PID;
		this.hours = hours;
		this.testOnlyBina = testOnlyBina;
		this.binaLimit = "limit " + binaLimit;
	}

	public int getProvider() {
		return provider;
	}

	public int getHours() {
		return hours;
	}

	public String getProductId() {
		return prodId;
	}

	public String getTestedDomain() {
		return domain;
	}

	public String getPID() {
		return PID;
	}

	public boolean getTestOnlyBina() {
		return testOnlyBina;
	}

	public String getBinaLimit() {
		return binaLimit;
	}

	public void setBinaLimit(String binaLimit) {
		this.binaLimit = binaLimit;
	}

	public static List<SPoptions> providerOptions(String[] args) throws IOException {
		JsonParams result = ConfigFile.getJsonParams(args);

		List<SPoptions> argOptions = new ArrayList<SPoptions>();
		argOptions.add(new SPoptions(result.productId, result.provider, result.domain, result.PID, result.hours,
				result.testOnlyBina, result.binaLimit));
		return argOptions;
	}
}