package testCaseChecker;

import org.apache.commons.cli.Options;

public class JsonParams {

	public String productId;
	public int provider;
	public String domain;
	public String PID;
	public int hours;
	public boolean testOnlyBina;
	public String binaLimit;
	private static Options options;
	
	public static Options argumantOptions() {
		options = new Options();
		options.addOption("productId", true, "");
		options.addOption("provider", true, "");
		options.addOption("domain", true, "");
		options.addOption("PID", true, "");
		options.addOption("hours", true, "");
		options.addOption("testOnlyBina", true, "");
		options.addOption("binaLimit", true, "");
		return options;
	}
}