package testCaseChecker;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import org.apache.commons.io.IOUtils;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

public class ConfigFile {

	private static String getContent(String jsonPath) throws MalformedURLException, IOException {
//		jsonPath = "http://linktestfiles.blob.core.windows.net/qa-reports/lp-test-files/running.json";
		if (jsonPath.toLowerCase().contains("http")) {
			return readUrl(jsonPath);
		} else {
			if (jsonPath == null || Files.notExists(Paths.get(jsonPath), LinkOption.NOFOLLOW_LINKS)) {
				System.out.println("file not found");
			} else {
				return new String(Files.readAllBytes(Paths.get(jsonPath)));
			}
		}
		return null;
	}

	private static boolean checkIfJsonFileIsValid(String content) throws IOException {
		try {
			new JsonParser().parse(content);
			return true;
		} catch (JsonParseException e) {
			System.out.println("Invalid file content");
			return false;
		}
	}

	private static String readUrl(String url) throws MalformedURLException, IOException {
		try (InputStream inputStream = new URL(url).openStream()) {
			return IOUtils.toString(inputStream, StandardCharsets.UTF_8);
		}
	}

	private static JsonParams getJsonResult(String jsonString) throws MalformedURLException, IOException {
		if (jsonString == null) {
			System.out.println("cannot parse the json content");
			return null;
		} else if (checkIfJsonFileIsValid(jsonString)) {
			Gson gson = new Gson();
			JsonParams result = gson.fromJson(jsonString, JsonParams.class);
			return result;
		}
		System.out.println("cannot parse the json content");
		return null;
	}

	public static JsonParams getJsonParams(String[] args) throws IOException {
		String jsonPath = JsonOptions.runningOptions(args);
		String jsonString = getContent(jsonPath);
		return getJsonResult(jsonString);
	}
}