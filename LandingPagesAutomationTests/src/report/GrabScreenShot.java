package report;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import javax.imageio.ImageIO;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import com.relevantcodes.extentreports.LogStatus;
import screenShots.EnumSelection;
import screenShots.TestLocation;
import utilities.Time;

public class GrabScreenShot {

	private EnumSelection TestSelection;
	private reportManager report;
	private Time util;
	public Azure azure;

	public GrabScreenShot(reportManager report, Time util, Azure azure, EnumSelection testSelection)
			throws InvalidKeyException, ParserConfigurationException, SAXException, IOException, URISyntaxException,
			StorageException {
		this.report = report;
		this.util = util;
		this.azure = azure;
		this.TestSelection = testSelection;
	}

	private String screenShotAndReturnPath(String pathToFolder, String ScreenShotName)
			throws IOException, ParserConfigurationException, SAXException, AWTException {
		String imgPathWithName = pathToFolder + "/" + ScreenShotName + ".jpg";

		Rectangle imgSavedSize = new Rectangle(1920, 1080);
		Robot robot = new Robot();
		BufferedImage screenShot = robot.createScreenCapture(imgSavedSize);
		ImageIO.write(screenShot, "jpeg", new File(imgPathWithName));
		return imgPathWithName;
	}

	private String scrrenShotToAzure(TestLocation test, String ScreenShotName) throws IOException,
			ParserConfigurationException, SAXException, URISyntaxException, StorageException, AWTException {
		String fileToUpload = screenShotAndReturnPath(
				TestSelection.getLocalLocationForTest() + "\\" + util.returnTodayDate(), ScreenShotName);
		azure.fileToAzure(TestSelection.getAzureLocationForTest(), fileToUpload, util.returnTodayDate());
		return Paths.get(fileToUpload).getFileName().toString();
	}

	private static int counter = 0;

	private String saveScreenshotName() throws MalformedURLException {
		String tmp = "screenshot_" + counter;
		counter++;
		return tmp;
	}

	public void scrrenShot(TestLocation test, String description) throws MalformedURLException, IOException,
			ParserConfigurationException, SAXException, URISyntaxException, AWTException, StorageException {
		report.getTest().log(LogStatus.INFO,
				description + report.getTest().addScreenCapture(scrrenShotToAzure(test, saveScreenshotName())));
	}
}