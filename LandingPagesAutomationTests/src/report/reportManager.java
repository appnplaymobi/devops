package report;

import java.io.IOException;
import java.net.URISyntaxException;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class reportManager {
	private ExtentReports extentReport;
	private ExtentTest test;

	public reportManager(String reportFilePath) {
		extentReport = new ExtentReports(reportFilePath);
	}

	public ExtentTest initTestCase(String testName, String testedUrl) {
		String publisherIdInBold = createTestName(testName);
		this.test = extentReport.startTest(publisherIdInBold, testedUrl);
		return this.test;
	}

	public ExtentTest getTest() {
		return test;
	}

	public ExtentReports getExtentReports() {
		return extentReport;
	}

	public String createTestName(String publisherID) {
		return "<b>" + publisherID + "</b>";
	}

	public void endTestReport() throws URISyntaxException,  IOException {
		endTestReport(test);
	}

	public void flushTest() {
		extentReport.flush();
	}

	public void endTestReport(ExtentTest testName) throws URISyntaxException, IOException {
		extentReport.endTest(testName);
	}
}