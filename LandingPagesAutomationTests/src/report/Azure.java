package report;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import externalFiles.ManagingXML;
import utilities.FilesAndPathManager;

public class Azure {

	private CloudStorageAccount storageAccount;
	private CloudBlobClient client;
	private CloudBlobContainer container;
	private CloudBlockBlob block;
	private FilesAndPathManager pathTo;
	
	public Azure(FilesAndPathManager pathTo) {
		this.pathTo = pathTo;
	}

	public void initAzureConnection() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {
		String AccountName = ManagingXML.getDataFromXML("accountName");
		String AccountKey = ManagingXML.getDataFromXML("accountKey");
		final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=" + AccountName
				+ ";AccountKey=" + AccountKey;

		storageAccount = CloudStorageAccount.parse(storageConnectionString);
		client = new CloudBlobClient(storageAccount.getBlobStorageUri(), storageAccount.getCredentials());
		container = client.getContainerReference("qa-reports");
	}

	public void uploadFile(String pathToUploadTo, String fileToUpload)
			throws URISyntaxException, StorageException, IOException {
		block = container.getBlockBlobReference(pathToUploadTo);
		block.getProperties().setContentType("text/html");
		block.uploadFromFile(fileToUpload);
	}

	public String fileToAzure(String localFile, String fileToUpload, String date)
			throws URISyntaxException, StorageException, IOException {
		Path path = Paths.get(fileToUpload);
		String pathToUploadTo = localFile + date + "/" + path.getFileName();
		uploadFile(pathToUploadTo, fileToUpload);
		return pathToUploadTo;
	}

	public void uploadBinaTest()
			throws ParserConfigurationException, SAXException, IOException, URISyntaxException, StorageException {
		uploadFile(pathTo.getPathToAzureBigQueryResultsFile(), pathTo.getPathToLocalBigQueryResultsFile());
	}

	public void uploadLPTest()
			throws ParserConfigurationException, SAXException, IOException, URISyntaxException, StorageException {
		uploadFile(pathTo.getPathToAzureSQLResultsFile(), pathTo.getPathToLocalSQLResultsFile());
	}
}