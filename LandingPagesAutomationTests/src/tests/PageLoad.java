package tests;

import java.util.concurrent.TimeUnit;
import com.relevantcodes.extentreports.LogStatus;
import report.reportManager;

public class PageLoad {

	private reportManager report;

	public PageLoad(reportManager report) {
		this.report = report;
	}

	public void reportPageLoad(long start, long end) {
		long durationCalculation = end - start;
		long seconds = TimeUnit.NANOSECONDS.toSeconds(durationCalculation);
		if (seconds > 1) {
			report.getTest().log(LogStatus.WARNING,
					String.format("The page loaded in: <b>%s</b> seconds", String.valueOf(seconds)));
		} else {
			report.getTest().log(LogStatus.INFO,
					String.format("The page loaded in: <b>%s</b> seconds", String.valueOf(seconds)));
		}
	}
}