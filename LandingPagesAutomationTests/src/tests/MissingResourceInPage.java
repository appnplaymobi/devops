package tests;

import java.net.MalformedURLException;
import java.util.List;
import org.openqa.selenium.logging.LogEntry;
import com.relevantcodes.extentreports.LogStatus;
import drivers.UsingDriver;
import report.reportManager;

public class MissingResourceInPage {

	private UsingDriver driver;
	private reportManager report;

	public MissingResourceInPage(UsingDriver driver, reportManager report) throws MalformedURLException {
		this.driver = driver;
		this.report = report;
	}

	public String searchForMissingResourceInPage(String url) {
		List<LogEntry> logEntries = driver.getDriverLogs(url);
		String message = "";
		if (logEntries.size() > 0) {
			for (LogEntry entry : logEntries) {
				message = entry.getMessage();
				return message;
			}
		}
		return message;
	}

	public void testMissingResourceInPage(String url) {
		String missingResource = searchForMissingResourceInPage(url);
		if (!missingResource.isEmpty()) {
			report.getTest().log(LogStatus.FAIL,
					String.format("Found missing resources in page: <b>%s</b> ", missingResource));
		} else {
			report.getTest().log(LogStatus.PASS, String.format("No missing resources in page"));
		}
	}
}