package tests;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.List;
import org.apache.commons.validator.routines.UrlValidator;
import com.relevantcodes.extentreports.LogStatus;
import report.reportManager;
import handleUrl.*;
import webElementsLocators.ElementsInPage;

public class BrokenLinksInPage {

	private checkUrlAndOpenConnection openConnection;
	private ElementsInPage element;
	private reportManager report;

	public BrokenLinksInPage(ElementsInPage element, reportManager report, checkUrlAndOpenConnection openConnection)
			throws MalformedURLException {
		this.element = element;
		this.report = report;
		this.openConnection = openConnection;
	}

	private void responseForbidden(String response) {
		String HashTag = element.elementByXpath("//*[@href='#']");
		if (HashTag.contains("Support")) {
			report.getTest().log(LogStatus.FATAL, String.format("Found Forbidden url in item: <b>%s</b>", HashTag));
		} else if (HashTag.contains("Add")) {
			report.getTest().log(LogStatus.WARNING, String.format("Found Forbidden url in item: <b>%s</b>", HashTag));
		}
	}

	private void checkResposeResult(String singlUrl) throws IOException {
		String response = openConnection.connection(singlUrl);

		if (!response.equals("OK")) {
			if (response.contains("Moved")) {
				report.getTest().log(LogStatus.PASS,
						String.format("<b>%s </b>, the url response status is: <b>%s</b>", singlUrl, response));
			}
			else if (response.equals("Forbidden")) {
				responseForbidden(response);
			} else {
				report.getTest().log(LogStatus.FAIL,
						String.format("The url: <b>%s <b>, is <b>%s <b>", singlUrl, response));
			}
		}
		if (response.equals("OK")) {
			report.getTest().log(LogStatus.PASS, String.format("The url: <b>%s <b>, is <b>%s <b>", singlUrl, response));
		}
	}

	public void checkLinksInPage(String tag, String value) throws IOException {
		List<String> urls = element.WebElementToString(element.findAllElementsInPage(tag), value);
		UrlValidator urlValidator = new UrlValidator();

		for (String singlUrl : urls) {
			if (urlValidator.isValid(singlUrl)) {
				try {
					checkResposeResult(singlUrl);
				} catch (ClassCastException classCalstExp) {
					report.getTest().log(LogStatus.FATAL,
							String.format("Caught \"Class Cast Exception\": %s", classCalstExp.getMessage()));
				} catch (UnknownHostException unKnoneHostExp) {
					report.getTest().log(LogStatus.FATAL,
							String.format("Caught \"Unknown Host Exception\": %s", unKnoneHostExp.getMessage()));
				} catch (Exception ex) {
					report.getTest().log(LogStatus.FATAL,
							String.format("Caught \"Exception\": %s on: <b>%s</b>", ex.getMessage(), singlUrl));
				}

			} else {
				continue;
			}
		}
	}
}