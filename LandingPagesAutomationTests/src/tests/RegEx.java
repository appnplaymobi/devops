package tests;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.relevantcodes.extentreports.LogStatus;
import drivers.UsingDriver;
import report.reportManager;

public class RegEx {

	private UsingDriver driver;
	private reportManager report;

	public RegEx(UsingDriver driver, reportManager report) throws MalformedURLException {
		this.driver = driver;
		this.report = report;
	}

	public void findUnWantedRegExPatternInPage() {
		String pageSource = driver.getPageSource();

		String singleDash = "#([^\\s\\W#]*)#";
		String coupleOfDashs = "##([^#]*)##";
		String curlyBrackets = "\\{\\{([^\\}]*)\\}\\}";

		ifPatternExist(singleDash, pageSource);
		ifPatternExist(coupleOfDashs, pageSource);
		ifPatternExist(curlyBrackets, pageSource);

		// test mail
		String mailRegExPattern = "[A-Za-z0-9._%]+@[A-Za-z0-9._-]+\\.[A-Za-z]{2,4}";
		if (!(findMailRegEx(mailRegExPattern, pageSource) != null)) {
			System.out.println(String.format("no mail was found in the page"));
			report.getTest().log(LogStatus.FAIL, String.format("no mail was found in the page"));
		} else {
			isMailHref();
		}
	}

	public WebElement ifMailInHref() {
		WebElement singleElement;
		try {
			singleElement = driver.getDriver().findElement(By.xpath("//*[contains(@href,'@')] "));
			if (singleElement.isEnabled()) {
				return singleElement;
			}
		} catch (Exception e) {
			// add to report
			System.out.println("exception in function : ifMailInHref " + e.getMessage());
		}
		return null;
	}

	public void isMailHref() {
		boolean isMailClickable = ifMailInHref() != null;
		if (!isMailClickable)
		report.getTest().log(LogStatus.FAIL, String.format("Mail was found in the page, but not clickable"));
	}

	public void ifPatternExist(String regExToFind, String StringToSearch) {
		ifPatternExist(regExToFind, StringToSearch, 0);
	}

	public void ifPatternExist(String regExToFind, String StringToSearch, int maxLength) {
		List<String> allMaches = findRegEx(regExToFind, StringToSearch);
		if (allMaches != null) {
			for (String possibleMatch : allMaches) {
				if (possibleMatch.length() > 100) {
					report.getTest().log(LogStatus.SKIP, String.format("the pattern: %s was found in the page: %s ",
							regExToFind.replace("\\", ""), possibleMatch.replace("<", "").replace(">", "")));
				} else {
					report.getTest().log(LogStatus.FAIL,
							String.format("the pattern: %s was found in the page. The text in the pattern is: %s ",
									regExToFind.replace("\\", ""), possibleMatch));
				}
			}
		}
	}

	public String findMailRegEx(String regExToFind, String StringToSearch) {
		Pattern checkRegEx = Pattern.compile(regExToFind);
		Matcher regMatch = checkRegEx.matcher(StringToSearch);

		while (regMatch.find()) {
			if (regMatch.group().trim().length() != 0) {
				return regMatch.group(0);
			}
		}
		return null;
	}

	public List<String> findRegEx(String regExToFind, String StringToSearch) {
		return findRegEx(regExToFind, StringToSearch, 0);
	}

	private List<String> findRegEx(String regExToFind, String StringToSearch, int maxLength) {
		Pattern checkRegEx = Pattern.compile(regExToFind);
		Matcher regMatch = checkRegEx.matcher(StringToSearch);
		List<String> allMatches = new ArrayList<String>();

		while (regMatch.find()) {
			String matchGroup = regMatch.group().trim();
			if (matchGroup.length() > 0 && ((maxLength > 0 && matchGroup.length() <= maxLength) || maxLength == 0)) {
				allMatches.add(matchGroup);
			}
		}
		List<String> ret = new ArrayList<>(new HashSet<>(allMatches));
		return ret;
	}
}