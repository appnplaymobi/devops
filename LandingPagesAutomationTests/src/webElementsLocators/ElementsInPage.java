package webElementsLocators;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import drivers.UsingDriver;

public class ElementsInPage {

	private UsingDriver driver;

	public ElementsInPage(UsingDriver driver) throws MalformedURLException {
		this.driver = driver;
	}

	public void scrollIntoView() {
		driver.getDriver().findElement(By.tagName("html")).sendKeys(Keys.HOME);
	}

	public WebElement elementByClassName(String className) {
		try {
			WebElement singleElement = driver.getDriver().findElement(By.className(className));
			return singleElement;
		} catch (Exception e) {
			System.out.println("exeption from " + e.getMessage());
			return null;
		}
	}

	public List<WebElement> elementsByCssSelector(String cssSelector) {
		try {
			return driver.getDriver().findElements(By.cssSelector(cssSelector));
		} catch (Exception e) {
			System.out.println("exeption from " + e.getMessage());
			return null;
		}
	}

	public WebElement waitByClassName(String elementValue) {
		try {
			WebDriverWait wait = new WebDriverWait(driver.getDriver(), 5);
			WebElement element = wait
					.until(ExpectedConditions.visibilityOfElementLocated((By.className(elementValue))));
			return element;
		} catch (Exception e) {
			return null;
		}
	}

	public List<WebElement> findAllElementsInPage(String tag) {
		List<WebElement> allElements = elementByTagName(tag);
		return allElements;
	}

	public List<WebElement> elementByTagName(String tagName) {
		List<WebElement> allElements = driver.getDriver().findElements(By.tagName(tagName));
		return allElements;
	}

	public String elementByXpath(String xPath) {
		WebElement singleElement = driver.getDriver().findElement(By.xpath(xPath));
		if (!singleElement.getAttribute("class").isEmpty()) {
			return singleElement.getAttribute("class");
		} else {
			return singleElement.getText();
		}
	}

	public List<String> WebElementToString(List<WebElement> lst, String value) {
		List<String> stringsList = new ArrayList<String>();
		for (WebElement link : lst) {
			stringsList.add(link.getAttribute(value));
		}
		return stringsList;
	}
}