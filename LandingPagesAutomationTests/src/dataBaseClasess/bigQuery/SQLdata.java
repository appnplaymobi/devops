package dataBaseClasess.bigQuery;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Statement;

public class SQLdata {

	private String _connectionString;
	private Connection con;
	private ResultSet result;

	public SQLdata(String connectionString) {
		this._connectionString = connectionString;
	}

	private ResultSet createResultSet(List<String> pids) throws SQLException {
		String sqlCommand = "select concat('https://', pd.domain_name, '/?pid=', pc.pid) from product_domains as pd "
				+ "join product_campaigns pc on pd.product_id = pc.product_id "
				+ "join product pr on pr.id = pc.product_id "
				+ "where pc.use_ai = 1 and pr.provider = 1 and  pc.pid in(" + String.join(",", pids) + ") "
				+ "and pd.domain_name like '%install.%'";
		con = DriverManager.getConnection(_connectionString);
		Statement statement = con.createStatement();

		result = statement.executeQuery(sqlCommand);
		return result;
	}

	public List<String> getUrlsByPids(List<String> pids) {
		List<String> collectUrls = new ArrayList<String>();
		try {
			result = createResultSet(pids);
			while (result.next()) {
				collectUrls.add(result.getString(1));
			}
			con.close();
		} catch (Exception e) {
			System.out.println("Problem Connecting! " + e.getMessage());
		}
		return collectUrls;
	}
}