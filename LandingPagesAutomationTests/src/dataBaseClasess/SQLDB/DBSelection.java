package dataBaseClasess.SQLDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import dataBaseClasess.dbTools.SqlDataParserForSQL;

public class DBSelection {

	private String _connectionString;
	private Connection con;

	public DBSelection(String connectionString) {
		this._connectionString = connectionString;
	}

	public Map<String, SqlDataParserForSQL> select(String prodId, int provider, String domain, String PID, int hours) {
		Map<String, SqlDataParserForSQL> mapUrlCreativeName = new HashMap<String, SqlDataParserForSQL>();
		try {
			String SPsql = "EXEC CreativeUrl ?, ?, ?, ?, ?";
			con = DriverManager.getConnection(_connectionString);

			PreparedStatement ps = con.prepareStatement(SPsql);
			ps.setQueryTimeout(90);
			ps.setEscapeProcessing(true);
			ps.setString(1, prodId); // product Id (String)
			ps.setInt(2, provider); // provider (int) 1 or 2
			ps.setString(3, domain); // domainName (String)
			ps.setString(4, PID); // PID (String get 11111 as default)
			ps.setInt(5, hours); // hours, 0 is all (Int)
			
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String creativeName = rs.getString(2);
				String fullUrl = rs.getString(1);
				mapUrlCreativeName.put(fullUrl, new SqlDataParserForSQL(creativeName, 0));
			}
			con.close();
		} catch (Exception e) {
			System.out.println("Problem Connecting! " + e.getMessage());
		}
		return mapUrlCreativeName;
	}
}