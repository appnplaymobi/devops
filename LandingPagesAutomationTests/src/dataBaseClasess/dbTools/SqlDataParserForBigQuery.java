package dataBaseClasess.dbTools;

import java.util.List;

public class SqlDataParserForBigQuery {
	
	private List<String> pids;
	private String url;

	public SqlDataParserForBigQuery(List<String> pids, String url) {
		this.pids = pids;
		this.url = url;
	}

	public List<String> getPid() {
		return pids;
	}

	public String getUrl() {
		return url;
	}
}