package utilities;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Time {

	public long startNanoTime() throws IOException {
		long start = System.nanoTime();
		return start;
	}

	public long endNanoTime() throws IOException {
		long end = System.nanoTime();
		return end;
	}

	public String returnTodayDate() {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		String todayDate = dateFormat.format(now);
		return todayDate;
	}
}