package utilities;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import externalFiles.ManagingXML;

public class FilesAndPathManager {

	private Time time;

	public FilesAndPathManager(Time time) {
		this.time = time;
	}

	private boolean createFolder(File parent, String... subFolder)
			throws ParserConfigurationException, SAXException, IOException {
		parent.mkdirs();
		if (!parent.exists() || !parent.isDirectory()) {
			return false;
		}
		for (String sub : subFolder) {
			File subFile = new File(parent, sub);
			subFile.mkdir();
			if (!subFile.exists() || !subFile.isDirectory()) {
				return false;
			}
		}
		return true;
	}

	public void createLocalReportFolders() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {

		createFolder(new File(getLocalBigQueryResultsFolder()), time.returnTodayDate());
		createFolder(new File(getLpLocalLPresultsFolder()), time.returnTodayDate());
	}

	public void createReportFoldersInAzure() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {

		createFolder(new File(getAzureLPresultsFolder(), time.returnTodayDate()));
		createFolder(new File(getAzureBigQueryResultsFolder(), time.returnTodayDate()));
	}

	public String getLocalBigQueryResultsFolder() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = System.getProperty("user.home") + "\\Downloads\\"
				+ ManagingXML.getDataFromXML("localBigQueryReportFolder");
		return fullPathToReport;
	}

	public String getPathToLocalBigQueryResultsFile() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = getLocalBigQueryResultsFolder() + "\\" + time.returnTodayDate()
				+ "/bigQueryReportResults.html";
		return fullPathToReport;
	}

	public String getLpLocalLPresultsFolder() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = System.getProperty("user.home") + "\\Downloads\\"
				+ ManagingXML.getDataFromXML("localLpReportFolder");
		return fullPathToReport;
	}

	public String getPathToLocalSQLResultsFile() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = getLpLocalLPresultsFolder() + "\\" + time.returnTodayDate() + "/lpReportResults.html";
		return fullPathToReport;
	}

	public String getAzureBigQueryResultsFolder() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = ManagingXML.getDataFromXML("azureBigQueryReportFolder");
		return fullPathToReport;
	}

	public String getPathToAzureBigQueryResultsFile() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = getAzureBigQueryResultsFolder() + time.returnTodayDate()
				+ "/bigQueryReportResults.html";
		return fullPathToReport;
	}

	public String getLinkToBigQueryReport() throws ParserConfigurationException, SAXException, IOException {
		return ManagingXML.getDataFromXML("azureReportFolder") + getPathToAzureBigQueryResultsFile();
	}

	public String getAzureLPresultsFolder() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = ManagingXML.getDataFromXML("azureLpReportFolder");
		return fullPathToReport;
	}

	public String getPathToAzureSQLResultsFile() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = getAzureLPresultsFolder() + time.returnTodayDate() + "/lpReportResults.html";
		return fullPathToReport;
	}

	public String getLinkToSQLreport() throws ParserConfigurationException, SAXException, IOException {
		return ManagingXML.getDataFromXML("azureReportFolder") + getPathToAzureSQLResultsFile();
	}
}