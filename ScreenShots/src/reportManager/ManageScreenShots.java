package reportManager;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.xml.sax.SAXException;
import tools.GenUtils;
import tools.ReadFiles;

public class ManageScreenShots {

	public static String takeScreenShotAndReturnPath(String ScreenShotName, WebDriver driver)
			throws IOException, ParserConfigurationException, SAXException {
		String reportedFolderPath = ReadFiles.getDataFromXML("reportFolder");

		String pathToimage = reportedFolderPath + "//" + GenUtils.returnTodayDate() + "//" + ScreenShotName + ".png";
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	
		FileUtils.copyFile(scrFile, new File(pathToimage));
		return pathToimage.trim();
	}
	
	
	
}