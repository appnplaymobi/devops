package manageSelenium;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.xml.sax.SAXException;

import reportManager.ManageScreenShots;
import tools.ReadFiles;

public class driversDefinitions {

	private static WebDriver driver;

	public static void initChromeDriver() throws ParserConfigurationException, SAXException, IOException {
		String chromeDriver = ReadFiles.getDataFromXML("chromeDriverPath");
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		driver = new ChromeDriver();
	}

	public static void waitForLoad() {
		new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd)
				.executeScript("return document.readyState").equals("complete"));
	}

	public static void navigateTo(String pageUrl)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		System.out.println("testd url: " + pageUrl);
		driver.get(pageUrl);
		driver.manage().window().maximize();
	}

	public static String getPageUrl() {
		String pageUrl = driver.getCurrentUrl();
		return pageUrl;
	}

	public static void quitDriver() {
		driver.quit();
	}

	public static String takeScreenShot(String ScreenShotName)
			throws IOException, ParserConfigurationException, SAXException {
		ManageScreenShots.takeScreenShotAndReturnPath(ScreenShotName, driver);
		return ScreenShotName;
	}

}