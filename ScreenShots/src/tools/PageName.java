package tools;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.conn.util.PublicSuffixMatcher;
import org.apache.http.conn.util.PublicSuffixMatcherLoader;

public class PageName {
	public static String getPageName(String url) throws MalformedURLException {
		URL aURL = new URL(url);
		String query = aURL.getQuery();
		Map<String, String> map = hashMapQuery(query);
		String finalName = getDomainName(url) + "-" + map.get("c");
		return finalName;
	}
	public static Map<String, String> hashMapQuery(String query) {
		Map<String, String> result = new HashMap<String, String>();
		if (query != null) {
			String[] allQuery = query.split("&");
			for (String singleQueryString : allQuery) {
				String[] pair = singleQueryString.split("=");
				String val = null;
				if (pair.length == 2) {
					val = pair[1];
				}
				result.put(pair[0], val);
			}
		}
		return result;
	}
	public static String getDomainName(String url) throws MalformedURLException {
		PublicSuffixMatcher publicSuffixMatcher = PublicSuffixMatcherLoader.getDefault();
		String domainName = publicSuffixMatcher.getDomainRoot(getHostUrl(url));
		return domainName;
	}
	public static String getHostUrl(String url) throws MalformedURLException {
		URL aURL = new URL(url);
		String domainName = aURL.getHost();
		return domainName;
	}
}
