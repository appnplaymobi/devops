package tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ReadFiles {

	public static String getDataFromXML(String elementName)
			throws ParserConfigurationException, SAXException, IOException {
		File fXmlFile = new File("D:/Bitbucket/devops/ScreenShots/src/tools/ScreenShots.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		return doc.getElementsByTagName(elementName).item(0).getTextContent();
	}

	public static List<String> readUrlsFile() throws ParserConfigurationException, SAXException, IOException {
		String urlsFile = getDataFromXML("urlsFilePath");
		List<String> lst = Files.readAllLines(Paths.get(urlsFile));
		return lst;
	}

	public static String getReportFilePath(String reportFileName)
			throws ParserConfigurationException, SAXException, IOException {
		String retpotPath = ReadFiles.getDataFromXML("reportFolder");
		String todayDate = GenUtils.returnTodayDate();
		String fullPathToReport = retpotPath + todayDate + reportFileName;
		return fullPathToReport;
	}
}