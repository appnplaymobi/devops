package tools;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GenUtils {
	
	public static String returnTodayDate() {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		String todayDate = dateFormat.format(now);
		return todayDate;
	}

}
