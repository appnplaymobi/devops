package mainPkg;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.relevantcodes.extentreports.LogStatus;

import manageSelenium.driversDefinitions;
import reportManager.reportManager;
import tools.PageName;
import tools.ReadFiles;

public class testManager {
	private static reportManager manager;

	@BeforeClass
	public static void startTestSession()
			throws ParserConfigurationException, SAXException, IOException, InvalidKeyException, URISyntaxException {
		driversDefinitions.initChromeDriver();
	}

	@Test
	public void test01() throws ParserConfigurationException, SAXException, IOException, InterruptedException, URISyntaxException {
		List<String> allUrls = ReadFiles.readUrlsFile();
		String reportFileName = ReadFiles.getDataFromXML("screenShotReportName");
		manager = new reportManager(ReadFiles.getReportFilePath(reportFileName));

		for (String url : allUrls) {
			driversDefinitions.navigateTo(url);
			manager.initTestCase(PageName.getDomainName(url), url);
			String pathTofile = driversDefinitions.takeScreenShot(PageName.getDomainName(url));
			manager.getTest().log(LogStatus.INFO, PageName.getHostUrl(url) + manager.getTest().addScreenCapture(pathTofile));			
		}
		manager.endTestReport();
		manager.flushTest();
	}
}
