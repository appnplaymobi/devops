package initChromeDriver;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.xml.sax.SAXException;

public class UsingDriver {

	private static WebDriver driver;
	
	private static void initDriver() throws ParserConfigurationException, SAXException, IOException {
		String chromeDriverLocation = new File("chromedriver.exe").getAbsolutePath();
		System.setProperty("webdriver.chrome.driver", chromeDriverLocation);
		driver = new ChromeDriver();
	}

	public WebDriver launchPage(String url) throws ParserConfigurationException, SAXException, IOException {
		initDriver();
		driver.get(url);
		return driver;
	}

	public WebDriver endDriver() {
		driver.close();
		return driver;
	}
	
	public WebElement findElementByXpath(String xpathString){
		return driver.findElement(By.xpath(xpathString));
	}
}