package slackMessaging;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.xml.sax.SAXException;

import configuration.UseDataFromXml;

public class MessageToSlack {

	private HttpPost createHttpPost() throws ParserConfigurationException, SAXException, IOException {
		String baseSlackUrl = UseDataFromXml.getSlackUrl();
		String slackChannel = UseDataFromXml.getSlackChannel();
		HttpPost httpPost = new HttpPost(baseSlackUrl + slackChannel);
		return httpPost;
	}

	private CloseableHttpResponse createHttpResponce(HttpPost httpPost) throws ClientProtocolException, IOException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = httpClient.execute(httpPost);
		return response;
	}

	private StringEntity createStringEntity(String slackMessage) {
		StringEntity jsonMessage = new StringEntity("{\"text\" : \"" + slackMessage + "\"}", "UTF-8");
		jsonMessage.setContentType("application/json");
		return jsonMessage;
	}

	public void postMassage(String slackMessage) throws IOException, ParserConfigurationException, SAXException {
		HttpPost httpPost = createHttpPost();
		httpPost.addHeader("Content-type", "application/json");
		StringEntity jsonMessage = createStringEntity(slackMessage);
		httpPost.setEntity(jsonMessage);
		CloseableHttpResponse response = createHttpResponce(httpPost);
		response.close();
	}
}