package slackMessaging;

public class MessageContent {


	public String content(int daysToRelease, String releaseVersion) {
		String content = "";
		if (daysToRelease < 8 && daysToRelease > 0) {
			content = String.format("the next %s will be released in: %s days", releaseVersion, daysToRelease);
		} else if (daysToRelease == 0) {
			content = String.format("The next %s, will be released today.", releaseVersion);
		} else if (daysToRelease == -1) {
			content = String.format("The next %s, supposed be released yesterday.", releaseVersion);
		}
		return content;
	}
}