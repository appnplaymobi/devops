package configuration;

import xmlConfiguration.UsingXml;

public class UseDataFromXml {

	private static String getConfigurationName() {
		String configurationFileName = "NextReleaseConfiguration.xml";
		return configurationFileName;
	}

	public static String getChromeUrl() {
		String chromeVersionUrl = "";
		try {
			chromeVersionUrl = UsingXml.getXmlValue("chromeNextReleaseWebPage", getConfigurationName());
		} catch (Exception e) {
			return null;
		}
		return chromeVersionUrl;
	}

	public static String getFirefoxUrl() {
		String chromeVersionUrl = "";
		try {
			chromeVersionUrl = UsingXml.getXmlValue("firefoxNextReleaseWebPage", getConfigurationName());
		} catch (Exception e) {
			return null;
		}
		return chromeVersionUrl;
	}

	public static String getSlackUrl() {
		String slackBaseUrl = "";
		try {
			slackBaseUrl = UsingXml.getXmlValue("slackUrl", getConfigurationName());
		} catch (Exception e) {
			return null;
		}
		return slackBaseUrl;
	}

	public static String getSlackChannel() {
		String slackChannel = "";
		try {
			slackChannel = UsingXml.getXmlValue("automationAlertsChannel", getConfigurationName());
		} catch (Exception e) {
			return null;
		}
		return slackChannel;
	}
}