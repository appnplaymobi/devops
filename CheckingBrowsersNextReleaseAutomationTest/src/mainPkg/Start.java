package mainPkg;

import java.io.IOException;
import java.text.ParseException;
import javax.mail.MessagingException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.mail.EmailException;
import org.xml.sax.SAXException;
import VM.TurnOffVM;
import checkReleaseAndVersion.CHDateAndVersion;
import checkReleaseAndVersion.FFDateAndVersion;
import checkReleaseAndVersion.FindWebElements;
import initChromeDriver.UsingDriver;
import slackMessaging.MessageContent;
import slackMessaging.MessageToSlack;
import utils.GenUtil;

public class Start {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException,
			InterruptedException, ParseException, EmailException, MessagingException {
		GenUtil utils = new GenUtil();
		MessageContent mailIt = new MessageContent();
		UsingDriver usingDriver = new UsingDriver();
		FindWebElements elementsInPage = new FindWebElements(usingDriver);
		FFDateAndVersion testFirefox = new FFDateAndVersion(usingDriver, utils, mailIt, elementsInPage);
		CHDateAndVersion testChrome = new CHDateAndVersion(usingDriver, utils, mailIt, elementsInPage);
		MessageToSlack slack = new MessageToSlack();

		String checkFirefox = testFirefox.mailFirefox();
		String checkChrome = testChrome.mailChrome();

		if (!checkFirefox.isEmpty() || !checkChrome.isEmpty()) {
			slack.postMassage(checkFirefox + "\n" + checkChrome);
		}
		TurnOffVM turnoff = new TurnOffVM();
		turnoff.sendShutDownCommand();
	}
}