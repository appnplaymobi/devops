package checkReleaseAndVersion;

public class VersionAndRelease {

	private String ffReleasedVersion;
	private int daysToRelease;
	public VersionAndRelease(String ffReleasedVersion, int daysToRelease) {
		this.ffReleasedVersion = ffReleasedVersion;
		this.daysToRelease = daysToRelease;
	}

	public String getReleasedVersionNumber() {
		return ffReleasedVersion;
	}

	public int getDaysToRelease() {
		return daysToRelease;
	}
}
