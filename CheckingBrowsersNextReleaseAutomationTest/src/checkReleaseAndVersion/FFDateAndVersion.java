package checkReleaseAndVersion;

import java.io.IOException;
import java.text.ParseException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import configuration.UseDataFromXml;
import initChromeDriver.UsingDriver;
import slackMessaging.MessageContent;
import utils.GenUtil;

public class FFDateAndVersion {

	public UsingDriver driver;
	public GenUtil utils;
	public MessageContent msg;
	public FindWebElements element;

	public FFDateAndVersion(UsingDriver driver, GenUtil utils, MessageContent msg, FindWebElements element) {
		this.driver = driver;
		this.utils = utils;
		this.msg = msg;
		this.element = element;
	}

	public String returnFirefoxReleaseDate()
			throws IOException, InterruptedException, ParseException, ParserConfigurationException, SAXException {
		String firefoxReleasePage = UseDataFromXml.getFirefoxUrl();
		driver.launchPage(firefoxReleasePage);
		Thread.sleep(3000);
		String ffReleaseDate = element.fireFoxReleaseDate();
		return ffReleaseDate;
	}

	public long extractFirefoxeReleaseDate()
			throws IOException, InterruptedException, ParseException, ParserConfigurationException, SAXException {
		String ffRelDate = returnFirefoxReleaseDate();
		long releaseDate = utils.longReleaseDate(ffRelDate);
		return releaseDate;
	}

	public String mailFirefox()
			throws IOException, InterruptedException, ParseException, ParserConfigurationException, SAXException {
		int daysToRelease = utils.diffInDays(extractFirefoxeReleaseDate());
		String ffReleaseVersion = element.fireFoxReleaseVersion();
		driver.endDriver();
		return msg.content(daysToRelease, ffReleaseVersion);
	}
}