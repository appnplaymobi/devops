package report;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class ReportTools {

	public String returnTodayDate() {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		String todayDate = dateFormat.format(now);
		return todayDate;
	}

	public void createFolder(String pathToFolder) {
		File newReportDirectory = new File(pathToFolder);
		if (!newReportDirectory.exists()) {
			try {
				newReportDirectory.mkdir();
			} catch (Exception e) {
				System.out.println("createReportFolder function crushed: " + e.getMessage());
			}
		}
	}

	public void createReportFolders() throws ParserConfigurationException, SAXException, IOException {
		String todayDate = returnTodayDate();

		String azureFolderPath = UsingXML.getXmlValue("azureReportFolder");
		createFolder(azureFolderPath + todayDate);

		String localFolderPath = UsingXML.getXmlValue("localReportFolder");
		createFolder(localFolderPath + todayDate);
	}
}