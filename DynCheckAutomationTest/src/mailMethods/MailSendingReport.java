package mailMethods;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.mail.EmailException;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class MailSendingReport {
	
	private MailFunctions mailFunctions;
	
	public MailSendingReport(MailFunctions mailFunctions) {
		this.mailFunctions = mailFunctions;
	}

	public void prepareMailResults(String mailContent, String mailSubject, List<String> sendToRecipientsForScanReport)
			throws EmailException, MessagingException, ParserConfigurationException, SAXException, IOException {

		String _sender = UsingXML.getXmlValue("sender");
		String _password = UsingXML.getXmlValue("password");

		Properties properties = mailFunctions._mailProperties(_sender, _password);

		Session mailSession = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(properties.getProperty("mail.smtp.user"),
						properties.getProperty("mail.smtp.password"));
			}
		});

		InternetAddress[] address = new InternetAddress[sendToRecipientsForScanReport.size()];
		for (int i = 0; i < sendToRecipientsForScanReport.size(); i++) {
			address[i] = new InternetAddress(sendToRecipientsForScanReport.get(i));
		}

		Multipart multiPart = mailFunctions.createMultipart();
		
		Message message = mailFunctions._mailMessage(mailSession, address, _sender, mailSubject, multiPart);

		MimeBodyPart htmlPart = new MimeBodyPart();

		htmlPart.setContent(mailContent, "text/html");
		multiPart.addBodyPart(htmlPart);

		Transport.send(message);
	}
	
	public void mailReulstsAVscanReport(String mailContent, String mailSubject) throws EmailException, MessagingException, ParserConfigurationException, SAXException, IOException{
		 
		List<String> sendToRecipientsForScanReport = mailFunctions.collectRecipientsForAVscanReport();
		prepareMailResults(mailContent, mailSubject, sendToRecipientsForScanReport);
	}
	
}