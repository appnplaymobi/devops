package mailMethods;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import configuration.UsingXML;
import report.ReportTools;

public class MailContent {

	private ReportTools util;

	public MailContent(ReportTools util) {
		this.util = util;
	}

	public String getReportPathInAsure() throws ParserConfigurationException, SAXException, IOException {
		String retpotPath = UsingXML.getXmlValue("azureReportFolder");
		String todayDate = "/reports/AVreport/" + util.returnTodayDate(); 
		String fileName = UsingXML.getXmlValue("reportName");

		String pathToReport = retpotPath + todayDate + fileName;
		return pathToReport;
	}

	public String setContentSuccessTest() throws ParserConfigurationException, SAXException, IOException {
		String asureReportPath = getReportPathInAsure();
		String finalUrl = "<a href=" + asureReportPath + ">" + asureReportPath + "</a>";
		String mesageContent = setMessageColor("<b>AV Scan report result: </b> ", "black") + finalUrl;
		return mesageContent;
	}
	
	public String setContentMessageSiteIsDwon() {
		String mesageContent = setMessageColor("<b>Site was down, test did not performed: </b> ", "red");
		return mesageContent;
	}
	
	public String setContentMessageLowBalance(String balance) {
		String mesageContent = setMessageColor(String.format("<b>test did not preformed dew to low balance in the account:. %s</b> ", balance), "red");
		return mesageContent;
	}

	public String setMessageColor(String message, String color) {
		String MessageColor = "<font color=" + color + ">" + message + "</font>";
		return MessageColor;
	}
}