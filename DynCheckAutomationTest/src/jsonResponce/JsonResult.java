package jsonResponce;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.xml.sax.SAXException;
import com.google.gson.Gson;
import com.relevantcodes.extentreports.LogStatus;
import jsonResponce.ResultTypes.Container;
import jsonResponce.ResultTypes.Result;
import report.ManageReport;
import configuration.UsingXML;
import driverDefinition.UsingDriver;

public class JsonResult {

	private UsingDriver driver;
	private ManageReport report;

	public JsonResult(UsingDriver driver, ManageReport report) {
		this.driver = driver;
		this.report = report;
	}

	public Map<String, String> buildJsonToSend()
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		String apiKey = UsingXML.getXmlValue("apiKey");
		String fileId = driver.getFileUploadId();
		
		System.out.println("fileId: " + fileId);
		Map<String, String> JsonMap = new HashMap<String, String>();
		JsonMap.put("data", "{\"api\":\"" + apiKey + '"' + ",\"file_id\":\"" + fileId + '"' + "}");
		return JsonMap;
	}

	public Result returnPostResult() throws ParserConfigurationException, SAXException, IOException,
			InterruptedException, KeyManagementException, NoSuchAlgorithmException {
		Map<String, String> map = buildJsonToSend();
		Gson gson = new Gson();

		SSLContext ctx = SSLContext.getInstance("TLS");
		ctx.init(new KeyManager[0], new TrustManager[] { new DefaultTrustManager() }, new SecureRandom());
		SSLContext.setDefault(ctx);

		SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(ctx,
				SSLConnectionSocketFactory.getDefaultHostnameVerifier());

		HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(connectionFactory).build();

		HttpPost post = new HttpPost(UsingXML.getXmlValue("apiUrl"));

		StringEntity postedString = new StringEntity(gson.toJson(map));

		post.setHeader("Content-type", "application/json");
		post.addHeader("Content-Type", "application/json");
		post.addHeader("cache-control", "no-cache");
		post.addHeader("Accept", "*/*");
		post.addHeader("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
		post.setEntity(postedString);

		HttpResponse response = httpClient.execute(post);
		InputStream in = response.getEntity().getContent();

		String stringResult = IOUtils.toString(in, "UTF-8");
		Result result = gson.fromJson(stringResult, Result.class);
		return result;
	}

	public void getResults() throws Exception {
		List<String> clean = new ArrayList<String>();
		List<String> detected = new ArrayList<String>();

		Result res = returnPostResult();

		while (!res.status_check.equals("done")) {
			Thread.sleep(180000);
			res = returnPostResult();
		}

		for (Container c : res.containers) {
			if (c.check_result.equals("Clean")) {

				clean.add(c.name);

			} else if (c.check_result.equals("Detected")) {
				detected.add(c.name);
			}
		}

		report.initTestCase("AV test clean");
		for (String cleanAVname : clean) {
			report.getTest().log(LogStatus.PASS, cleanAVname);
			System.out.println(cleanAVname);
		}
		report.endTestReport();

		report.initTestCase("AV test failed");
		for (String detectedAVname : detected) {
			System.out.println(detectedAVname);
			report.getTest().log(LogStatus.FAIL, detectedAVname);
		}
		report.endTestReport();
		report.flushTest();
	}
}