package utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class ManageTestedExe {

	public static void downloadLinker() throws ParserConfigurationException, SAXException, IOException {
		String toFile = returnExeFullPath();
		String fromFile = UsingXML.getXmlValue("exeFileFromAzure");
		downLoadFile(fromFile, toFile);
	}

	public static String returnExeFullPath() throws ParserConfigurationException, SAXException, IOException {
		String exePathLocation = UsingXML.getXmlValue("exePathLocation");
		String exeName = UsingXML.getXmlValue("exeName");
		String fullPath = exePathLocation + "/" + exeName;
		return fullPath;
	}

	public static void downLoadFile(String fromFile, String toFile) throws MalformedURLException, IOException {
		try {
			FileUtils.copyURLToFile(new URL(fromFile), new File(toFile), 60000, 60000);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("exception on: downLoadFile() function: " + e.getMessage());
		}
	}

	public static void deleteFileIfExist() throws ParserConfigurationException, SAXException, IOException {
		String reportFilePath = returnExeFullPath();
		boolean ifExist = new File(reportFilePath).exists();
		if (ifExist) {
			File file = new File(reportFilePath);
			file.delete();
		}
	}
}
