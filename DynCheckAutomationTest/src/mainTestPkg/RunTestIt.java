package mainTestPkg;

import connectingToVPN.UsingVPN;
import driverDefinition.UsingDriver;
import jsonResponce.JsonResult;
import report.AsureRelatedFunctions;
import report.ManageReport;
import report.ReportTools;
import webElements.FindElements;
import webElements.LoginPageActions;
import webElements.ScanPageActions;
import webElements.SufficientFunds;

public class RunTestIt {

	public static void main(String[] args) throws Exception {
		
		ReportTools util = new ReportTools();
		AsureRelatedFunctions azure = new AsureRelatedFunctions(util); 
		
		String todayDate = util.returnTodayDate();
		ManageReport report = new ManageReport("D:/LP_automation_files/reports/AVreport/" + todayDate + "/AV-report.html");
		UsingVPN useVpn = new UsingVPN();
		UsingDriver driver = new UsingDriver(); 
		FindElements elements = new FindElements(driver);
		LoginPageActions loginPage = new LoginPageActions(elements);
		ScanPageActions scanPage = new ScanPageActions(elements);
		SufficientFunds checkMoneyInAccount = new SufficientFunds(elements); 
		JsonResult jsonResults = new JsonResult(driver, report); 
		
		TestIt testIt = new TestIt(useVpn, driver, loginPage, checkMoneyInAccount, scanPage, jsonResults, azure, util);
		testIt.test01DownloadExeAndConnectToVPN();
		testIt.test02();
	}
}