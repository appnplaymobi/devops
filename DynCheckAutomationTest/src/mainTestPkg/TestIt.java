package mainTestPkg;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.microsoft.azure.storage.StorageException;

import configuration.UsingXML;
import connectingToVPN.UsingVPN;
import driverDefinition.UsingDriver;
import jsonResponce.JsonResult;
import mailMethods.MailContent;
import mailMethods.MailFunctions;
import mailMethods.MailSendingReport;
import report.AsureRelatedFunctions;
import report.ReportTools;
import usingRobot.UsingRobot;
import utilities.ManageTestedExe;
import webElements.LoginPageActions;
import webElements.ScanPageActions;
import webElements.SufficientFunds;

public class TestIt {

	private UsingVPN vpn;
	private UsingDriver driver;
	private LoginPageActions loginActions;
	private ScanPageActions scanActions;
	private SufficientFunds sufficientFunds;
	private JsonResult jsonResults;
	private AsureRelatedFunctions azure;
	private ReportTools reportUtils;

	public TestIt(UsingVPN vpn, UsingDriver driver, LoginPageActions loginActions, SufficientFunds sufficientFunds,
			ScanPageActions scanActions, JsonResult jsonResults, AsureRelatedFunctions azure, ReportTools reportUtils)

			throws ParserConfigurationException, SAXException, IOException, InvalidKeyException, URISyntaxException,
			StorageException {
		this.vpn = vpn;
		this.driver = driver;
		driver.initDriver();
		this.loginActions = loginActions;
		this.scanActions = scanActions;
		this.sufficientFunds = sufficientFunds;
		this.jsonResults = jsonResults;
		this.reportUtils = reportUtils;
		this.azure = azure;
		azure.initAzureConnection();
	}

	@Before
	public void beforeTest() throws InvalidKeyException, ParserConfigurationException, SAXException, IOException,
			URISyntaxException, StorageException {
		reportUtils.createReportFolders();
	}

	@Test
	public void test01DownloadExeAndConnectToVPN() throws Exception {
		 ManageTestedExe.downloadLinker();
		 vpn.trunVpnOn();
	}

	@Test
	public void test02() throws Exception {
		MailFunctions mailFunctions = new MailFunctions();
		MailSendingReport sendReport = new MailSendingReport(mailFunctions);
		MailContent mailContent = new MailContent(reportUtils);

		driver.getDriver().get(UsingXML.getXmlValue("loginPage"));
		boolean siteUp = loginActions.loginToService();
		if (siteUp) {
			boolean haveSufficientFunds = sufficientFunds.checkAmountMoneyInTheAccount();
			if (haveSufficientFunds) {
				driver.getDriver().navigate().to(UsingXML.getXmlValue("scanPage"));
				scanActions.clickOnBrowseBtn();
				UsingRobot.browseToFile();
				scanActions.fileScanning();
				jsonResults.getResults();
				vpn.turnVpnOff();
				azure.uploadReport();
				sendReport.mailReulstsAVscanReport(mailContent.setContentSuccessTest(), "[Test] AV Scan Report");
				ManageTestedExe.deleteFileIfExist();
			} else {
				vpn.turnVpnOff();
				sendReport.mailReulstsAVscanReport(mailContent.setContentMessageLowBalance(sufficientFunds.accountBalance()),
						"[Test] AV Scan Report - low balance");
			}

		} else {
			vpn.turnVpnOff();
			sendReport.mailReulstsAVscanReport(mailContent.setContentMessageSiteIsDwon(),
					"[Test] AV Scan Report - site is down");
		}
		driver.endDriver();
	}
}