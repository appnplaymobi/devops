package usingRobot;

import java.awt.AWTException;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class UsingRobot {

	public static void getPathToClipboard()
			throws ParserConfigurationException, SAXException, IOException, AWTException, InterruptedException {
		String path = UsingXML.getXmlValue("exePathLocation");
		StringSelection stringSelection = new StringSelection(path);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, stringSelection);
	}

	public static void browseToFile() throws Exception {
		Thread.sleep(1000);
		robot.altD();
		getPathToClipboard();
		robot.controlV();
		robot.enter();
		robot.chooseFile();
		Thread.sleep(4000);
	}
}