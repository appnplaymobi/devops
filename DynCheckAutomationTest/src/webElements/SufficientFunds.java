package webElements;

import java.io.IOException;

public class SufficientFunds {
	
	private FindElements elements;
	
	public SufficientFunds(FindElements elements) {
		this.elements = elements;
	}
	
	public String accountBalance() throws IOException, InterruptedException {
		String moneyAmountInString = elements.findElementByXpath("//*[@id='dropdownMenu1']/span[1]").getText();
		return moneyAmountInString.replace("$", "");
	}
	
	public boolean checkAmountMoneyInTheAccount() throws IOException, InterruptedException {
		String blance = accountBalance();
		double blanceAsInt = Double.parseDouble(blance);
		if (blanceAsInt >= 10) {
			return true;
		} else if (blanceAsInt <= 10) {
			return false;
		}
		return false;
	}
}
