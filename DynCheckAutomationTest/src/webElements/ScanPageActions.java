package webElements;

import java.io.IOException;

public class ScanPageActions {

	private FindElements elements;

	public ScanPageActions(FindElements elements) {
		this.elements = elements;
	}

	public void clickOnBrowseBtn() throws IOException, InterruptedException {
		elements.findElementByXpath("//*[@class='btn btn-default']").click();
	}

	public void clickOnScanBtn() {
		elements.findElementById("start_scan_av").click();
	}

	public boolean messageOption(String message) {
		return elements.waitUntilByXpath("//span[text()='" + message + "']") != null;
	}

	public boolean fileScanning() throws Exception {
		boolean successMessage = messageOption("Upload success!");
		if (successMessage) {
			clickOnScanBtn();
			return true;
		} else {
//			need to write log
			return false;
		}
	}
}
