package connectingToVPN;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.xml.sax.SAXException;
import com.google.gson.Gson;

import configuration.UsingXML;

public class VpnTools {

	private Process Run(String... commands) throws IOException {
		ProcessBuilder processBuilder = new ProcessBuilder(commands);
		return processBuilder.start();
	}

	public Process RunRasdial(String... commands) throws IOException, ParserConfigurationException, SAXException {
		String rasdialExe = UsingXML.getXmlValue("rasdial");
		String[] newCommands = new String[commands.length + 1];
		newCommands[0] = rasdialExe;

		System.arraycopy(commands, 0, newCommands, 1, commands.length);
		return Run(newCommands);
	}

	public String callingMaxMind(String url) throws ClientProtocolException, IOException {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(url);
		HttpResponse response = httpClient.execute(get);
		InputStream in = response.getEntity().getContent();
		String maxMindResult = IOUtils.toString(in, "UTF-8");
		return maxMindResult;
	}

	private String returnMaxMindResult()
			throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
		String maxMindUrl = UsingXML.getXmlValue("maxMind");
		String maxMindResult = callingMaxMind(maxMindUrl);
		return maxMindResult;
	}

	public String parseMaxMindJsonResult()
			throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
		String jsonRes = returnMaxMindResult();
		Gson g = new Gson();
		VpnResult res = g.fromJson(jsonRes, VpnResult.class);
		String country = res.getCountry();
		return country;
	}
}