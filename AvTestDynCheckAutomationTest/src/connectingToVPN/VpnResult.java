package connectingToVPN;

public class VpnResult {

	private String country;
	private String ip;

	private VpnResult(String country, String ip) {
		this.country = country;
		this.ip = ip;
	}

	public String getCountry() {
		return country;
	}

	public String getIp() {
		return ip;
	}
}