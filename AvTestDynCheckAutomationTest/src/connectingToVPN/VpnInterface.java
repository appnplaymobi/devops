package connectingToVPN;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public interface VpnInterface {
	void trunVpnOn() throws ParserConfigurationException, SAXException, IOException, InterruptedException;

	void turnVpnOff() throws IOException, ParserConfigurationException, SAXException, InterruptedException;

}
