package utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;
import configuration.UsingXML;
import report.PathManager;

public class ManageTestedExe {

	private PathManager pathTo;

	public ManageTestedExe(PathManager pathTo) {
		this.pathTo = pathTo;
	}

	public void downloadLinker() throws ParserConfigurationException, SAXException, IOException {
		String toFile = pathTo.getExeLocation() + pathTo.getExeName();
		String fromFile = UsingXML.getXmlValue("exeFileFromAzure");
		downLoadFile(fromFile, toFile);
	}

	private void downLoadFile(String fromFile, String toFile) throws MalformedURLException, IOException {
		try {
			FileUtils.copyURLToFile(new URL(fromFile), new File(toFile), 60000, 60000);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("exception on: downLoadFile() function: " + e.getMessage());
		}
	}

	public void deleteFileIfExist() throws ParserConfigurationException, SAXException, IOException {
		String reportFilePath = pathTo.getExeLocation() + pathTo.getExeName();
		boolean ifExist = new File(reportFilePath).exists();
		if (ifExist) {
			File file = new File(reportFilePath);
			file.delete();
		}
	}
}
