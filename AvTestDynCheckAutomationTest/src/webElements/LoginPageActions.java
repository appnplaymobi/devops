package webElements;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class LoginPageActions {

	private FindElements elements;

	public LoginPageActions(FindElements elements) {
		this.elements = elements;
	}

	public boolean checkIfSiteIsUp() {
		try {
			String Sign = elements.waitUntilByXpath("//*[@id='bs-example-navbar-collapse-1']/div/a[2]/button")
					.getText();
			if (Sign.equals("Sign up")) {
				return true;
			}
		} catch (Exception e) {
			System.out.println(String.format("exception in function: checkIfSiteIsUp(): %s", e.getMessage()));
			return false;
		}
		return false;
	}

	public void fillUserName(String userName) throws IOException, InterruptedException {
		elements.findElementByXpath("//*[@type='email']").sendKeys(userName);
	}

	public void fillPassword(String password) throws IOException, InterruptedException {
		elements.findElementByXpath("//*[@type='password']").sendKeys(password);
	}

	public void clickOnLoginBtn() throws IOException, InterruptedException {
		elements.findElementByXpath("//*[@type='submit']").click();
	}

	public boolean loginToService()
			throws IOException, InterruptedException, ParserConfigurationException, SAXException {
		boolean siteUp = checkIfSiteIsUp();
		if (siteUp) {
			fillUserName(UsingXML.getXmlValue("userName"));
			fillPassword(UsingXML.getXmlValue("passWord"));
			Thread.sleep(90);
			clickOnLoginBtn();
			return true;
		}
		return false;
	}
}