package webElements;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import driverDefinition.UsingDriver;

public class FindElements {

	private UsingDriver driver;

	public FindElements(UsingDriver driver) {
		this.driver = driver;
	}

	public WebElement findElementById(String id) {
		try {
			WebElement element = driver.getDriver().findElement(By.id(id));
			return element;
		} catch (Exception e) {
			System.out.println("findElementById: " + e.getMessage());
			return null;
		}
	}

	public WebElement findElementByXpath(String xpathToClick) throws IOException, InterruptedException {
		try {
			WebElement element = driver.getDriver().findElement(By.xpath(xpathToClick));
			return element;
		} catch (Exception e) {
			return null;
		}
	}

	public WebElement waitUntilByXpath(String elementValue) {
		WebDriverWait wait = new WebDriverWait(driver.getDriver(), 30);
		try {
			WebElement element;
			element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.xpath(elementValue))));
			return element;
		} catch (Exception e) {
			return null;
		}
	}
}