package driverDefinition;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.xml.sax.SAXException;

public class UsingDriver {

	private WebDriver driver;

	public void initDriver() throws ParserConfigurationException, SAXException, IOException {
		String chromeDriverLocation = new File("chromedriver.exe").getAbsolutePath();
		System.setProperty("webdriver.chrome.driver", chromeDriverLocation);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	public WebDriver getDriver() {
		return driver;
	}

	public WebDriver endDriver() {
		driver.quit();
		return driver;
	}

	public void waitForPageToLoad() {
		new WebDriverWait(driver, 30).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
	}

	public String extractCurrentUrl() {
		return driver.getCurrentUrl();
	}

	public String getFileUploadId() throws InterruptedException {
		String fileId = "";
		for (int i = 0; i < 30; i++) {
			try {
				String urlToSplit = extractCurrentUrl();
				String[] splitedUrl = urlToSplit.split("/");
				String idInUrl = splitedUrl[splitedUrl.length - 2];
				if (splitedUrl.length == 6 && idInUrl.equals("id")) {
					fileId = splitedUrl[splitedUrl.length - 1];
					break;
				} else {
					Thread.sleep(2000);
				}
			} catch (Exception e) {
				System.out.println(String.format("exception in function: getFileUploadId() %s", e.getMessage()));
			}
		}
		return fileId;
	}
}