package mainTestPkg;

import connectingToVPN.UsingVPN;
import driverDefinition.UsingDriver;
import jsonResponce.JsonResult;
import report.AsureRelatedFunctions;
import report.ManageReport;
import report.PathManager;
import slackMessaging.MessageToSlack;
import usingRobot.UsingRobot;
import utilities.ManageTestedExe;
import webElements.FindElements;
import webElements.LoginPageActions;
import webElements.ScanPageActions;
import webElements.SufficientFunds;

public class RunTestIt {

	public static void main(String[] args) throws Exception {
		PathManager pathManager = new PathManager();
		AsureRelatedFunctions azure = new AsureRelatedFunctions(pathManager);
		azure.initAzureConnection();
		ManageTestedExe testExe = new ManageTestedExe(pathManager);
		ManageReport report = new ManageReport(pathManager.getPathToLocalResultFile());
		UsingVPN useVpn = new UsingVPN();
		UsingDriver driver = new UsingDriver();
		FindElements elements = new FindElements(driver);
		LoginPageActions loginPage = new LoginPageActions(elements);
		ScanPageActions scanPage = new ScanPageActions(elements);
		SufficientFunds checkMoneyInAccount = new SufficientFunds(elements);
		JsonResult jsonResults = new JsonResult(driver, report);
		MessageToSlack slack = new MessageToSlack();
		UsingRobot usingRobot = new UsingRobot(pathManager);

		TestIt testIt = new TestIt(useVpn, driver, loginPage, checkMoneyInAccount, scanPage, jsonResults, azure,
				pathManager, slack, testExe, usingRobot, pathManager);
		testIt.test01DownloadExeAndConnectToVPN();
		testIt.test02();
	}
}