package mainTestPkg;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;

import VM.TurnOffVM;
import configuration.UsingXML;
import connectingToVPN.UsingVPN;
import driverDefinition.UsingDriver;
import jsonResponce.JsonResult;
import mailMethods.MailContent;
import mailMethods.MailFunctions;
import mailMethods.MailSendingReport;
import report.AsureRelatedFunctions;
import report.PathManager;
import slackMessaging.MessageToSlack;
import usingRobot.UsingRobot;
import utilities.ManageTestedExe;
import webElements.LoginPageActions;
import webElements.ScanPageActions;
import webElements.SufficientFunds;

public class TestIt {

	private UsingVPN vpn;
	private UsingDriver driver;
	private LoginPageActions loginActions;
	private ScanPageActions scanActions;
	private SufficientFunds sufficientFunds;
	private JsonResult jsonResults;
	private AsureRelatedFunctions azure;
	private PathManager reportUtils;
	private MessageToSlack slack;
	private ManageTestedExe testExe;
	private UsingRobot usingRobot;
	private PathManager pathTo;

	public TestIt(UsingVPN vpn, UsingDriver driver, LoginPageActions loginActions, SufficientFunds sufficientFunds,
			ScanPageActions scanActions, JsonResult jsonResults, AsureRelatedFunctions azure, PathManager reportUtils,
			MessageToSlack slack, ManageTestedExe testExe, UsingRobot usingRobot, PathManager pathTo)

			throws ParserConfigurationException, SAXException, IOException, InvalidKeyException, URISyntaxException,
			StorageException {
		this.vpn = vpn;
		this.driver = driver;
		driver.initDriver();
		this.loginActions = loginActions;
		this.scanActions = scanActions;
		this.sufficientFunds = sufficientFunds;
		this.jsonResults = jsonResults;
		this.reportUtils = reportUtils;
		this.azure = azure;
		this.slack = slack;
		this.testExe = testExe;
		this.usingRobot = usingRobot;
		this.pathTo = pathTo;
	}

	@Before
	public void beforeTest() throws InvalidKeyException, ParserConfigurationException, SAXException, IOException,
			URISyntaxException, StorageException {
		reportUtils.createReportFolders();
	}

	@Test
	public void test01DownloadExeAndConnectToVPN() throws Exception {
		testExe.downloadLinker();
		vpn.trunVpnOn();
	}

	@Test
	public void test02() throws Exception {
		TurnOffVM vm = new TurnOffVM();
		MailFunctions mailFunctions = new MailFunctions();
		MailSendingReport sendReport = new MailSendingReport(mailFunctions);
		MailContent mailContent = new MailContent(reportUtils);

		driver.getDriver().get(UsingXML.getXmlValue("loginPage"));
		boolean siteUp = loginActions.loginToService();
		if (siteUp) {
			boolean haveSufficientFunds = sufficientFunds.checkAmountMoneyInTheAccount();
			if (haveSufficientFunds) {
				driver.getDriver().navigate().to(UsingXML.getXmlValue("scanPage"));
				scanActions.clickOnBrowseBtn();
				usingRobot.browseToFile();
				scanActions.fileScanning();
				jsonResults.getResults();
				vpn.turnVpnOff();
				azure.uploadReport();
				slack.postMassage("click here to see AV daily test result", pathTo.getLinkToReport());
				testExe.deleteFileIfExist();
			} else {
				vpn.turnVpnOff();
				sendReport.mailReulstsAVscanReport(
						mailContent.setContentMessageLowBalance(sufficientFunds.accountBalance()),
						"[Test] AV Scan Report - low balance");
				vm.sendShutDownCommand();
			}

		} else {
			vpn.turnVpnOff();
			slack.postMassage(mailContent.setContentMessageSiteIsDwon(), "");
			vm.sendShutDownCommand();
		}
		driver.endDriver();
		vm.sendShutDownCommand();
	}
}