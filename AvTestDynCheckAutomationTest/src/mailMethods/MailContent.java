package mailMethods;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import report.PathManager;

public class MailContent {

	private PathManager pathManager;

	public MailContent(PathManager pathManager) {
		this.pathManager = pathManager;
	}

	public String setContentSuccessTest() throws ParserConfigurationException, SAXException, IOException {
		String mesageContent = String.format("AV Scan report result: %s", pathManager.getPathToAzureResultFile());
		return mesageContent;
	}

	public String setContentMessageSiteIsDwon() {
		String mesageContent = "Site was down, test did not performed";
		return mesageContent;
	}

	public String setContentMessageLowBalance(String balance) {
		String mesageContent = setMessageColor(
				String.format("<b>test did not preformed dew to low balance in the account:. %s</b> ", balance), "red");
		return mesageContent;
	}

	public String setMessageColor(String message, String color) {
		String MessageColor = "<font color=" + color + ">" + message + "</font>";
		return MessageColor;
	}
}