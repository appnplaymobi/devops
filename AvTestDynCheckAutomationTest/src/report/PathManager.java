package report;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class PathManager {

	public String returnTodayDate() {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		String todayDate = dateFormat.format(now);
		return todayDate;
	}

	private boolean createFolder(File parent, String... subFolder)
			throws ParserConfigurationException, SAXException, IOException {
		parent.mkdirs();
		if (!parent.exists() || !parent.isDirectory()) {
			return false;
		}
		for (String sub : subFolder) {
			File subFile = new File(parent, sub);
			subFile.mkdir();
			if (!subFile.exists() || !subFile.isDirectory()) {
				return false;
			}
		}
		return true;
	}

	public void createReportFolders() throws ParserConfigurationException, SAXException, IOException {
		createFolder(new File(getAzureResultFolder()), returnTodayDate());
		createFolder(new File(getLocalResultFolder()), returnTodayDate());
	}

	public String getLocalResultFolder() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = System.getProperty("user.home") + "\\Downloads\\"
				+ UsingXML.getXmlValue("localReportFolderName");
		return fullPathToReport;
	}

	public String getPathToLocalResultFile() throws ParserConfigurationException, SAXException, IOException {

		String fullPathToReport = getLocalResultFolder() + "\\" + returnTodayDate()
				+ UsingXML.getXmlValue("reportName");
		return fullPathToReport;
	}

	public String getAzureResultFolder() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = UsingXML.getXmlValue("azureReportFolderName");
		return fullPathToReport;
	}

	public String getPathToAzureResultFile() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = getAzureResultFolder() + returnTodayDate() + UsingXML.getXmlValue("reportName");
		return fullPathToReport;
	}

	public String getLinkToReport() throws ParserConfigurationException, SAXException, IOException {
		return UsingXML.getXmlValue("azureBaseFolder") + getPathToAzureResultFile();
	}

	public String getExeLocation() throws ParserConfigurationException, SAXException, IOException {
		String fullPath = getLocalResultFolder() + "\\" + "fish" + "\\";
		return fullPath;
	}

	public String getExeName() throws ParserConfigurationException, SAXException, IOException {
		return UsingXML.getXmlValue("exeName");
	}
}