package t;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;

public class sssss {

	private int providerValue;
	private int hoursValue;
	private static Options options;
	private static CommandLineParser cmdParser;
	private static CommandLine cmdLine;

	public sssss(int providerValue, int hoursValue) {
		this.providerValue = providerValue;
		this.hoursValue = hoursValue;
	}

	public int getProvider() {
		return providerValue;
	}

	public int getHours() {
		return hoursValue;
	}

	private static Options argumantOptions() {
		options = new Options();
		options.addOption("provider", true, "");
		options.addOption("hours", true, "");
		return options;
	}

	public static sssss providerOptions(String[] args) {
		cmdParser = new DefaultParser();
		options = argumantOptions();
		int provider = 0;
		int hours = 0;
		try {
			cmdLine = cmdParser.parse(options, args, true);
			if (cmdLine.hasOption("provider")) {
				String providerVal = cmdLine.getOptionValue("provider");
				switch (providerVal) {
				case "1":
					provider = 1;
					break;
				case "2":
					provider = 2;
					break;

				case "0":
					provider = 0;
					break;
				}
			}
			if (cmdLine.hasOption("hours")) {
				String hoursVal = cmdLine.getOptionValue("hours");
				hours = Math.max(0, Integer.parseInt(hoursVal));
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println(String.format("the tested provider is: %s", provider));
		System.out.println(String.format("the chosen period of time is: %s", hours));
		return new sssss(provider, hours);
	}
}