package testFunctions.ExtentionsLP;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import HTMLParser.feedRouterWikiRow;
import manageUrls.UrlBuilder;
import report.TestResult;
import report.reportManager;
import utils.UrlRelatedFunctions;

public class ExtractingLPurls {
	
	private reportManager manager;
	private UrlRelatedFunctions getUrlRelatedFunctions = new UrlRelatedFunctions();
	private boolean report_passed_tests;

	public ExtractingLPurls(reportManager report) {
		this.manager = report;
		report_passed_tests = false;
	}

	public void getUrlAndTest() throws IOException, ParserConfigurationException, SAXException, InterruptedException {

		List<feedRouterWikiRow> allData = UrlBuilder.extractUrl();
		for (feedRouterWikiRow itemInList : allData) {
			List<TestResult> testCollections = new ArrayList<TestResult>();
			String urlToTest = itemInList.getQueryUrl();
			getUrlRelatedFunctions.navigateToPageAndWaitToFinishLoding(urlToTest);
			String url = getUrlRelatedFunctions.getUrlFromJson();

			boolean failure = false;
			if (!checkIfError(testCollections, url)) {
				failure = true;
			}

			writeExtractedUrlAsInfoToReport(testCollections, url);

			if (!failure && !checkIfHostMatch(testCollections, url)) {
				failure = true;
			}
			
			if(!failure) {
				checkIfExtractedChannelMatchGivenChannelFromWiki(testCollections, url, itemInList.getChannel());
			}

			if (report_passed_tests || !report_passed_tests && hasFailures(testCollections)) {
				ExtentTest test = manager.initTestCase(itemInList.getPid(),
						getUrlRelatedFunctions.openUrlInNewTab(urlToTest));
				for (TestResult testResult : testCollections) {
					test.log(testResult.status, testResult.message);
				}
			}
		}
	}

	private boolean hasFailures(List<TestResult> testCollections) {
		for (TestResult testResult : testCollections) {
			if (testResult.status == LogStatus.ERROR || testResult.status == LogStatus.FAIL
					|| testResult.status == LogStatus.FATAL) {
				return true;
			}
		}
		return false;
	}

	private boolean checkIfHostMatch(List<TestResult> testCollections, String url) throws MalformedURLException {
		boolean Hostmatch = getUrlRelatedFunctions.getIsHostMatch(url);
		if (Hostmatch) {
			testCollections.add(new TestResult(LogStatus.PASS, "Final host match expected result"));
		} else {
			testCollections.add(new TestResult(LogStatus.ERROR, "Host didn't match"));
		}
		return Hostmatch;
	}

	private void checkIfExtractedChannelMatchGivenChannelFromWiki(List<TestResult> testCollections, String urlToExtract,
			String channelToMatchWith) throws MalformedURLException {
		String extractedChannelFromUrl = getUrlRelatedFunctions.extractChannel(urlToExtract);
		if (extractedChannelFromUrl == null) {
			testCollections.add(new TestResult(LogStatus.FAIL, "No channel was detected in final url"));
		} else if (!extractedChannelFromUrl.equals(channelToMatchWith)) {
			testCollections.add(new TestResult(LogStatus.FAIL, String.format(
					"The expected channel was: %s. The result was: %s", channelToMatchWith, extractedChannelFromUrl)));
		} else if (extractedChannelFromUrl.equals(channelToMatchWith)) {
			testCollections.add(new TestResult(LogStatus.PASS, "Channel match expexted result"));
		}
	}

	private boolean checkIfError(List<TestResult> testCollections, String url) {
		if (url == null) {
			testCollections
					.add(new TestResult(LogStatus.FATAL, String.format("Could not extract url from FeedRouter")));
			return false;
		}
		return true;
	}

	private void writeExtractedUrlAsInfoToReport(List<TestResult> testCollections, String finalUrl) {
		testCollections.add(new TestResult(LogStatus.INFO, String.format("The tested url was: %s", finalUrl)));
	}

}
