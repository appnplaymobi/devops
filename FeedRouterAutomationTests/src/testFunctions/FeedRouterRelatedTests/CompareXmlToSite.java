package testFunctions.FeedRouterRelatedTests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import com.relevantcodes.extentreports.LogStatus;

import HTMLParser.feedRouterWikiParser;
import HTMLParser.feedRouterWikiRow;
import XMLParser.FeedRouter;
import XMLParser.Publisher;
import report.TestResult;
import report.reportManager;

public class CompareXmlToSite {

	public reportManager manager;

	public CompareXmlToSite(reportManager report) {
		this.manager = report;
	}

	public void comparePidFromXmlToPidFromSite(FeedRouter feedRouterValues)
			throws IOException, ParserConfigurationException, SAXException {
		List<TestResult> testCollections = new ArrayList<TestResult>();
		List<Publisher> allPublishers = feedRouterValues.publishers;
		List<feedRouterWikiRow> resultList = feedRouterWikiParser.parse();
		for (Publisher singlePublisher : allPublishers) {
			if (singlePublisher.publisherid != null) {
				boolean foundPublisherId = false;
				for (feedRouterWikiRow items : resultList) {
					if (singlePublisher.publisherid.equals(items.getPid())) {
						foundPublisherId = true;
						break;
					}
				}
				if (!foundPublisherId) {
					testCollections.add(new TestResult(LogStatus.FAIL, String.format(
							"The publisherId: <b>%s</b>, exist in the FeedRouter XML but not exist in the LP site",
							singlePublisher.publisherid)));
				}
			}
		}
		manager.initTestCase("Comparing publisherId from XML to publisherId in the LP Extensions site", "");
		for (TestResult testResult : testCollections) {
			manager.getTest().log(testResult.status, testResult.message);
		}
	}

	public void comparePidFromSiteToPidFromXml(FeedRouter feedRouterValues)
			throws IOException, ParserConfigurationException, SAXException {
		List<TestResult> testCollections = new ArrayList<TestResult>();
		List<Publisher> allPublishers = feedRouterValues.publishers;
		List<feedRouterWikiRow> wikiPidList = feedRouterWikiParser.parse();
		for (feedRouterWikiRow wikiPidRow : wikiPidList) {
			boolean foundPublisherId = false;
			for (Publisher singlePublisher : allPublishers) {
				if (singlePublisher.publisherid != null && wikiPidRow.getPid().equals(singlePublisher.publisherid)) {
					foundPublisherId = true;
					break;
				}
			}
			if (!foundPublisherId) {
				testCollections.add(new TestResult(LogStatus.FAIL,
						String.format(
								"The publisherId: <b>%s</b>, exist in the LP site but not exist in the FeedRouter XML",
								wikiPidRow.getPid())));
			}
			else {
				testCollections.add(new TestResult(LogStatus.PASS,
						String.format(
								"All publisherId match", "")));
			}
		}
		manager.initTestCase("Comparing publisherId from LP Extensions site to publisherId in the FeedRouter XML", "");
		for (TestResult testResult : testCollections) {
			manager.getTest().log(testResult.status, testResult.message);
		}
	}
}
