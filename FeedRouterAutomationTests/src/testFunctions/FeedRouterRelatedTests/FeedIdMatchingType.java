package testFunctions.FeedRouterRelatedTests;

import java.util.ArrayList;
import java.util.List;
import com.relevantcodes.extentreports.LogStatus;
import XMLParser.DownloadProvider;
import XMLParser.Feed;
import XMLParser.FeedRouter;
import XMLParser.FeedToPublisher;
import XMLParser.Publisher;
import XMLParser.RoutingDefinition;
import XMLParser.RoutingParameter;
import report.TestResult;
import report.reportManager;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.net.MalformedURLException;

public class FeedIdMatchingType {
	private static String NO_PUB = "NO_PUB";
	private static String DIFF_TYPES = "DIFF_TYPES";
	private static String BOTH_EMPTY = "BOTH_EMPTY";
	private static String NO_FEED = "NO_FEED";
	private static String EQUALS = "EQUALS";

	public reportManager manager;

	public FeedIdMatchingType(reportManager report) {
		this.manager = report;
	}

	public static Map<String, String> separateQuery(String query) {
		return hashMapQuery(query, new String[] { "&", "=" });
	}

	public static Map<String, String> separateQueryStringAdditions(String query) {
		return hashMapQuery(query, new String[] { ";", "=" });
	}

	private static Map<String, String> hashMapQuery(String query, String[] seperators) {
		Map<String, String> result = new HashMap<String, String>();
		if (query != null) {
			String[] allQuery = query.split(seperators[0]);
			for (String singleQueryString : allQuery) {
				String[] pair = singleQueryString.split(seperators[1]);

				String val = null;
				if (pair.length == 2) {
					val = pair[1];
				}
				result.put(pair[0], val);
			}
		}
		return result;
	}

	public String normalizeUrl(String url) throws MalformedURLException {
		if (url.startsWith("//")) {
			url = "https:" + url;
			return url;
		}
		return url;
	}

	private Map<String, String> parseQueryString(String url) throws MalformedURLException {
		URL aURL = new URL(url);
		String query = aURL.getQuery();
		return separateQuery(query);
	}

	public List<DataLink> returnUrlsToTest(FeedRouter feedRouterValues) throws MalformedURLException {
		List<Feed> allFeeds = feedRouterValues.feeds;
		List<DataLink> collectedLinks = new ArrayList<DataLink>();

		for (Feed feed : allFeeds) {
			String normalizedUrl = normalizeUrl(feed.Url);
			DataLink link = new DataLink(feed, parseQueryString(normalizedUrl));
			String publisher = link.getPublisher();
			String dpid = link.getDpid();
			if (publisher != null && publisher.equals("defaultsf") && dpid != null && dpid.equals("yhs")) {
				collectedLinks.add(link);
			}
		}
		return collectedLinks;
	}

	public static String getTypeFromQueryAdditions(String queryStringAdditions) {
		return separateQueryStringAdditions(queryStringAdditions).get("type");
	}

	private Map<String, List<TestResult>> InitTest() {
		Map<String, List<TestResult>> ret = new HashMap<String, List<TestResult>>();

		ret.put(NO_PUB, new ArrayList<TestResult>());
		ret.put(DIFF_TYPES, new ArrayList<TestResult>());
		ret.put(BOTH_EMPTY, new ArrayList<TestResult>());
		ret.put(NO_FEED, new ArrayList<TestResult>());
		ret.put(EQUALS, new ArrayList<TestResult>());

		return ret;
	}

	private Map<String, String> GetTestCaseNamesMap() {
		Map<String, String> ret = new HashMap<String, String>();

		ret.put(NO_PUB, "Feed have type, publisher dont have");
		ret.put(DIFF_TYPES, "Feed and Publisher have diffrent types");
		ret.put(BOTH_EMPTY, "Both Feed and Publisher the type is empty");
		ret.put(NO_FEED, "Feed dont have type");
		ret.put(EQUALS, "The Type for the Feed and for the Publisher is equal");

		return ret;
	}

	public void typeMatcher(FeedRouter feedRouterValues) throws Exception {
		List<DataLink> links = returnUrlsToTest(feedRouterValues);

		Map<String, List<TestResult>> testMap = InitTest();
		Map<String, List<Publisher>> pubsMap = FeedToPublisher.feedIdToPublisherList(feedRouterValues);

		for (DataLink link : links) {

			List<Publisher> pubs = pubsMap.get(link.getFeed().id);
			if (pubs == null) {
				continue;
			}
			for (int i = 0; i < pubs.size(); i++) {
				Publisher singlePublisher = pubs.get(i);
				for (DownloadProvider singleDownloadProvider : singlePublisher.downloadProviders) {

					List<String> types = new ArrayList<String>();
					if (singleDownloadProvider.routingParameters != null) {
						for (int j = 0; j < singleDownloadProvider.routingParameters.size(); j++) {
							RoutingParameter routingParameter = singleDownloadProvider.routingParameters.get(j);
							if (routingParameter.routingDefinitions != null) {
								for (RoutingDefinition routingDefinition : routingParameter.routingDefinitions) {
									if (routingDefinition.QueryStringAddition != null) {
										types.add(getTypeFromQueryAdditions(routingDefinition.QueryStringAddition));
									}
								}
							}
						}
					}
					boolean publisherHasType = false;
					boolean typeFound = false;
					for (String singleType : types) {
						String acualType = null;
						if (link.getType() != null && link.getType().equals(singleType)) {
							typeFound = true;
							acualType = singleType;
						}
						if (singleType != null) {
							publisherHasType = true;
						}
					}
					if (link.getType() == null && !publisherHasType) {
						testMap.get(BOTH_EMPTY)
								.add(new TestResult(LogStatus.ERROR,
										String.format("Both FeedID: <b>%s</b>, and publisher: <b>%s</b> dont have type",
												link.getFeed().id, singlePublisher.id)));
					} else if (link.getType() == null && publisherHasType) {

						testMap.get(NO_FEED)
								.add(new TestResult(LogStatus.PASS, String.format(
										"The FeedID: <b>%s</b>, does not have type but the publisher: <b>%s</b> has types: <b>%s</b>",
										link.getFeed().id, singlePublisher.id, GetPusblisherTypesString(types))));
					} else if (link.getType() != null && !publisherHasType) {
						testMap.get(NO_PUB)
								.add(new TestResult(LogStatus.WARNING,
										String.format(
												"FeedID: <b>%s</b> has type, but the publisher: <b>%s</b> does not have",
												link.getFeed().id, singlePublisher.id)));

					} else if (link.getType() != null && publisherHasType && !typeFound) {
						 testMap.get(DIFF_TYPES).add(new TestResult(LogStatus.PASS,
									String.format(
											"Diffrent tyeps for the Feed: <b>%s</b>, and for the publisher: <b>%s</b> Feed type: <b>%s</b>, publisher type: <b>%s</b>",
											link.getFeed().id, singlePublisher.id,link.getType(), GetPusblisherTypesString(types))));
						 
						 
					} else if (typeFound) {
						testMap.get(EQUALS)
								.add(new TestResult(LogStatus.PASS,
										String.format(
												"for the FeedID: <b>%s</b>, the publisher: <b>%s</b> has the correct type: <b>%s</b>",
												link.getFeed().id, singlePublisher.id, GetPusblisherTypesString(types))));
					} else {
						throw new Exception(
								"Not supposed to get here: " + link.getFeed().id + ", " + singlePublisher.id);
					}
				}
			}
		}

		Map<String, String> testNames = GetTestCaseNamesMap();
		for (Map.Entry<String, String> entry : testNames.entrySet()) {
			manager.initTestCase(entry.getValue(), "");

			for (TestResult testResult : testMap.get(entry.getKey())) {
				manager.getTest().log(testResult.status, testResult.message);
			}
		}
	}

	private String GetPusblisherTypesString(List<String> types) {
		StringBuilder builder = new StringBuilder();
		for (String singleType : types) {
			if (singleType != null) {
				builder.append(singleType);
				builder.append(",");
			}
		}
		String line = builder.toString();

		if (line.endsWith(",")) {
			return line.substring(0, line.length() - 1);
		}

		return line;
	}
}