package testFunctions.FeedRouterRelatedTests;

import java.util.ArrayList;
import java.util.List;
import com.relevantcodes.extentreports.LogStatus;
import XMLParser.DomainDefault;
import XMLParser.DownloadProvider;
import XMLParser.FeedRouter;
import XMLParser.Publisher;
import XMLParser.RoutingDefinition;
import XMLParser.RoutingParameter;
import report.TestResult;
import report.reportManager;
import utils.Utilities;

public class PublisherTests {

	public reportManager manager;

	public PublisherTests(reportManager report) {
		this.manager = report;
	}

	public void defaultPublisherValue_VS_IdValue(FeedRouter feedRouterValues) {

		List<DomainDefault> defaultDomains = feedRouterValues.domainDefaults;
		List<Publisher> publishers = feedRouterValues.publishers;
		List<TestResult> testCollections = new ArrayList<TestResult>();

		for (DomainDefault defaultPublisherValue : defaultDomains) {
			boolean foundPublisher = false;
			boolean foundDp = false;
			for (Publisher publisher : publishers) {
				if (defaultPublisherValue.DefaultPub.equals(publisher.id)) {
					foundPublisher = true;
					for (DownloadProvider dp : publisher.downloadProviders) {
						if (dp.DPID.equals(defaultPublisherValue.defaultDownloadProvider)) {
							foundDp = true;
							break;
						} else {
							testCollections.add(new TestResult(LogStatus.FAIL,
									String.format(String.format(
											"For the publisher: <b>%s</b>, the DPID value does not match to the DefaultProvider",
											publisher.id))));
						}
					}
					break;
				}
			}
			if (!foundPublisher || !foundDp) {
				testCollections.add(new TestResult(LogStatus.FAIL, String.format(
						"No publisher was defined for the default domain: <b>%s</b>. ", defaultPublisherValue.Domain)));

			}
		}
		manager.initTestCase("DefaultPublisher VS PublisherId", "");
		for (TestResult testResult : testCollections) {
			manager.getTest().log(testResult.status, testResult.message);
		}
	}

	public void routingDefinitionRerunFeedVlaue_VS_PublisherID(FeedRouter feedRouterValues) {

		List<Publisher> allPublishers = feedRouterValues.publishers;
		List<TestResult> testCollections = new ArrayList<TestResult>();

		for (Publisher singlePublisher : allPublishers) {
			for (DownloadProvider downloadProvider : singlePublisher.downloadProviders) {
				if (downloadProvider.routingParameters != null) {
					for (RoutingParameter routingParameter : downloadProvider.routingParameters) {
						if (routingParameter.routingDefinitions != null) {
							for (RoutingDefinition routingDefinition : routingParameter.routingDefinitions) {
								if (routingDefinition.RerunFeed == null || routingDefinition.RerunFeed.isEmpty()) {
									continue;
								}
								if (Utilities.idEquals(allPublishers, routingDefinition.RerunFeed)) {
									break;
								} else {
									testCollections.add(new TestResult(LogStatus.FAIL,
											String.format(
													"For the RerunFeed value: <b>%s</b>, Publisher Id didnt found",
													routingDefinition.RerunFeed)));
								}
							}
						}
					}
				}
			}
		}
		manager.initTestCase("RerunFeed VS PublisherId", "");
		for (TestResult testResult : testCollections) {
			manager.getTest().log(testResult.status, testResult.message);
		}
	}
}