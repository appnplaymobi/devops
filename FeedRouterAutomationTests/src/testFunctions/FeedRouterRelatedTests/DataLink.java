package testFunctions.FeedRouterRelatedTests;

import java.util.Map;

import XMLParser.Feed;

public class DataLink {
	
	private Map<String, String> queryString;
	private Feed feed;
	private String publisher;
	private String dpId;
	private String type;
	
	public DataLink(Feed feed, Map<String, String> query)
	{
		this.feed = feed;
		queryString = query;
		
		publisher = query.get("publisher");
		dpId = query.get("dpid");
		type = query.get("type");
	}
	
	public Map<String, String> getQuery() {
		return queryString;
	}
	
	public Feed getFeed() {
		return feed;
	}
	
	public String getPublisher() {
		return publisher;
	}
	
	public String getDpid() {
		return dpId;
	}
	
	public String getType() {
		return type;
	}
	
}
