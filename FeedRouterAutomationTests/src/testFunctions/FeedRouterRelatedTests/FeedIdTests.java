package testFunctions.FeedRouterRelatedTests;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.microsoft.azure.storage.StorageException;
import com.relevantcodes.extentreports.LogStatus;

import XMLParser.DownloadProvider;
import XMLParser.Feed;
import XMLParser.FeedRouter;
import XMLParser.Publisher;
import XMLParser.RoutingDefinition;
import XMLParser.RoutingParameter;
import report.TestResult;
import report.reportManager;
import utils.Utilities;

public class FeedIdTests {

	public reportManager manager;

	public FeedIdTests(reportManager report) {
		this.manager = report;
	}

	public void defaultFeedValue_VS_FeedID(FeedRouter feedRouterValues)
			throws URISyntaxException, StorageException, IOException {

		List<Feed> allFeeds = feedRouterValues.feeds;
		List<Publisher> allPublishers = feedRouterValues.publishers;
		List<TestResult> testCollections = new ArrayList<TestResult>();

		for (Publisher singlePublisher : allPublishers) {
			for (DownloadProvider singleDownloadProvider : singlePublisher.downloadProviders) {
				boolean foundDf = false;
				for (Feed singleFeed : allFeeds) {
					if (singleDownloadProvider.DefaultFeed.equals(singleFeed.id)) {
						foundDf = true;
						break;
					}
				}
				if (!foundDf) {
					testCollections.add(new TestResult(LogStatus.FAIL,
							String.format("for the DefaultFeed: <b>%s</b>, no feed id that matches results",
									singleDownloadProvider.DefaultFeed)));
				}
			}
		}
		manager.initTestCase("DefaultFeed VS FeedId", "");
		for (TestResult testResult : testCollections) {
			manager.getTest().log(testResult.status, testResult.message);
		}
	}

	private static Map<String, String> hashMapQuery(String query, String[] seperators) {
		Map<String, String> result = new HashMap<String, String>();
		if (query != null) {
			String[] allQuery = query.split(seperators[0]);
			for (String singleQueryString : allQuery) {
				String[] pair = singleQueryString.split(seperators[1]);

				String val = null;
				if (pair.length == 2) {
					val = pair[1];
				}
				result.put(pair[0], val);
			}
		}
		return result;
	}

	public static Map<String, String> separateQuery(String query) {
		return hashMapQuery(query, new String[] { ":", ";" });
	}
	
	public void feedId_VS_RangeToFeed(FeedRouter feedRouterValues) {
		List<Feed> allFeeds = feedRouterValues.feeds;
		List<Publisher> allPublishers = feedRouterValues.publishers;
		List<TestResult> testCollections = new ArrayList<TestResult>();
		
		for (int i = 0; i < allPublishers.size(); i++) {
			Publisher singlePublisher = allPublishers.get(i);
			for (DownloadProvider singleDownloadProvider : singlePublisher.downloadProviders) {
				if (singleDownloadProvider.RangeToFeed != null && singlePublisher.id != null) {
					String[] q = singleDownloadProvider.RangeToFeed.split("\\{");
					String e = q[1].replace("}", "");
					Map<String, String> countryToChannel = hashMapQuery(e, new String[] { ";", ":" });
					for (Map.Entry<String, String> entry : countryToChannel.entrySet()) {
						if (!Utilities.idEquals(allFeeds, entry.getValue())) {
							testCollections.add(new TestResult(LogStatus.FAIL, String.format(
									"for the RangeToFeed parameter: <b>%s</b>, that under the: <b>%s</b> publisher no Feed value where found", entry.getValue(), singlePublisher.id)));
						}
					}
				}
			}
		}
		manager.initTestCase("FeedId VS RangeToFeed", "");
		for (TestResult testResult : testCollections) {
			manager.getTest().log(testResult.status, testResult.message);
		}
	}

	public void feedId_VS_DefaultFeedValue(FeedRouter feedRouterValues)
			throws URISyntaxException, StorageException, IOException {
		List<Feed> allFeeds = feedRouterValues.feeds;
		List<Publisher> allPublishers = feedRouterValues.publishers;
		List<TestResult> testCollections = new ArrayList<TestResult>();

		for (Feed singleFeed : allFeeds) {
			boolean foundFiD = false;
			for (int i = 0; i < allPublishers.size() && !foundFiD; i++) {
				Publisher singlePublisher = allPublishers.get(i);

				for (DownloadProvider singleDownloadProvider : singlePublisher.downloadProviders) {
					if (singleFeed.id.equals(singleDownloadProvider.DefaultFeed)) {
						foundFiD = true;
						break;
					}
					if (singleDownloadProvider.routingParameters != null) {
						for (int j = 0; j < singleDownloadProvider.routingParameters.size() && !foundFiD; j++) {
							RoutingParameter routingParameter = singleDownloadProvider.routingParameters.get(j);
							if (routingParameter.routingDefinitions != null) {
								for (RoutingDefinition routingDefinition : routingParameter.routingDefinitions) {
									if ((routingDefinition.OverrideFeed != null
											&& !routingDefinition.OverrideFeed.isEmpty())
											&& routingDefinition.OverrideFeed.equals(singleFeed.id)) {
										foundFiD = true;
										break;
									}
								}
							}
						}
					}
				}
			}
			if (!foundFiD) {
				testCollections.add(new TestResult(LogStatus.FAIL, String.format(
						"for the FeedID: <b>%s</b>, no DefaultFeed value and no OverrideFeed value ", singleFeed.id)));
			}
		}
		manager.initTestCase("FeedId VS DefaultFeed without OverrideFeed", "");
		for (TestResult testResult : testCollections) {
			manager.getTest().log(testResult.status, testResult.message);
		}

	}

	public void routingDefinitionOverrideFeedVlaue_VS_FeedID(FeedRouter feedRouterValues)
			throws URISyntaxException, StorageException, IOException {

		List<Feed> allFeeds = feedRouterValues.feeds;
		List<Publisher> allPublishers = feedRouterValues.publishers;
		List<TestResult> testCollections = new ArrayList<TestResult>();

		for (Publisher singlePublisher : allPublishers) {
			for (DownloadProvider downloadProvider : singlePublisher.downloadProviders) {
				if (downloadProvider.routingParameters != null) {
					for (RoutingParameter routingParameter : downloadProvider.routingParameters) {
						if (routingParameter.routingDefinitions != null) {
							boolean foundRf = false;
							for (RoutingDefinition routingDefinition : routingParameter.routingDefinitions) {
								if (routingDefinition.OverrideFeed != null) {
									for (Feed singleFeed : allFeeds) {
										if (routingDefinition.OverrideFeed.equals(singleFeed.id)) {
											foundRf = true;
											break;
										}
									}
									if (!foundRf && !routingDefinition.OverrideFeed.isEmpty()) {
										testCollections.add(new TestResult(LogStatus.FAIL,
												String.format(
														"For the OverrideFeed value: <b>%s</b>, that locat under: <b>%s</b> publisher, FeedId didnt found",
														routingDefinition.OverrideFeed, singlePublisher.id)));
									}
								}
							}
						}
					}
				}
			}
		}
		manager.initTestCase("OverrideFeed VS FeedId", "");
		for (TestResult testResult : testCollections) {
			manager.getTest().log(testResult.status, testResult.message);
		}
	}

}
