package mailReports;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.mail.EmailException;
import org.xml.sax.SAXException;
import externalFiles.manageExternalFiles;

public class MailSending {

	public static void createAndSendMailResults(String mailContent, String mailSubject,
			List<String> sendToRecipientsForScanReport)
			throws EmailException, MessagingException, ParserConfigurationException, SAXException, IOException {

		String _sender = manageExternalFiles.getDataFromXML("sender");
		String _password = manageExternalFiles.getDataFromXML("password");

		Properties properties = MailFunctions._mailProperties(_sender, _password);

		Session mailSession = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(properties.getProperty("mail.smtp.user"),
						properties.getProperty("mail.smtp.password"));
			}
		});

		InternetAddress[] address = new InternetAddress[sendToRecipientsForScanReport.size()];
		for (int i = 0; i < sendToRecipientsForScanReport.size(); i++) {
			address[i] = new InternetAddress(sendToRecipientsForScanReport.get(i));
		}

		Multipart multiPart = MailFunctions.createMultipart();

		Message message = MailFunctions._mailMessage(mailSession, address, _sender, mailSubject, multiPart);

		MimeBodyPart htmlPart = new MimeBodyPart();

		htmlPart.setContent(mailContent, "text/html");
		multiPart.addBodyPart(htmlPart);

		Transport.send(message);
	}

}
