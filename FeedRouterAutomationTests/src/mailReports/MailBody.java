package mailReports;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import externalFiles.manageExternalFiles;
import pathManager.SetPath;

public class MailBody {

	public static String setContent() throws ParserConfigurationException, SAXException, IOException {

		SetPath pathTo = new SetPath();
		
		String ExtensionsLpReport = manageExternalFiles.getDataFromXML("ExtensionsLpReportName");
		String ExtensionsLpReportPath = pathTo.getReportPathInAsure(ExtensionsLpReport);
		System.out.println("ExtensionsLpReportPath: " + ExtensionsLpReportPath);
		String ExtensionsLpReportFinalUrl = "<a href=" + ExtensionsLpReportPath + ">" + ExtensionsLpReportPath + "</a><br>";
		String ExtensionsLpReportMsg = setMessageColor("<b>Extensions Feed Router test result: </b><br> ", "black") + ExtensionsLpReportFinalUrl;
		
		String FeedRouterReport = manageExternalFiles.getDataFromXML("FeedRouterReportName");
		String FeedRouterReportPath = pathTo.getReportPathInAsure(FeedRouterReport);
		String FeedRouterReportFinalUrl = "<a href=" + FeedRouterReportPath + ">" + FeedRouterReportPath + "</a>";
		String FeedRouterReportMsg = setMessageColor("<b>Feed Router test result: </b><br> ", "black") + FeedRouterReportFinalUrl;
		
		String mesageContent = ExtensionsLpReportMsg + FeedRouterReportMsg;
		return mesageContent;
	}

	public static String setMessageColor(String message, String color) {
		String MessageColor = "<font color=" + color + ">" + message + "</font>";
		return MessageColor;
	}
}