package pathManager;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import externalFiles.manageExternalFiles;
import report.ReportToAsure;
import utils.Utilities;

public class SetPath {

	private static String _reportedFolder;
	private Utilities tools;

	public SetPath() {
		tools = new Utilities();
		_reportedFolder = "reports/FeedRouterReport/";
	}

	public String getReportFilePath(String reportFileName)
			throws ParserConfigurationException, SAXException, IOException {
		String retpotPath = manageExternalFiles.getDataFromXML("reportPath");
		String todayDate = tools.returnTodayDate();
		String fullPathToReport = retpotPath + _reportedFolder + todayDate + reportFileName;
		return fullPathToReport;
	}

	public String getReportPathInAsure(String reportFileName)
			throws ParserConfigurationException, SAXException, IOException {
		String retpotPath = manageExternalFiles.getDataFromXML("AzureBlob");
		String todayDate = tools.returnTodayDate();
		String pathToReport = retpotPath + _reportedFolder + todayDate + reportFileName;
		return pathToReport;
	}

	public void preperReport(String reportFileName)
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		String reportFilePathToload = getReportFilePath(reportFileName);
		Path getFileName = Paths.get(reportFilePathToload);
		String pathToUploadTo = _reportedFolder + tools.returnTodayDate() + "/" + getFileName.getFileName();
		ReportToAsure.uploadToAzure(pathToUploadTo, reportFilePathToload);
	}
}