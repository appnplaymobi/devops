package externalFiles;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.apache.commons.io.FileUtils;
import com.microsoft.azure.storage.StorageException;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import pathManager.SetPath;


public class manageExternalFiles {

	private static SetPath pathTo = new SetPath();

	public static String getDataFromXML(String elementName)
			throws ParserConfigurationException, SAXException, IOException {
		File fXmlFile = new File("D:/Bitbucket/devops/FeedRouterAutomationTests/src/externalFiles/feedRouter.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		return doc.getElementsByTagName(elementName).item(0).getTextContent();
	}

	public static void uploadReport(String ReportName)
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		pathTo.preperReport(ReportName);
	}

	public void downLoadFile(String fromFile, String toFile) throws MalformedURLException, IOException {
		try {
			FileUtils.copyURLToFile(new URL(fromFile), new File(toFile),  60000, 60000);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void downLoadFeedRouterXmlFile() throws MalformedURLException, IOException, ParserConfigurationException, SAXException{
		String from = manageExternalFiles.getDataFromXML("xmlAzurePath");
		String to = manageExternalFiles.getDataFromXML("xmlLocalPath");
		downLoadFile(from, to);
	}

}