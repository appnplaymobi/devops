package mainPkg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import HTMLParser.feedRouterWikiParser;
import HTMLParser.feedRouterWikiRow;
import XMLParser.FeedRouter;
import XMLParser.Parser;
import XMLParser.Publisher;
import report.TestResult;

public class PidTest {

	public static void main(String[] args)
			throws ParserConfigurationException, SAXException, IOException, JAXBException {

		FeedRouter unmarshalXML = Parser.unmarshalFeedRouterXmlFile();
		comparePidFromSiteToPidFromXml(unmarshalXML);

	}

	public static void comparePidFromXmlToPidFromSite(FeedRouter feedRouterValues)
			throws IOException, ParserConfigurationException, SAXException {
//		List<TestResult> testCollections = new ArrayList<TestResult>();
		List<Publisher> allPublishers = feedRouterValues.publishers;
		List<feedRouterWikiRow> resultList = feedRouterWikiParser.parse();
		for (Publisher singlePublisher : allPublishers) {
			if (singlePublisher.publisherid != null) {
				boolean foundPublisherId = false;
				for (feedRouterWikiRow items : resultList) {
					if (singlePublisher.publisherid.equals(items.getPid())) {
						foundPublisherId = true;
						break;
					}
				}
				if (!foundPublisherId) {
					System.out.println(String.format("The publisherId: <b>%s</b>, exist in the FeedRouter XML but not exist in the LP site",
							singlePublisher.publisherid));
				}
			}
		}
	}

	public static void comparePidFromSiteToPidFromXml(FeedRouter feedRouterValues)
			throws IOException, ParserConfigurationException, SAXException {
		List<Publisher> allPublishers = feedRouterValues.publishers;
		List<feedRouterWikiRow> wikiPidList = feedRouterWikiParser.parse();
		for (feedRouterWikiRow wikiPidRow : wikiPidList) {
			boolean foundPublisherId = false;
			for (Publisher singlePublisher : allPublishers) {
				if (singlePublisher.publisherid != null && wikiPidRow.getPid().equals(singlePublisher.publisherid)) {
					foundPublisherId = true;
					break;
				}
			}

			if (!foundPublisherId) {
				System.out.println(String.format("The publisherId: <b>%s</b>, exist in the LP site but not exist in the FeedRouter XML",
						wikiPidRow.getPid()));
			}
		}
	}
}