package mainPkg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class sss {

	public static void main(String[] args) {		
		String str = "0-99:{AU:clickd;CA:clickd;DE:clickd;ES:clickd;FR:clickd;GB:InfoSpaceSoftonicUS;IN:clickd;IT:clickd;JP:clickd;NL:clickd;NZ:clickd;US:clickd}";
		String[] q = str.split("\\{");
		String e = q[1].replace("}", "");
		
		Map<String, String> countryToChannel = hashMapQuery(e, new String[] { ";", ":" });
		
		for (Map.Entry<String, String> entry : countryToChannel.entrySet())
		{
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
		
	}
	
	public static Map<String, String> separateQuery(String query) {
		return hashMapQuery(query, new String[] { ":", ";" });
	}
	
	private static Map<String, String> hashMapQuery(String query, String[] seperators) {
		Map<String, String> result = new HashMap<String, String>();
		if (query != null) {
			String[] allQuery = query.split(seperators[0]);
			for (String singleQueryString : allQuery) {
				String[] pair = singleQueryString.split(seperators[1]);

				String val = null;
				if (pair.length == 2) {
					val = pair[1];
				}
				result.put(pair[0], val);
			}
		}
		return result;
	}
	
	public static void asd(String str){
		
		List<String> ss = new ArrayList<String>();
		String[] s = str.split(":\\{");
		for (String d : s){
			String[] pair = d.split(";");
			if (pair.length == 2){
				ss.add(pair[0]);
				
			}
		}
		for (String f : ss){
			System.out.println(f);
		}		
	}
}
