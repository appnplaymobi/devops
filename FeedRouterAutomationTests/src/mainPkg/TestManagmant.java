package mainPkg;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import javax.mail.MessagingException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.mail.EmailException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import XMLParser.FeedRouter;
import XMLParser.FeedToPublisher;
import XMLParser.Parser;
import browsersDefinition.driversDefinitions;
import externalFiles.manageExternalFiles;
import mailReports.MailFunctions;
import pathManager.SetPath;
import report.ReportToAsure;
import report.reportManager;
import testFunctions.FeedRouterRelatedTests.CompareXmlToSite;
import testFunctions.FeedRouterRelatedTests.FeedIdMatchingType;
import testFunctions.FeedRouterRelatedTests.FeedIdTests;
import testFunctions.FeedRouterRelatedTests.PublisherTests;
import testFunctions.ExtentionsLP.ExtractingLPurls;

import org.junit.runners.MethodSorters;
import org.junit.FixMethodOrder;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class TestManagmant {

	private static reportManager manager;
	private static SetPath pathTo = new SetPath();

	@BeforeClass
	public static void startTestSession() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {
		driversDefinitions.initChromeDriver();
		ReportToAsure.initAzureConnection();
	}

	@Test
	public void test01_parsingAndTestingExtensionsLP() throws IOException, ParserConfigurationException, SAXException,
			InterruptedException, URISyntaxException, StorageException {

		String reportFileName = manageExternalFiles.getDataFromXML("ExtensionsLpReportName");
		manager = new reportManager(pathTo.getReportFilePath(reportFileName));

		ExtractingLPurls pars = new ExtractingLPurls(manager);
		pars.getUrlAndTest();
		
		manager.endTestReport();
		manager.flushTest();
		manageExternalFiles.uploadReport(reportFileName);
		driversDefinitions.quitDriver();
	}

	@Test
	public void test02_parsingAndTestingFeedRouterXML() throws Exception {

		manageExternalFiles usingFiles = new manageExternalFiles();
		usingFiles.downLoadFeedRouterXmlFile();

		String reportFileName = manageExternalFiles.getDataFromXML("FeedRouterReportName");
		manager = new reportManager(pathTo.getReportFilePath(reportFileName));

		FeedRouter unmarshalXML = Parser.unmarshalFeedRouterXmlFile();

		FeedIdTests feedTests = new FeedIdTests(manager);
		feedTests.feedId_VS_RangeToFeed(unmarshalXML);
		feedTests.defaultFeedValue_VS_FeedID(unmarshalXML);
		feedTests.feedId_VS_DefaultFeedValue(unmarshalXML);
		feedTests.routingDefinitionOverrideFeedVlaue_VS_FeedID(unmarshalXML);

		PublisherTests publisherTests = new PublisherTests(manager);
		publisherTests.defaultPublisherValue_VS_IdValue(unmarshalXML);
		publisherTests.routingDefinitionRerunFeedVlaue_VS_PublisherID(unmarshalXML);

		FeedToPublisher.feedIdToPublisherList(unmarshalXML);
		FeedIdMatchingType matchType = new FeedIdMatchingType(manager);
		matchType.typeMatcher(unmarshalXML);
		
		CompareXmlToSite compare = new CompareXmlToSite(manager);
		compare.comparePidFromXmlToPidFromSite(unmarshalXML);
		compare.comparePidFromSiteToPidFromXml(unmarshalXML);

		manager.endTestReport();
		manager.flushTest();
		manageExternalFiles.uploadReport(reportFileName);
	}

	@Test
	public void test03_mail_sending()
			throws SAXException, IOException, ParserConfigurationException, EmailException, MessagingException {
		MailFunctions.sendMailAfterTestEnds();
	}
}