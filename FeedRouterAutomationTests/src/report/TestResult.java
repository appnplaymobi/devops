package report;

import com.relevantcodes.extentreports.LogStatus;

public class TestResult {
	
	public LogStatus status;
	
	public String message;
	
	public TestResult(LogStatus status, String message){
		this.status = status;
		this.message = message;
	}

}
