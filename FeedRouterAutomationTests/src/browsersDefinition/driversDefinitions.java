package browsersDefinition;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.xml.sax.SAXException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import externalFiles.manageExternalFiles;
import webElements.Elements;

public class driversDefinitions {

	private static WebDriver driver;

	public static void initChromeDriver() throws ParserConfigurationException, SAXException, IOException {
		String chromeDriver = manageExternalFiles.getDataFromXML("chromeDriverPath");
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		driver = new ChromeDriver();
	}

	public static void waitForLoad() {
		new WebDriverWait(driver, 30).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
	}

	public static void getPage(String pageUrl)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		System.out.println("testd url: " + pageUrl);
		driver.get(pageUrl);
		deleteCookeisAndLogs();
	}

	public static String getPageUrl() {
		String pageUrl = driver.getCurrentUrl();
		return pageUrl;
	}

	public static void deleteCookeisAndLogs() {
		driver.manage().logs().get(LogType.BROWSER).getAll();
		driver.manage().deleteAllCookies();
	}

	public static void quitDriver() {
		driver.quit();
	}
	
	public static String getUrlFromJson(){
		try{
			String jsonString = Elements.preSelector(driver);
			Gson gson = new Gson();
			JsonElement element = gson.fromJson(jsonString, JsonElement.class);
			JsonObject jsonObj = element.getAsJsonObject();
			return jsonObj.getAsJsonPrimitive("url").getAsString();	
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return null;
	}
}