package manageUrls;

import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import HTMLParser.feedRouterWikiParser;
import HTMLParser.feedRouterWikiRow;
import externalFiles.manageExternalFiles;

public class UrlBuilder {

	public static String buildUrl(String pid) throws ParserConfigurationException, SAXException, IOException {
		String baseUrlDeclaration = manageExternalFiles.getDataFromXML("baseUrl");
		String publisherDeclaration = manageExternalFiles.getDataFromXML("publisher");

		String finalUrl = baseUrlDeclaration + publisherDeclaration + pid;
		return finalUrl;
	}

	public static List<feedRouterWikiRow> extractUrl() throws ParserConfigurationException, SAXException, IOException {
		List<feedRouterWikiRow> resultList = feedRouterWikiParser.parse();
		for (feedRouterWikiRow items : resultList) {
			String urlToTest = buildUrl(items.getPid());
			items.setUrl(urlToTest);
		}
		return resultList;
	}
}