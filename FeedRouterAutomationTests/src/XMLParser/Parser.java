package XMLParser;

import java.io.FileReader;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import externalFiles.manageExternalFiles;

public class Parser {

	public static FeedRouter unmarshalFeedRouterXmlFile()
			throws JAXBException, ParserConfigurationException, SAXException, IOException {
		String pathToXml = manageExternalFiles.getDataFromXML("xmlLocalPath");
		JAXBContext context = JAXBContext.newInstance(FeedRouter.class);
		Unmarshaller um = context.createUnmarshaller();
		FeedRouter feedRouter = (FeedRouter) um.unmarshal(new FileReader(pathToXml));
		return feedRouter;
	}
}
