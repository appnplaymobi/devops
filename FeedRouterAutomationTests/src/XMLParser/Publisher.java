package XMLParser;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class Publisher implements IdFeedObject{
	
	@XmlAttribute(name = "id")
	public String id;
	@XmlAttribute(name = "publisherid")
	public String publisherid;
	
	@XmlElementWrapper(name = "DownloadProviderList")
	@XmlElement(name = "DownloadProvider")
	public List<DownloadProvider> downloadProviders;

	@Override
	public String getId() {
		return id;
	}


	@Override
	public String getDefaultPub() {
		return null;
	}

}
