package XMLParser;

import javax.xml.bind.annotation.XmlAttribute;

public class Feed implements IdFeedObject{
	
	@XmlAttribute(name = "id")
	public String id;
	@XmlAttribute(name = "Url")
	public String Url;
	@XmlAttribute(name = "QueryParamsMatching")
	public String QueryParamsMatching;
	@XmlAttribute(name = "QueryParamsSynMap")
	public String QueryParamsSynMap;
	@XmlAttribute(name = "EncryptQueryParams")
	public String EncryptQueryParams;
	
	@Override
	public String getId() {
		return id;
	}


	@Override
	public String getDefaultPub() {
		return null;
	}
}
