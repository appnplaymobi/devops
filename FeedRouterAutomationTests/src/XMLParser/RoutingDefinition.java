package XMLParser;

import javax.xml.bind.annotation.XmlAttribute;

public class RoutingDefinition {

	@XmlAttribute(name = "ParameterValue")
	public String ParameterValue;
	@XmlAttribute(name = "OverrideFeed")
	public String OverrideFeed;
	@XmlAttribute(name = "CountryList")
	public String CountryList;
	@XmlAttribute(name = "QueryStringAddition")
	public String QueryStringAddition;
	@XmlAttribute(name = "IsReferrerOverride")
	public String IsReferrerOverride;
	@XmlAttribute(name = "RerunFeed")
	public String RerunFeed;
	@XmlAttribute(name = "Range")
	public String Range;
	@XmlAttribute(name = "IsAutoProtocol")
	public String IsAutoProtocol;
}