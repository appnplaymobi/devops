package XMLParser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.microsoft.azure.storage.StorageException;
import XMLParser.DownloadProvider;
import XMLParser.Feed;
import XMLParser.FeedRouter;
import XMLParser.Publisher;
import XMLParser.RoutingDefinition;
import XMLParser.RoutingParameter;

public class FeedToPublisher {

	public static Map<String, List<Publisher>> feedIdToPublisherList(FeedRouter feedRouterValues)
			throws URISyntaxException, StorageException, IOException {
		List<Feed> allFeeds = feedRouterValues.feeds;
		List<Publisher> allPublishers = feedRouterValues.publishers;
		Map<String, List<Publisher>> feedToPublisher = new HashMap<String, List<Publisher>>();

		for (Feed singleFeed : allFeeds) {
			boolean foundFiD = false;
			for (int i = 0; i < allPublishers.size() && !foundFiD; i++) {
				Publisher singlePublisher = allPublishers.get(i);

				for (DownloadProvider singleDownloadProvider : singlePublisher.downloadProviders) {
					if (singleFeed.id != null && singleFeed.id.equals(singleDownloadProvider.DefaultFeed)) {
						AddToMap(feedToPublisher, singleFeed.id, singlePublisher);
					}
					if (singleDownloadProvider.RangeToFeed != null) {

						if (singleDownloadProvider.RangeToFeed.contains(singleFeed.id)) {
							AddToMap(feedToPublisher, singleFeed.id, singlePublisher);
							foundFiD = true;
							break;
						}

					}
					if (singleDownloadProvider.routingParameters != null) {
						for (int j = 0; j < singleDownloadProvider.routingParameters.size() && !foundFiD; j++) {
							RoutingParameter routingParameter = singleDownloadProvider.routingParameters.get(j);
							if (routingParameter.routingDefinitions != null) {
								for (RoutingDefinition routingDefinition : routingParameter.routingDefinitions) {
									if ((routingDefinition.OverrideFeed != null
											&& !routingDefinition.OverrideFeed.isEmpty())
											&& routingDefinition.OverrideFeed.equals(singleFeed.id)) {
										foundFiD = true;
										AddToMap(feedToPublisher, singleFeed.id, singlePublisher);
										break;
									}
								}
							}
						}

					}
				}
			}
		}
		return feedToPublisher;
	}

	private static void AddToMap(Map<String, List<Publisher>> feedToPublisher, String feedId, Publisher pub) {
		List<Publisher> feedPublishers = feedToPublisher.get(feedId);

		if (feedPublishers == null) {
			List<Publisher> pubs = new ArrayList<Publisher>();
			pubs.add(pub);

			feedToPublisher.put(feedId, pubs);
		} else {
			feedPublishers.add(pub);
		}
	}
}
