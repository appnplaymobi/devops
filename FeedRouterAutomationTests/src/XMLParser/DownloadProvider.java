package XMLParser;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class DownloadProvider implements IdFeedObject{
	
	@XmlAttribute(name = "DPID")
	public String DPID;
	@XmlAttribute(name = "DefaultFeed")
	public String DefaultFeed;
	@XmlAttribute(name = "CountryToFeed")
	public String CountryToFeed;
	@XmlAttribute(name = "SearchProviderToFeed")
	public String SearchProviderToFeed;
	@XmlAttribute(name = "RangeToFeed")
	public String RangeToFeed;
	@XmlAttribute(name = "IgnoreParams")
	public String IgnoreParams;
	@XmlAttribute(name = "IgnoreRouterParams")
	public String IgnoreRouterParams;
	
	@XmlElementWrapper(name = "RoutingParametersList")
	@XmlElement(name = "RoutingParameter")
	public List<RoutingParameter> routingParameters;

	@Override
	public String getId() {
		return null;
	}

	@Override
	public String getDefaultPub() {
		return null;
	}
}
