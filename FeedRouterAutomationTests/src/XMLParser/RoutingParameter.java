package XMLParser;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class RoutingParameter {

	@XmlAttribute(name = "ID")
	public String ID;
	@XmlAttribute(name = "ParameterName")
	public String ParameterName;
	@XmlAttribute(name = "ParameterType")
	public String ParameterType;
	@XmlAttribute(name = "ComparisonOperator")
	public String ComparisonOperator;
	@XmlAttribute(name = "Enter")
	public String Enter;
	
	
	@XmlElementWrapper(name = "RoutingDefinitionsList")
	@XmlElement(name = "RoutingDefinition")
	public List<RoutingDefinition> routingDefinitions;
}