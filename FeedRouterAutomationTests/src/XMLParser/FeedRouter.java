package XMLParser;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FeedRouter")
public class FeedRouter {
	
	@XmlElementWrapper(name = "DomainDefaultsList")
	@XmlElement(name = "DomainDefault")
	public List<DomainDefault> domainDefaults;
	
	@XmlElementWrapper(name = "RoutingParametersList")
	@XmlElement(name = "RoutingParameter")
	public List<RoutingParameter> routingParameters;
	
	@XmlElementWrapper(name = "PublisherList")
	@XmlElement(name = "Publisher")
	public List<Publisher> publishers;
	
	@XmlElementWrapper(name = "FeedList")
	@XmlElement(name = "Feed")
	public List<Feed> feeds;
}
