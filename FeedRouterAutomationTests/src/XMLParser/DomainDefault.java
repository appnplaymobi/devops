package XMLParser;

import javax.xml.bind.annotation.XmlAttribute;

public class DomainDefault implements IdFeedObject{

	@XmlAttribute(name = "Domain")
	public String Domain;
	@XmlAttribute(name = "DefaultPublisher")
	public String DefaultPub;
	@XmlAttribute(name = "DefaultDownloadProvider")
	public String defaultDownloadProvider;
	
	@Override
	public String getId() {
		return null;
	}
	
	@Override
	public String getDefaultPub() {
		return DefaultPub;
	}
}
