package webElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Elements {
	
	public static String preSelector(WebDriver driver){
		try {
			WebElement element = driver.findElement(By.tagName("pre"));
			return element.getText();
		} catch (Exception e) {
			return null;
		}		
	}
	
}
