package HTMLParser;

public class tableColumnData {
	private int channelIndex;
	private int pidIndex;

	tableColumnData(int channelIndex, int pidIndex) {
		this.channelIndex = channelIndex;
		this.pidIndex = pidIndex;
	}

	public int getChannelIndex() {
		return channelIndex;
	}

	public int getPidIndex() {
		return pidIndex;
	}
}