package HTMLParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.SAXException;
import externalFiles.manageExternalFiles;

public class feedRouterWikiParser {

	public static List<feedRouterWikiRow> parse() throws IOException, ParserConfigurationException, SAXException {
		String urlToConnect = manageExternalFiles.getDataFromXML("urlToParse");
		List<feedRouterWikiRow> feedResults = new ArrayList<feedRouterWikiRow>();
		
		/*Response html = Jsoup.connect(urlToConnect).execute();
		File f = new File("D:\\filename.html");
		FileUtils.writeStringToFile(f, html.body(), "UTF-8");*/
		
		Document doc = Jsoup.connect(urlToConnect).get();
		
		/*File input = new File("D:\\filename2.html");
		Document doc = Jsoup.parse(input, "UTF-8", urlToConnect);*/

		Elements tables = doc.select(".wikitable");
		for (Element table : tables) {
			Elements rows = table.select("tr");
			if (rows.size() > 1) {
				Element firstRow = rows.first();
				Elements headers = firstRow.select("th");
				tableColumnData data = getTableColumnData(headers);

				if (data != null) {
					for (int i = 1; i < rows.size(); i++) {
						Element row = rows.get(i);
//						Elements cells = row.select("th");
						Elements cells = row.children();

						if (cells.size() > Math.max(data.getChannelIndex(), data.getPidIndex())) {
							String pid = cells.get(data.getPidIndex()).text().trim();
							String channel = cells.get(data.getChannelIndex()).text().trim();
							if (!pid.isEmpty() && !channel.isEmpty() && channel.toLowerCase().startsWith("yhs")) {
								feedResults.add(new feedRouterWikiRow(pid, channel));
							}
						}
					}
				}
			}

		}
		return feedResults;
	}

	public static tableColumnData getTableColumnData(Elements headers) {
		int pidIndex = -1, channelIndex = -1, counter = 0;
		for (Element header : headers) {
			String txt = header.text().trim().toLowerCase();

			if (txt.equals("pid")) {
				pidIndex = counter;
			}
			if (txt.contains("channel")) {
				channelIndex = counter;
			}
			counter++;
		}
		if (pidIndex == -1 || channelIndex == -1) {
			return null;
		}
		return new tableColumnData(channelIndex, pidIndex);
	}
}