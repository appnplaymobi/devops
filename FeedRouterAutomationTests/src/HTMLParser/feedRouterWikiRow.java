package HTMLParser;

public class feedRouterWikiRow {

	private final String QUERY = "&q=kuku&searchtype=ds&combofeed=yahoo";
	private final String MOCK_REDIRECT_FLAG = "&_auohr5i5muizcy5=true";

	private String pid;
	private String channel;
	private String url;
	private String queryUrl;

	public feedRouterWikiRow(String pid, String channel) {
		this.pid = pid;
		this.channel = channel;
	}

	public String getPid() {
		return pid;
	}

	public String getChannel() {
		return channel;
	}

	public void setUrl(String newUrl) {
		url = newUrl + MOCK_REDIRECT_FLAG;
		queryUrl = url + QUERY;
	}

	public String getUrl() {
		return url;
	}

	public String getQueryUrl() {
		return queryUrl;
	}
}
