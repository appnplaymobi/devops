package utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import XMLParser.IdFeedObject;
import externalFiles.manageExternalFiles;

public class Utilities {

	public String returnTodayDate() {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		String todayDate = dateFormat.format(now);
		return todayDate;
	}

	public static Document parsedXMLdocument() throws SAXException, IOException, ParserConfigurationException {
		String pathToXml = manageExternalFiles.getDataFromXML("xmlPath");
		File savedXmlFile = new File(pathToXml);

		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document xmlDoc = docBuilder.parse(savedXmlFile);
		return xmlDoc;
	}
	
	public static boolean idEquals(List<? extends IdFeedObject> list, String value) {
		for (IdFeedObject obj : list) {
			if (obj.getId() != null && obj.getId().equals(value)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean dPidEquals(List<? extends IdFeedObject> list, String value) {
		for (IdFeedObject obj : list) {
			if (obj.getDefaultPub() != null && obj.getDefaultPub().equals(value)) {
				return true;
			}
		}
		return false;
	}
}
