package utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import browsersDefinition.driversDefinitions;

public class UrlRelatedFunctions {
	
	public void navigateToPageAndWaitToFinishLoding(String urlToTest)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		driversDefinitions.getPage(urlToTest);
		driversDefinitions.waitForLoad();
		Thread.sleep(300);
	}

	public boolean getIsHostMatch(String url) throws MalformedURLException {
		URL aURL = new URL(url);
		String domainName = aURL.getHost();
		if (domainName.equals("search.yahoo.com")) {
			return true;
		}
		return false;
	}
	
	public String openUrlInNewTab(String urlPage) {
		urlPage = urlPage.replace(" ", "%20");
		String targetBlank = " target=" + '"' + "_blank" + '"';
		String finalUrl = "<a href=" + urlPage + targetBlank + ">" + urlPage + "</a>";
		return finalUrl;
	}

	public String extractChannel(String url) throws MalformedURLException {
		URL resolvedUrl = new URL(url);
		String[] urlQuery = resolvedUrl.getQuery().split("&");
		for (String pairs : urlQuery) {
			String[] parametersAfterSplit = pairs.split("=");
			if (parametersAfterSplit[0].equals("type")) {
				return parametersAfterSplit[1];
			}
		}
		return null;
	}
	
	public String getUrlFromJson() throws MalformedURLException{
		return validateUrl(driversDefinitions.getUrlFromJson());
	}
	
	public static Map<String, String> hashMapQuery(String query) {
		Map<String, String> result = new HashMap<String, String>();
		if (query != null) {
			String[] allQuery = query.split("&");
			for (String singleQueryString : allQuery) {
				String[] pair = singleQueryString.split("=");

				String val = null;
				if (pair.length == 2) {
					val = pair[1];
				}
				result.put(pair[0], val);
			}
		}
		return result;
	}

	public String validateUrl(String url) throws MalformedURLException {
		if (!url.startsWith("http")) {
			url = "http:" + url;
			return url;
		}
		return url;

	}

	public String extractTypeFromUrl(String url, String vlaueToExtract) throws MalformedURLException {
		String validateUrl = validateUrl(url);
		URL aURL = new URL(validateUrl);
		String query = aURL.getQuery();
		Map<String, String> map = hashMapQuery(query);
		String type = map.get(vlaueToExtract);
		return type;
	}
}