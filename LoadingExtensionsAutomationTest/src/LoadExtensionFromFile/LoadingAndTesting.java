package LoadExtensionFromFile;

import java.awt.AWTException;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import configuration.UsingXML;
import manifest.GetValueFromManifest;
import testCases.CaseDefaultSearch;
import testCases.ExtensionErrors;
import usingChromeDriver.UsingDriver;
import usingChromeDriver.UsingJSOnDriver;

public class LoadingAndTesting {

	UsingDriver driver;
	UsingJSOnDriver jsOnDriver;
	ExtensionErrors extensionErrors;
	GetValueFromManifest getValueFromManifest;
	utilties.Utils utils;
	CaseDefaultSearch defaultSearch;
		
	public LoadingAndTesting(UsingDriver driver, ExtensionErrors extensionErrors,
			GetValueFromManifest getValueFromManifest, utilties.Utils utils, CaseDefaultSearch defaultSearch, UsingJSOnDriver jsOnDriver) {
		this.driver = driver;
		this.jsOnDriver = jsOnDriver;
		this.extensionErrors = extensionErrors;
		this.getValueFromManifest = getValueFromManifest;
		this.utils = utils;
		this.defaultSearch = defaultSearch;
		}

	public void loadAndTestExtensionFromFile()
			throws ParserConfigurationException, SAXException, IOException, InterruptedException, AWTException {
		String pathToFolder = UsingXML.getXmlValue("unzipedPath");
		
		List<String> allFoldersNames = utils.getListOfExtensionsNames(UsingXML.getXmlValue("unzipedPath"));
		for (String folderName : allFoldersNames) {
			System.out.println("pathToFolder: " + pathToFolder + "/" + folderName);
			driver.loadExtensionFromFolder(pathToFolder + "/" + folderName);
			Thread.sleep(1000);
			jsOnDriver.waitForPageToLoad();
			Thread.sleep(2000);
			
			extensionErrors.checkForErrorInExtension(folderName);
			driver.quitDriver();
//			String manifestValue = getValueFromManifest.valueFromManifest(folderName);
//			if (manifestValue != null) {
//				defaultSearch.ifHoldingSearch();
//			}
		}
	}
}