package usingChromeDriver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class UsingDriver {

	private static WebDriver driver;
	private static ChromeOptions chOptions;

	public void initDriver() throws ParserConfigurationException, SAXException, IOException {
		String chromeDriver = UsingXML.getXmlValue("chromePath");
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	public WebDriver getDriver() {
		return driver;
	}

	public WebDriver quitDriver() {
		driver.quit();
		return driver;
	}

	public String getCurrentUrl() {
		return driver.getCurrentUrl();
	}

	public ChromeOptions returnChromeOptions() throws ParserConfigurationException, SAXException, IOException {
		String chromeDriverPath = UsingXML.getXmlValue("chromePath");
		System.setProperty("webdriver.chrome.driver", chromeDriverPath);
		chOptions = new ChromeOptions();
		return chOptions;
	}

	public void loadExtensionFromFile(String pathToExtension)
			throws ParserConfigurationException, SAXException, IOException {
		chOptions = returnChromeOptions();
		chOptions.addExtensions(new File(pathToExtension));
		driver = new ChromeDriver(chOptions);
	}

	public void loadExtensionFromFolder(String pathToExtension)
			throws ParserConfigurationException, SAXException, IOException {
		chOptions = returnChromeOptions();
		chOptions.addArguments("--force-dev-mode-highlighting");
		chOptions.addArguments("--load-extension=" + pathToExtension);
		driver = new ChromeDriver(chOptions);

	}

	public List<LogEntry> getDriverLogs(String testedUrl) {
		List<LogEntry> logEntries = driver.manage().logs().get(LogType.BROWSER).getAll();
		return logEntries;
	}

	public ArrayList<String> setFocusOnHandle() {
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		return tabs;
	}
}