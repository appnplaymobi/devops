package usingChromeDriver;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UsingJSOnDriver {

	private UsingDriver driver;

	public UsingJSOnDriver(UsingDriver driver) {
		this.driver = driver;
	}

	public void waitForPageToLoad() {
		new WebDriverWait(driver.getDriver(), 30).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
	}

	public String getExtensionInstalledExtension() throws InterruptedException {
		String randonExtensionId = "var knownId = 'aapnijgdinlhnhlmodcfapnahmbfebeb'; var extensions = "
				+ "document.querySelector('body > extensions-manager').shadowRoot.querySelector('#items-list').shadowRoot.querySelectorAll('extensions-item'); "
				+ "for(var i = 0; i < extensions.length; i++) { if(extensions[i].id !== knownId){ return extensions[i].id; } } return null;";
		if (randonExtensionId.isEmpty()) {
			Thread.sleep(1000);
		}
		return (String) ((JavascriptExecutor) driver.getDriver()).executeScript(randonExtensionId);
	}

	public String getLocalStorageLength() {
		return ((JavascriptExecutor) driver.getDriver()).executeScript("return window.localStorage.length;").toString();
	}

	public void deleteAllCookiesAndRefreshThePage() {
		try {
			driver.getDriver().manage().deleteAllCookies();
			System.out.println("deone delete coocies");
//			String bla = (String) ((JavascriptExecutor) driver.getDriver()).executeScript(
//					"var ret = ''; for (var i = 0; i < localStorage.length; i++){ret += localStorage.key(i) + ',' + localStorage.getItem(localStorage.key(i)) + ';';}return ret;");
//			System.out.println("bla:1 " + bla);
//			System.out.println("bla1 size" + bla.length());

			((JavascriptExecutor) driver.getDriver())
					.executeScript("window.sessionStorage.clear(); window.localStorage.clear();");
			
			driver.getDriver().navigate().refresh();

			Thread.sleep(2000);

//			bla = (String) ((JavascriptExecutor) driver.getDriver()).executeScript(
//					"var ret = ''; for (var i = 0; i < localStorage.length; i++){ret += localStorage.key(i) + ',' + localStorage.getItem(localStorage.key(i)) + ';';}return ret;");
//			System.out.println(bla);
//			System.out.println("bla:2 " + bla);
//			System.out.println("bla2 size" + bla.length());

			System.out.println("done refreshing the page");
		} catch (Exception e) {
			System.out.println("exception message from function: extractJsValueFromPage " + e.getMessage());
		}
	}
}