package testCases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.logging.LogEntry;
import org.xml.sax.SAXException;

import usingChromeDriver.UsingDriver;
import usingChromeDriver.UsingJSOnDriver;

public class ExtensionErrors {

	UsingDriver driver;
	UsingJSOnDriver jsFunctionsOnDriver;

	public ExtensionErrors(UsingDriver driver, UsingJSOnDriver jsFunctionsOnDriver) {
		this.driver = driver;
		this.jsFunctionsOnDriver = jsFunctionsOnDriver;
	}

	private String getRandomExtensionIdGivenByChrome() throws InterruptedException {
		driver.getDriver().navigate().to("chrome://extensions");
		return jsFunctionsOnDriver.getExtensionInstalledExtension();
	}

	private void manageTabsAndNavigateToBackgroundPage(String backgroundPageUrl) throws InterruptedException {

		ArrayList<String> allTabs = driver.setFocusOnHandle();
		if (allTabs.size() != 2) {
			Thread.sleep(1000);
		}
		driver.getDriver().switchTo().window(allTabs.get(1));
		jsFunctionsOnDriver.waitForPageToLoad();
		driver.getDriver().switchTo().window(allTabs.get(0));
		driver.getDriver().navigate().to(backgroundPageUrl);

		jsFunctionsOnDriver.deleteAllCookiesAndRefreshThePage();
	}

	public void checkForErrorInExtension(String folderName)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {

		String backgroundPageUrl = "chrome-extension://" + getRandomExtensionIdGivenByChrome()
				+ "/_generated_background_page.html";

		manageTabsAndNavigateToBackgroundPage(backgroundPageUrl);
		List<LogEntry> logEntries = driver.getDriverLogs(backgroundPageUrl);
		String message = "";
		if (logEntries.size() > 0) {
			for (LogEntry entry : logEntries) {
				message = entry.getMessage();
				System.out.println(String.format(
						"For the extension id: <b>%s</b>, the error from the background page is: </b>%s</b>",
						folderName, message));
			}
		}
	}
}