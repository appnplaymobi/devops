package testCases;

import java.awt.AWTException;
import java.util.ArrayList;

import usingChromeDriver.UsingDriver;
import usingRobot.SendKeys;

public class CaseDefaultSearch {

	UsingDriver driver;
	SendKeys sendKeys;

	public CaseDefaultSearch(UsingDriver driver, SendKeys sendKeys) {
		this.driver = driver;
		this.sendKeys = sendKeys;
	}

	private void usingSendKeysToSearch() throws AWTException, InterruptedException {
		sendKeys.focusOnAddressBar();
		Thread.sleep(150);
		sendKeys.sendText();
		sendKeys.enter();
		Thread.sleep(150);
	}

	private String searchAndReturnCurrentUrl() throws AWTException, InterruptedException {
		ArrayList<String> allTabs = driver.setFocusOnHandle();
		driver.getDriver().switchTo().window(allTabs.get(0));
		usingSendKeysToSearch();
		return driver.getCurrentUrl();
	}

	public boolean ifHoldingSearch() throws AWTException, InterruptedException {
		Thread.sleep(1000);
		String url = searchAndReturnCurrentUrl();
		boolean hasSearch = false;
		if (url.contains("hspart=Lkry")) {
			hasSearch = true;
		}
		return hasSearch;
	}
}