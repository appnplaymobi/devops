package downloadAndUnZipExtensions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class downloadAndUnZipExtensionsTools {

	public String getDownloadUrl(String extensionName) throws ParserConfigurationException, SAXException, IOException {
		return UsingXML.getXmlValue("baseUrl") + extensionName + UsingXML.getXmlValue("urlXtraParam");
	}

	public String getFileNameInDownloadLocation(String extensionName)
			throws ParserConfigurationException, SAXException, IOException {
		return UsingXML.getXmlValue("crxPath") + extensionName + ".crx";
	}

	public Path getZipTargetPath(String extId) throws ParserConfigurationException, SAXException, IOException {
		Path targetPath = Paths.get(UsingXML.getXmlValue("unzipedPath") + "//" + getExtensionNameWithOutSuffix(extId));
		return targetPath;
	}

	public String getExtensionNameWithOutSuffix(String fullExtensionName) {
		return FilenameUtils.removeExtension(fullExtensionName);
	}

	public void createNewFolder(String unzipedExtensionFolder) {
		File newFolderForunzipedExtension = new File(unzipedExtensionFolder);
		if (!newFolderForunzipedExtension.exists()) {
			newFolderForunzipedExtension.mkdir();
		}
	}
}