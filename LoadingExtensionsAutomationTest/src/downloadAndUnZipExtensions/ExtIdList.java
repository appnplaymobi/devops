package downloadAndUnZipExtensions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExtIdList {

	public static List<String> ExtensionsList() {
		List<String> extId = new ArrayList<String>(Arrays.asList("cpcclgmahkanfflnknlhbchbhmanfhpa",
				"bckffjiolfmaolfhciilpmenmcmpefgn", "lfmjlklnhfdfbamekdalinmcheapbmhe",
				"okhdmhkiegcfmjfkecgadkcfdnjmddca", "nejaaacdbgfeejoelhnbjabfmpofdidc",
				"iddoiekbooccnkjanapimigbceojphia", "pbikfgnphcckepoifhdcpijccompilln",
				"jjpfmiaoimolnfdfobijfeagciooncdd", "flggbgnlbpdopagcanbibikmladpcpkd",
				"bfadakkblmnolaafogeinadlfmpdkkdj", "hbfnfdbcddldnfkkepndecekciijfpjl",
				"gihjcojibpoelkhlofofhdagbhkoncfl", "cgcnmfhlcheglhapcomjkcimlokjmhah",
				"febpnlhbkmdekehlenfljcfgdifcildm", "gcgahckjkemmamjfcoloeccfbanednhi",
				"pkdmjmciienkmojcignajbfoikpdgeie", "kllgokmjckclcjfecfobamhlacmnndkp",
				"enichmmafnkhpdkadjdpihbjdamagnln", "nglocomemhoabpeoihblphaelljddjap",
				"imbappfoloneblppblofnkmegpghddim", "ldopdfgoajalpmiobjjfcijoffpjbnen",
				"cmjgcmgbhpglmnlkljeinmjaeoifbhca", "ogiflkfoooajaoalcmefgnjoclkbkkog",
				"cpaenlonojmpomichciokfbbkhlchcba", "cpgcmdkkgeilmakgcconaeejolkmkgoa",
				"cpbogamaeokccmfbaclibdjjnjlpkill", "bgahknbpjmchggbdoehhmbcgmmbafimh",
				"jcaeinkhackhjgocaeobeodkjcmabfgb", "jplkflffjagnocepcbmlkkjjhaellkcb",
				"hmaalgfdmcfgclkfamdiccgoiidebfph", "pnbljbmaakdimfpijogbieobeibhegbj",
				"ikkdblkbbllppaldkibimhpjpphlceil", "gllglhnkenecbajdbklpnabolcldehhm",
				"ldofkaeknajjemngcohjkbdljapecjgb", "plbbjlhkjjkfflflgmelckjnpbiojjfo",
				"ffjmnoecapndhcldgnafmkibeagljmhp", "neodcfifgcdlllhmjggknbippppllnja",
				"bgbahnoajjkdkkgchabpcafkhjbcckfa", "heiokalccokgadndfhejiikjpecdfjjb",
				"hbhkpjeboacffepgacaobfolhbkcnefd", "ncniemlipldfpkglnadjbpeemmjpijle",
				"lakbopfgpohfkbdjhhkmmbhkijbapghd", "dhjcohmgmdjjaoedejclbkkdgfeaiiej",
				"annhfgheldoegkpkpdaoplkindfclahd", "ciefkgbmkkdkijmolmjgkafocbdohhcc",
				"abjjmjmlahaoilofocbhdholkllkfkjj", "dapcekphhjfdlglocohadbapnoiabgkm",
				"gmanbdieafbkaakfeokpfcihbhhlpkio", "ohfjhffoffjjblhgbkdhdglhcblgjgoo",
				"cfobedombdffkohhnbgkemdfpdljfiah", "fhpjibkjfokbkjeongihbpdkonakfike",
				"dkhhddfkcdoinjdikmjnnbjlfeenmjha", "jcbifileifaoaanepoclaojicaollmno",
				"jdbmakjandfgdkhiidbbicpedjjlmimc", "kfdelpmkljcalklhaihoodppgkbglhdi",
				"gkjfaoeigocobkfciiahhfabhpicdgaj", "epajmhlfinjecnffhgldhddcmbdddggd",
				"aoekoghmabmcdailcompkbfclkjlofmf", "fiabfcibealmlllmnbiihfiknbcnhhla",
				"neiidhnjpkjcedipcdgpbladglddjkmi", "ecihmmgjchcokdfbpinfokhambbbodpo",
				"nognlmgiboflbpbfhhpaapgkhegnnekk", "fcnndlpeaepbmjdpmfmfpgjaglkdmenh",
				"kbbpfodjjfomgppolnkeloplmeignbel", "lfednoncdmgfmgmgdmjfffamcagibghi",
				"meanncefkecmlgemoapgpifaehnddjgo", "dphcmdokjicnocjicjcdngkgdegbhaij",
				"jgjeefhjgbbfbkhcmffjohdpckkpliee", "kjjlgckmlahfmacbmahpcpbghmaogehc",
				"efmcgblkgienegceekncfmlabnodpcck", "dfhbfihajlehdolghpaempnfihmeopeg",
				"fbphgebomlfmfkknlaggpigbboinidoi", "pgbmhonbcnemkjpleofflpodebeinnki",
				"mclcpjhcfoaonblfdjflpeajjaoaihgd", "kffplnohkmnjpakkgahhbpndamfidlkb"));
		return extId;
	}
}