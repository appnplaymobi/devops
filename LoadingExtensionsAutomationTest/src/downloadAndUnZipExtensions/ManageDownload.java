package downloadAndUnZipExtensions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;

import configuration.UsingXML;

public class ManageDownload {

	downloadAndUnZipExtensionsTools tools;

	public ManageDownload(downloadAndUnZipExtensionsTools tools) {
		this.tools = tools;
	}

	private void downloadExtension(String extensionName)
			throws MalformedURLException, IOException, ParserConfigurationException, SAXException {
		File dowloadExtensionFile = new File(tools.getFileNameInDownloadLocation(extensionName));
		try {
			FileUtils.copyURLToFile(new URL(tools.getDownloadUrl(extensionName)), dowloadExtensionFile, 60000, 60000);
			if (dowloadExtensionFile.length() == 0) {
				String extensionNameWithOutSuffix = tools.getExtensionNameWithOutSuffix(extensionName);
				System.out.println("extension not found in web store: " + extensionNameWithOutSuffix);
				dowloadExtensionFile.delete();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void unzipFile(Path zipPath, Path targetDirectory) throws IOException {
		try (ZipFile zipFile = new ZipFile(zipPath.toFile())) {
			for (Enumeration<? extends ZipEntry> entries = zipFile.entries(); entries.hasMoreElements();) {
				ZipEntry entry = entries.nextElement();
				Path targetFile = targetDirectory.resolve(entry.getName());
				if (entry.isDirectory()) {
					Files.createDirectories(targetFile);
				} else {
					Files.createDirectories(targetFile.getParent());
					try (InputStream in = zipFile.getInputStream(entry)) {
						Files.copy(in, targetFile);
					}
				}
			}
		}
	}

	public void downloadAndUnzipExtension() throws MalformedURLException, IOException, ParserConfigurationException, SAXException {
		List<String> extList = ExtIdList.ExtensionsList();
		for (String extId : extList) {
			downloadExtension(extId);
			Path zipPath = Paths.get(UsingXML.getXmlValue("crxPath") + extId + ".crx");
			
			tools.createNewFolder(tools.getExtensionNameWithOutSuffix(extId));
			if (Files.exists(zipPath)) {
				unzipFile(zipPath, tools.getZipTargetPath(tools.getExtensionNameWithOutSuffix(extId)));
			}
		}
	}
}