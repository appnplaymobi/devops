package usingRobot;

import java.awt.event.KeyEvent;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

public class SendKeys {

	Robot robot;

	public SendKeys(Robot robot) throws AWTException {
		this.robot = new Robot();
	}

	public void esc() {
		robot.keyPress(KeyEvent.VK_ESCAPE);
		robot.keyRelease(KeyEvent.VK_ESCAPE);
	}

	public void focusOnAddressBar() throws AWTException, InterruptedException {
		Thread.sleep(100);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_L);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_L);
	}

	public void sendText() {
		StringSelection stringSelection = new StringSelection("bla bla bla");
		Toolkit tolKit = Toolkit.getDefaultToolkit();
		Clipboard clipboard = tolKit.getSystemClipboard();
		clipboard.setContents(stringSelection, stringSelection);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
	}
	
	public Clipboard xt() {
		StringSelection stringSelection = new StringSelection("bla bla bla");
		Toolkit tolKit = Toolkit.getDefaultToolkit();
		Clipboard clipboard = tolKit.getSystemClipboard();
		clipboard.setContents(stringSelection, stringSelection);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		return clipboard;
	}

	public void enter() throws AWTException {
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}
}