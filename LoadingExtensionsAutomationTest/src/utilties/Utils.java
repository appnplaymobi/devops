package utilties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class Utils {

	public List<String> getListOfExtensionsNames(String path)
			throws ParserConfigurationException, SAXException, IOException {
		List<String> itemsInFolder = new ArrayList<String>();
		File folderInDirectory = new File(path);
		File[] directories = folderInDirectory.listFiles();
		for (File folder : directories) {
			if (folder.isDirectory()) {
				itemsInFolder.add(folder.getName());
			}
		}
		return itemsInFolder;
	}

	public static void unzipFile(Path zipPath, Path targetDirectory) throws IOException {
		try (ZipFile zipFile = new ZipFile(zipPath.toFile())) {
			for (Enumeration<? extends ZipEntry> entries = zipFile.entries(); entries.hasMoreElements();) {
				ZipEntry entry = entries.nextElement();
				Path targetFile = targetDirectory.resolve(entry.getName());
				if (entry.isDirectory()) {
					Files.createDirectories(targetFile);
				} else {
					Files.createDirectories(targetFile.getParent());
					try (InputStream in = zipFile.getInputStream(entry)) {
						Files.copy(in, targetFile);
					}
				}
			}
		}
	}
}