package manifest;

import java.io.FileReader;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class GetValueFromManifest {

	public String valueFromManifest(String extensionName)
			throws ParserConfigurationException, SAXException, IOException {
		String path = UsingXML.getXmlValue("localExtensionsToLoadPath");
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(path + extensionName + "/manifest.json"));
			JSONObject jsonObj = (JSONObject) obj;
			JSONObject manifestVal = (JSONObject) jsonObj.get("chrome_settings_overrides");
			return manifestVal.toString();
		} catch (Exception e) {
			return null;
		}
	}
}