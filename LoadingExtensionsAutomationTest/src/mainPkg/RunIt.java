package mainPkg;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.IOException;
import java.net.MalformedURLException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import LoadExtensionFromFile.LoadingAndTesting;
import downloadAndUnZipExtensions.ManageDownload;
import downloadAndUnZipExtensions.downloadAndUnZipExtensionsTools;
import manifest.GetValueFromManifest;
import testCases.CaseDefaultSearch;
import testCases.ExtensionErrors;
import usingChromeDriver.UsingDriver;
import usingChromeDriver.UsingJSOnDriver;
import usingRobot.SendKeys;

public class RunIt {

	public static void main(String[] args) throws MalformedURLException, IOException, ParserConfigurationException,
			SAXException, InterruptedException, AWTException {

		UsingDriver driver = new UsingDriver();
		UsingJSOnDriver jsOnDriver = new UsingJSOnDriver(driver);
		ExtensionErrors extensionErrors = new ExtensionErrors(driver, jsOnDriver);
		GetValueFromManifest getValueFromManifest = new GetValueFromManifest();
		Robot robot = new Robot();
		SendKeys sendKeys = new SendKeys(robot);
		utilties.Utils utils = new utilties.Utils();
		CaseDefaultSearch testDefaultSearch = new CaseDefaultSearch(driver, sendKeys);
		downloadAndUnZipExtensionsTools downloadTools = new downloadAndUnZipExtensionsTools();
		ManageDownload downloadAndUnzip = new ManageDownload(downloadTools);
		LoadingAndTesting loadExtension = new LoadingAndTesting(driver, extensionErrors, getValueFromManifest, utils,
				testDefaultSearch, jsOnDriver);
		loadExtension.loadAndTestExtensionFromFile();
//		downloadAndUnzip
		downloadAndUnzip.downloadAndUnzipExtension();
	}
}