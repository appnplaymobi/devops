package testCases;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;
import com.google.cloud.bigquery.JobException;
import com.microsoft.azure.storage.StorageException;
import dbData.RetriveDataResults;
import driverDefinition.UsingDriver;
import report.AsureRelatedFunctions;
import report.ReportManager;
import urlValidation.ValidateUrl;
import utilities.Util;

public class TestCaseOne {

	private UsingDriver driver;
	private Util utils;
	private TestPgSegment testsFunctions;
	private ValidateUrl urlValidation;
	private AsureRelatedFunctions azure;
	private ReportManager manager;
	private TestChromeExtentionWindow extension;

	public TestCaseOne(UsingDriver driver, Util utils, TestPgSegment testsFunctions, ValidateUrl urlValidation,
			AsureRelatedFunctions Azure, ReportManager manager, TestChromeExtentionWindow extension)
			throws ParserConfigurationException, SAXException, IOException, InvalidKeyException, URISyntaxException,
			StorageException {
		this.driver = driver;
		driver.initDriver();
		this.utils = utils;
		this.testsFunctions = testsFunctions;
		this.urlValidation = urlValidation;
		this.azure = Azure;
		azure.initAzureConnection();
		this.manager = manager;
		this.extension = extension;
	}

	@Before
	public void beforeTest()
			throws InvalidKeyException, ParserConfigurationException, SAXException, IOException, URISyntaxException {
		utils.createReportFolders();
	}

	@Test
	public void test() throws FileNotFoundException, JobException, IOException, ParserConfigurationException,
			SAXException, InterruptedException, URISyntaxException, StorageException, AWTException {
		RetriveDataResults data = new RetriveDataResults();
		List<String> allUrls = data.createUrlToTest();
		for (String url : allUrls) {
			manager.initTestCase(url);
			long pageLoadStart = utils.startNanoTime();
			System.out.println(String.format("working on: %s", url));
			driver.getDriver().get(url);
			driver.waitForPageToLoad();
			boolean notUnknownSource = urlValidation.ifUnknownSource(url);
			boolean notChromeStore = urlValidation.ifPageLeadsToChromeStore(url);
			if (notUnknownSource && notChromeStore) {
				long pageLoadEnd = utils.endNanoTime();
				testsFunctions.reportPageLoad(pageLoadStart, pageLoadEnd);
				driver.waitForPageToLoad();
				testsFunctions.TestglobalParameterPgSegment(url);
				extension.clickOnAddToChromeBtn();
				extension.checkChromeExtension(url);
				driver.deleteCookeisAndLogs();
			}
		}
		driver.endDriver();
		manager.endTestReport();
		manager.flushTest();
		String reportPath = utils.returnLocalReportPath();
		azure.fileToAzure(reportPath, utils.returnTodayDate());
	}
}