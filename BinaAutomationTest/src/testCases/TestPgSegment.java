package testCases;

import java.util.concurrent.TimeUnit;

import com.relevantcodes.extentreports.LogStatus;

import driverDefinition.UsingDriver;
import report.ReportManager;

public class TestPgSegment {

	private UsingDriver driver;
	private ReportManager report;

	public TestPgSegment(UsingDriver driver, ReportManager report) {
		this.driver = driver;
		this.report = report;
	}

	public void reportPageLoad(long start, long end) {
		long durationCalculation = end - start;
		long seconds = TimeUnit.NANOSECONDS.toMillis(durationCalculation);
		report.getTest().log(LogStatus.INFO,
				String.format("The page loaded in: <b>%s</b> milliseconds", String.valueOf(seconds)));
	}

	public void TestglobalParameterPgSegment(String url) {
		String jsParameter = driver.extractJsValueFromPage("pgSegment");
		if (jsParameter.contains("1_")) {
			report.getTest().log(LogStatus.PASS, String.format("Test pass the <b>jsParameter</b> is: %s", jsParameter));
		}
		if (jsParameter.equals("3_-1")) {
			report.getTest().log(LogStatus.FAIL,
					String.format("For the URL <b>%s</b>, there is no model was found for pid/product", url));
		}
		if (jsParameter.equals("3_-2")) {
			report.getTest().log(LogStatus.FAIL,
					String.format("For the URL <b>%s</b>, there is error in the classify", url));
		}
		if (jsParameter.equals("2")) {
			report.getTest().log(LogStatus.FAIL,
					String.format("For the URL <b>%s</b>, there is no AI for campaign", url));
		}
		if (jsParameter.equals("0")) {
			report.getTest().log(LogStatus.WARNING,
					String.format("For the URL <b>%s</b>, the pgSegment retrun 0", url));
		}
	}
}