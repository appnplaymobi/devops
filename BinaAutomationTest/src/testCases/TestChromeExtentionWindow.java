package testCases;

import java.awt.AWTException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Set;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.StorageException;
import com.relevantcodes.extentreports.LogStatus;
import driverDefinition.UsingDriver;
import elementLocators.ElementsInPgae;
import report.ReportManager;

public class TestChromeExtentionWindow {

	private ElementsInPgae element;
	private UsingDriver driver;
	private ReportManager report;

	public TestChromeExtentionWindow(ElementsInPgae element, UsingDriver driver, ReportManager report)
			throws MalformedURLException {
		this.element = element;
		this.driver = driver;
		this.report = report;
	}

	public WebElement findHowToLink() {
		return element.elementByClassName("how-to-link");
	}

	public WebElement findAddToChrome() {
		return element.elementByClassName("AddToBrowser");
	}

	public boolean waitForHowToLink() {
		if (element.waitByClassName("how-to-link") != null) {
			return true;
		}
		return false;
	}

	public void clickOnAddToChromeBtn() {
		boolean howItWorksMsgDisplay = waitForHowToLink();
		try {
			if (howItWorksMsgDisplay) {
				findHowToLink().click();
				findAddToChrome().click();
				System.out.println("found first if");
			} else {
				findAddToChrome().click();
				System.out.println("found second if");
			}
		} catch (Exception e) {
			report.getTest().log(LogStatus.FATAL, "install button is not clickeble");
		}
	}

	public void checkChromeExtension(String url) throws MalformedURLException, IOException,
			ParserConfigurationException, SAXException, URISyntaxException, AWTException, StorageException {
		String originalWindowHandle = driver.getDriver().getWindowHandle();
		WebDriver popup = null;
		Set<String> windowsList = driver.getDriver().getWindowHandles();

		for (String window : windowsList) {
			if (!window.equals(originalWindowHandle)) {
				popup = driver.getDriver().switchTo().window(window);
				if (popup.getTitle().contains("Chrome Web Store")) {
					element.waitByClassName("e-f-pa");
					popup.close();
				} else {
					String chromeStoreTitle = driver.getDriver().getTitle();
					popup.close();
					if (chromeStoreTitle.contains("page not found") || chromeStoreTitle.contains("404")) {
						report.getTest().log(LogStatus.FATAL,
								String.format("The chrome store open with: <b>%s</b>", chromeStoreTitle));
					}
				}
			}
		}
		driver.getDriver().switchTo().window(originalWindowHandle);
	}
}