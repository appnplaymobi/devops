package mailMethods;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import configuration.UsingXML;
import utilities.Util;

public class MailContent {

	private Util util;

	public MailContent(Util util) {
		this.util = util;
	}

	public String getReportPathInAsure() throws ParserConfigurationException, SAXException, IOException {
		String retpotPath = UsingXML.getXmlValue("azureReportFolder");
		String todayDate = "/reports/Bina/" + util.returnTodayDate(); 
		String fileName = UsingXML.getXmlValue("reportName");

		String pathToReport = retpotPath + todayDate + fileName;
		return pathToReport;
	}

	public String setMailContent() throws ParserConfigurationException, SAXException, IOException {
		String asureReportPath = getReportPathInAsure();
		String finalUrl = "<a href=" + asureReportPath + ">" + asureReportPath + "</a>";
		String mesageContent = setMessageColor("<b>Bina report result: </b> ", "black") + finalUrl;
		return mesageContent;
	}
	
	public String setMessageColor(String message, String color) {
		String MessageColor = "<font color=" + color + ">" + message + "</font>";
		return MessageColor;
	}
}