package dbData;

import java.util.List;

public class SqlDataParser {
	
	private List<String> pids;
	private String url;

	public SqlDataParser(List<String> pids, String url) {
		this.pids = pids;
		this.url = url;
	}

	public List<String> getPid() {
		return pids;
	}

	public String getUrl() {
		return url;
	}
}