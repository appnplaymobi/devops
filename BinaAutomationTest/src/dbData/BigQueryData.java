package dbData;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.FieldList;
import com.google.cloud.bigquery.FieldValue;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.JobException;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;

import configuration.UsingXML;

public class BigQueryData {

	private BigQuery bigquery;
	private TableResult queryResult;

	private BigQuery createBigQueryConnection()
			throws FileNotFoundException, IOException, ParserConfigurationException, SAXException {
		
		String credentialsJsonFile = UsingXML.getXmlValue("credentialsFile");
		BigQuery bigquery = BigQueryOptions.newBuilder()
				.setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream(credentialsJsonFile))).build()
				.getService();
		return bigquery;
	}

	private TableResult selectActiveCampaigns() throws FileNotFoundException, IOException, ParserConfigurationException,
			SAXException, JobException, InterruptedException {
		
		bigquery = createBigQueryConnection();
		String query = "SELECT PID, EntityName FROM [PageGenAdmain.CampaignsStats] where EntityName not like"
				+ " 'notificationdefault' and PageLoadCount > 2000 order by PageLoadCount desc;";
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query).setUseLegacySql(true).build();

		queryResult = bigquery.query(queryConfig);
		return queryResult;
	}

	public List<Map<String, String>> getActiveCampaigns() throws FileNotFoundException, IOException,
			ParserConfigurationException, SAXException, JobException, InterruptedException {
		
		queryResult = selectActiveCampaigns();
		FieldList fields = queryResult.getSchema().getFields();
		List<Map<String, String>> results = new ArrayList<Map<String, String>>();

		for (FieldValueList row : queryResult.iterateAll()) {
			int index = 0;
			Map<String, String> rowMap = new HashMap<String, String>();

			for (FieldValue val : row) {
				rowMap.put(fields.get(index).getName(), val.getStringValue());
				index++;
			}
			results.add(rowMap);
		}
		return results;
	}
}