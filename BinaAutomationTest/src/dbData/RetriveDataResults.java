package dbData;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.google.cloud.bigquery.JobException;
import configuration.UsingXML;

public class RetriveDataResults {

	private SQLdata sqlData;
	private BigQueryData bigData;

	private List<String> getActivePids() throws FileNotFoundException, JobException, IOException,
			ParserConfigurationException, SAXException, InterruptedException {

		bigData = new BigQueryData();
		List<Map<String, String>> data = bigData.getActiveCampaigns();
		List<String> allPids = new ArrayList<String>();

		for (Map<String, String> row : data) {
			allPids.add(row.get("PID"));
		}
		return allPids;
	}

	private SQLdata returnConnectionString() throws ParserConfigurationException, SAXException, IOException {
		String cConnectionString = UsingXML.getXmlValue("dbConnectionString");
		sqlData = new SQLdata(cConnectionString);
		return sqlData;
	}

	public List<String> createUrlToTest() throws FileNotFoundException, JobException, IOException,
			ParserConfigurationException, SAXException, InterruptedException {

		List<String> pids = getActivePids();
		sqlData = returnConnectionString();

		List<String> collectUrls = new ArrayList<String>();
		List<String> urlsFromSQL = sqlData.getUrlsByPids(pids);
		for (String singleUrl : urlsFromSQL) {
			collectUrls.add(singleUrl);
		}
		return collectUrls;
	}
}