package elementLocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import driverDefinition.UsingDriver;

public class ElementsInPgae {

	private UsingDriver driver;

	public ElementsInPgae(UsingDriver driver) {
		this.driver = driver;
	}

	public WebElement elementByClassName(String className) {
		try {
			WebElement singleElement = driver.getDriver().findElement(By.className(className));
			return singleElement;
		} catch (Exception e) {
			return null;
		}
	}

	public WebElement waitByClassName(String elementValue) {
		try {
			WebDriverWait hold = new WebDriverWait(driver.getDriver(), 10);

			WebElement elementInPage = hold
					.until(ExpectedConditions.visibilityOfElementLocated((By.className(elementValue))));
			return elementInPage;
		} catch (Exception e) {
			return null;
		}
	}

}
