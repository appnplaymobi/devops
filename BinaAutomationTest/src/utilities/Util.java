package utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import configuration.UsingXML;

public class Util {

	public String returnTodayDate() {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		String todayDate = dateFormat.format(now);
		return todayDate;
	}

	public long startNanoTime() throws IOException {
		long start = System.nanoTime();
		return start;
	}

	public long endNanoTime() throws IOException {
		long end = System.nanoTime();
		return end;
	}

	public void createReportFolder(String pathToFolder) throws ParserConfigurationException, SAXException, IOException {
		File newReportDirectory = new File(pathToFolder);
		if (!newReportDirectory.exists()) {
			try {
				newReportDirectory.mkdir();
			} catch (Exception e) {
				System.out.println("createReportFolder function crushed: " + e.getMessage());
			}
		}
	}

	public void createFolder(String pathToFolder) {
		File newReportDirectory = new File(pathToFolder);
		if (!newReportDirectory.exists()) {
			try {
				newReportDirectory.mkdir();
			} catch (Exception e) {
				System.out.println("createReportFolder function crushed: " + e.getMessage());
			}
		}
	}

	public void createReportFolders() throws ParserConfigurationException, SAXException, IOException {
		String todayDate = returnTodayDate();

		String azureFolderPath = UsingXML.getXmlValue("azureReportFolder");
		createFolder(azureFolderPath + todayDate);

		String localFolderPath = UsingXML.getXmlValue("localReportFolder");
		createFolder(localFolderPath + todayDate);
	}

	public String returnLocalReportPath() throws ParserConfigurationException, SAXException, IOException {
		String localFolder = UsingXML.getXmlValue("localReportFolder");
		String reportName = UsingXML.getXmlValue("reportName");
		String pathToReport = localFolder + returnTodayDate() + reportName;
		return pathToReport;
	}
}