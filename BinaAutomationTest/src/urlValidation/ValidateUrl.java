package urlValidation;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import com.relevantcodes.extentreports.LogStatus;

import driverDefinition.UsingDriver;
import report.ReportManager;

public class ValidateUrl {

	private UsingDriver driver;
	private ReportManager report;

	public ValidateUrl(UsingDriver driver, ReportManager report) {
		this.driver = driver;
		this.report = report; 
	}

	public boolean ifUnknownSource(String urlToCheck) throws IOException {
		try {
			URL u = new URL(urlToCheck);
			HttpURLConnection huc = (HttpURLConnection) u.openConnection();
			huc.setRequestMethod("GET");
			huc.connect();
			huc.getResponseMessage();
			return true;
		} catch (Exception e) {
			report.getTest().log(LogStatus.FATAL, String
					.format("The Url: <b>%s</b> cannot be tested. The error is: <b>%s</b>", urlToCheck, e.getMessage()));
			return false;
		}
	}

	public boolean ifPageLeadsToChromeStore(String url) {
		String urlToCheck = driver.extractCurrentUrl();
		if (urlToCheck.contains("chrome.google.com")) {
			System.out.println("chrome.google.com");
			report.getTest().log(LogStatus.SKIP, String
					.format("The Url: <b>%s</b> leads to chrome store", url));
			return false;
		}
		return true;
	}
}