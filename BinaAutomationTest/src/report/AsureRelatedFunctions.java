package report;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

import configuration.UsingXML;
import utilities.Util;

public class AsureRelatedFunctions {

	private CloudStorageAccount storageAccount;
	private CloudBlobClient client;
	private CloudBlobContainer container;
	private CloudBlockBlob block;
	private Util utils;
	
	public AsureRelatedFunctions(Util utils) {
		this.utils = utils;
	}
	
	public void initAzureConnection() throws ParserConfigurationException, SAXException, IOException,
			InvalidKeyException, URISyntaxException, StorageException {
		String AccountName = UsingXML.getXmlValue("accountName");
		String AccountKey = UsingXML.getXmlValue("accountKey");
		final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=" + AccountName
				+ ";AccountKey=" + AccountKey;
		storageAccount = CloudStorageAccount.parse(storageConnectionString);
		client = new CloudBlobClient(storageAccount.getBlobStorageUri(), storageAccount.getCredentials());
		container = client.getContainerReference("qa-reports");
	}

	public void uploadFile(String pathToUploadTo, String fileToUpload)
			throws URISyntaxException, StorageException, IOException {
		block = container.getBlockBlobReference(pathToUploadTo);
		block.getProperties().setContentType("text/html");
		block.uploadFromFile(fileToUpload);
	}

	public String fileToAzure(String fileToUpload, String date)
			throws URISyntaxException, StorageException, IOException {
		Path path = Paths.get(fileToUpload);
		String pathToUploadTo = "reports/Bina/" + date + "/" + path.getFileName();
		uploadFile(pathToUploadTo, fileToUpload);
		return pathToUploadTo;
	}

	public void uploadReport()
			throws URISyntaxException, StorageException, IOException, ParserConfigurationException, SAXException {
		String todayDate = utils.returnTodayDate();
		fileToAzure(UsingXML.getXmlValue("localReportFolder") + todayDate + UsingXML.getXmlValue("reportName"),
				todayDate);
	}
}