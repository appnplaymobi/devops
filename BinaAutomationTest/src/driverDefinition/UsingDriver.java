package driverDefinition;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class UsingDriver {

	private WebDriver driver;

	public void initDriver() throws ParserConfigurationException, SAXException, IOException {
		String chromeDriver = UsingXML.getXmlValue("chromeDriverPath");
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		ChromeOptions options = new ChromeOptions();
		options.setPageLoadStrategy(PageLoadStrategy.NONE);
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
	}

	public WebDriver getDriver() {
		return driver;
	}

	public WebDriver endDriver() {
		driver.quit();
		return driver;
	}

	public String extractCurrentUrl() {
		return driver.getCurrentUrl();
	}

	public void deleteCookeisAndLogs() {
		driver.manage().logs().get(LogType.BROWSER).getAll();
		driver.manage().deleteAllCookies();
	}

	public void waitForPageToLoad() {
		new WebDriverWait(driver, 30).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
	}

	public List<LogEntry> getDriverLogs(String testedUrl) {
		List<LogEntry> logEntries = driver.manage().logs().get(LogType.BROWSER).getAll();
		return logEntries;
	}

	public String extractJsValueFromPage(String jsValue) {
		try {
			return ((JavascriptExecutor) driver).executeScript("return " + jsValue).toString();
		} catch (Exception e) {
			System.out.println("exception message from function: extractJsValueFromPage " + e.getMessage());
		}
		return null;
	}
}