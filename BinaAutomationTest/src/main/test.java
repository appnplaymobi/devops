package main;

import driverDefinition.UsingDriver;
import elementLocators.ElementsInPgae;
import mailMethods.MailContent;
import mailMethods.MailFunctions;
import mailMethods.MailSendingReport;
import report.AsureRelatedFunctions;
import report.ReportManager;
import testCases.TestCaseOne;
import testCases.TestChromeExtentionWindow;
import testCases.TestPgSegment;
import urlValidation.ValidateUrl;
import utilities.Util;

public class test {

	public static void main(String[] args) throws Exception {
		UsingDriver driver = new UsingDriver();
		Util utils = new Util();
		String reportPath = utils.returnLocalReportPath();
		ReportManager report = new ReportManager(reportPath);

		AsureRelatedFunctions azure = new AsureRelatedFunctions(utils);
		ElementsInPgae element = new ElementsInPgae(driver); 
		TestChromeExtentionWindow extensionCheck = new TestChromeExtentionWindow(element, driver, report); 
		ValidateUrl urlValidation = new ValidateUrl(driver, report);
		TestPgSegment testPgSementInPage = new TestPgSegment(driver, report); 
		TestCaseOne firstTestCase = new TestCaseOne(driver, utils, testPgSementInPage, urlValidation, azure, report, extensionCheck);
		firstTestCase.test();
//		firstTestCase.testingOnly();
		MailFunctions mailFunctions = new MailFunctions();
		MailSendingReport sendReport = new MailSendingReport(mailFunctions);
		MailContent mailContent = new MailContent(utils);
		sendReport.mailReulsts(mailContent.setMailContent(), "[Test] Bina Automation test report");
	}
}