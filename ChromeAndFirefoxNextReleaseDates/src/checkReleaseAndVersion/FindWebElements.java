package checkReleaseAndVersion;

import org.openqa.selenium.WebElement;
import initChromeDriver.UsingDriver;
import java.io.IOException;
import java.text.ParseException;

public class FindWebElements {

	public UsingDriver driver;

	public FindWebElements(UsingDriver driver) {
		this.driver = driver;
	}

	public String fireFoxReleaseVersion() throws IOException, InterruptedException {
		WebElement releaseVersion = driver.findElementByXpath("//*[@id='mw-content-text']/table[1]/tbody/tr[2]/td[4]");
		return releaseVersion.getText();
	}

	public String fireFoxReleaseDate() throws IOException, InterruptedException, ParseException {
		WebElement releaseDate = driver.findElementByXpath("//*[@id='mw-content-text']/table[1]/tbody/tr[2]/th[3]");
		//*[@id="mw-content-text"]/table[1]/tbody/tr[2]/th[3]
		return releaseDate.getText();
	}

	public String chromeReleaseVersion() {
		WebElement releaseVersion = driver.findElementByXpath("//*[@title='Download Chrome Beta']");
		return releaseVersion.getText();
	}

	public String chromeReleaseDate() {
		WebElement releaseDate = driver
				.findElementByXpath("//*[@id='releases-section']/div/div/section[2]/div[3]/h3/span[2]");
		return releaseDate.getText().replace("( ", "").replace(" )", "");
	}
}