package checkReleaseAndVersion;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import externalFiles.ManageXml;
import initChromeDriver.UsingDriver;
import mailResults.MailTemplate;
import utils.GenUtil;

public class CHDateAndVersion {

	public UsingDriver driver;
	public GenUtil utils;
	public MailTemplate mail;
	public FindWebElements element;
	
	public CHDateAndVersion(UsingDriver driver, GenUtil utils, MailTemplate mail, FindWebElements element) {
		this.driver = driver;
		this.utils = utils;
		this.mail = mail;
		this.element = element;
		
	}

	public String returnChromeReleaseDate() throws ParserConfigurationException, SAXException, IOException {
		String firefoxReleasePage = ManageXml.getDataFromXML("chromeNextReleaseWebPage");
		driver.launchPage(firefoxReleasePage);
		String chReleaseDate = element.chromeReleaseDate();
		return chReleaseDate;
	}

	public long extractChromeReleaseDate()
			throws ParserConfigurationException, SAXException, IOException, ParseException {
		String releaseDtafromPage = returnChromeReleaseDate();
		Map<String, String> StringMothToYear = utils.setStringMonth();
		String[] getDate = releaseDtafromPage.split(" ");
		String completeDate = utils.getCurrentYear() + "-" + StringMothToYear.get(getDate[0]) + "-" + getDate[1];
		long longCompleteDate = utils.longReleaseDate(completeDate);
		return longCompleteDate;
	}

	public String mailChrome()
			throws IOException, InterruptedException, ParseException, ParserConfigurationException, SAXException {
		int daysToRelease = utils.diffInDays(extractChromeReleaseDate());		
		String chReleaseVersion = element.chromeReleaseVersion();
		driver.endDriver();
		return mail.content(daysToRelease, chReleaseVersion);
	}
}