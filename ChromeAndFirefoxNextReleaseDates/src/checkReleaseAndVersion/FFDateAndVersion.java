package checkReleaseAndVersion;

import java.io.IOException;
import java.text.ParseException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import externalFiles.ManageXml;
import initChromeDriver.UsingDriver;
import mailResults.MailTemplate;
import utils.GenUtil;

public class FFDateAndVersion {

	public UsingDriver driver;
	public GenUtil utils;
	public MailTemplate mail;
	public FindWebElements element;

	public FFDateAndVersion(UsingDriver driver, GenUtil utils, MailTemplate mail, FindWebElements element) {
		this.driver = driver;
		this.utils = utils;
		this.mail = mail;
		this.element = element;
	}

	public String returnFirefoxReleaseDate()
			throws IOException, InterruptedException, ParseException, ParserConfigurationException, SAXException {
		String firefoxReleasePage = ManageXml.getDataFromXML("firefoxNextReleaseWebPage");
		driver.launchPage(firefoxReleasePage);
		Thread.sleep(3000);
		String ffReleaseDate = element.fireFoxReleaseDate();
		return ffReleaseDate;
	}

	public long extractFirefoxeReleaseDate()
			throws IOException, InterruptedException, ParseException, ParserConfigurationException, SAXException {
		String ffRelDate = returnFirefoxReleaseDate();
		long releaseDate = utils.longReleaseDate(ffRelDate);
		return releaseDate;
	}

	public String mailFirefox()
			throws IOException, InterruptedException, ParseException, ParserConfigurationException, SAXException {
		int daysToRelease = utils.diffInDays(extractFirefoxeReleaseDate());
		String ffReleaseVersion = element.fireFoxReleaseVersion();
		driver.endDriver();
		return mail.content(daysToRelease, ffReleaseVersion);
	}
}