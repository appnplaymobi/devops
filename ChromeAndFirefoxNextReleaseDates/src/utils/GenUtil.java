package utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.xml.sax.SAXException;

public class GenUtil {

	public int diffInDays(long releaseDate)
			throws IOException, InterruptedException, ParseException, ParserConfigurationException, SAXException {
		DateTime relDate = new DateTime(releaseDate);
		DateTime currentDateTime = new DateTime();
		DateTime currentDate = new DateTime(currentDateTime.getYear(), currentDateTime.getMonthOfYear(),
				currentDateTime.getDayOfMonth(), relDate.getHourOfDay(), relDate.getMinuteOfHour(), relDate.getZone());
		Days diffInDays = Days.daysBetween(currentDate, relDate);
		return diffInDays.getDays();
	}

	public long longReleaseDate(String browserReleaseDate) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		long releaseDate = df.parse(browserReleaseDate).getTime();
		return releaseDate;
	}

	public Map<String, String> setStringMonth() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("Jan", "1");
		map.put("Feb", "2");
		map.put("Mar", "3");
		map.put("Apr", "4");
		map.put("May", "5");
		map.put("Jun", "6");
		map.put("Jul", "7");
		map.put("Aug", "8");
		map.put("Sep", "9");
		map.put("Oct", "10");
		map.put("Nov", "11");
		map.put("Dec", "12");
		return map;
	}

	public String getCurrentYear() {
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		String stringCurrentYear = Integer.toString(currentYear);
		return stringCurrentYear;
	}
}