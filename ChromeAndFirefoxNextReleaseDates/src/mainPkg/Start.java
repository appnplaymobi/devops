package mainPkg;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import javax.mail.MessagingException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.mail.EmailException;
import org.xml.sax.SAXException;
import checkReleaseAndVersion.CHDateAndVersion;
import checkReleaseAndVersion.FFDateAndVersion;
import checkReleaseAndVersion.FindWebElements;
import initChromeDriver.UsingDriver;
import mailResults.MailTemplate;
import mailResults.mailFunctions;
import mailResults.mailSendingReport;
import utils.GenUtil;

public class Start {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException,
			InterruptedException, ParseException, EmailException, MessagingException {
		
		GenUtil utils = new GenUtil();
		
		MailTemplate mailIt = new MailTemplate();
		UsingDriver usingDriver = new UsingDriver();
		FindWebElements elementsInPage = new FindWebElements(usingDriver);
		FFDateAndVersion testFirefox = new FFDateAndVersion(usingDriver, utils, mailIt, elementsInPage);
		CHDateAndVersion testChrome = new CHDateAndVersion(usingDriver, utils, mailIt, elementsInPage);
		
		String checkFirefox = testFirefox.mailFirefox();
		String checkChrome = testChrome.mailChrome();
		
		if (!checkFirefox.isEmpty() || !checkChrome.isEmpty()){
			List<String> recipientsList = mailFunctions.collectRecipients();
			mailSendingReport.createAndSendMailResults(testFirefox.mailFirefox() + testChrome.mailChrome(), mailIt.subject(), recipientsList);
		}
	}
}