package mailResults;

public class MailTemplate {

	public String subject() {
		String mesageContent = String.format("[test] version alert mail");
		return mesageContent;
	}

	public String content(int daysToRelease, String releaseVersion) {
		String content = "";
		if (daysToRelease < 8 && daysToRelease > 0) {
			content = String.format(
					"The next" + setBoldColor(" %s", "red") + ", will be released in: "+ setBoldColor("%s dayes<br>", "black"),
					releaseVersion, daysToRelease);
		} else if (daysToRelease == 0) {
			content = String.format("The next" + setBoldColor(" %s", "red") + ", will be released today.<br>",
					releaseVersion);
		} else if (daysToRelease == -1) {
			content = String.format("The next"  + setBoldColor(" %s", "red") + " supposed be released yesterday.<br>", releaseVersion);
		}
		return content;
	}

	public static String setBoldColor(String message, String color) {
		String MessageColor = "<b><font color=" + color + ">" + message + "</font></b>";
		return MessageColor;
	}
}