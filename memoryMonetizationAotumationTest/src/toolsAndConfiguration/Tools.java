package toolsAndConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Tools {

	public Process executeConsoleCommand(String[] params) throws Exception {
		final ProcessBuilder processBuilder = new ProcessBuilder(params);
		processBuilder.redirectErrorStream(true);
		Process process = null;
		try {
			process = processBuilder.start();
		} catch (IOException e) {
			System.out.println(e);
			throw new Exception("Exception launching a process", e);
		}
		return process;
	}

	public List<String> getConsoleOutput(Process process) throws Exception {
		List<String> lines = new ArrayList<String>();
		InputStream input = process.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		try {
			String line = reader.readLine();
			while (line != null) {
				line = line.trim();
				if (!line.isEmpty()) {
					lines.add(line);
				}
				line = reader.readLine();
			}
		} catch (IOException e) {
			System.out.println(e);
			throw new Exception("It was not possible to obtain the process output", e);
		} finally {
			try {
				if (input != null) {
					input.close();
				}
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				System.out.println(e);
				throw new Exception("It was not possible to close the console stream", e);
			}
		}
		return lines;
	}
}
