package mainPkg;

import commands.SendCommandLine;
import commands.cmdLine.ParseCmdLine;
import commands.killAndRestoreProcses.ProcessKiller;
import commands.taskList.ParseTaskList;
import memMonitoring.MemUsage;
import toolsAndConfiguration.Tools;

public class runMonitor {

	public static void main(String[] args) throws Exception {
		MemUsage memUsage = new MemUsage();
		boolean highMemory = memUsage.isHighMemoryPercentage();
		if (highMemory) {
			Tools utils = new Tools();
			SendCommandLine commandLine = new SendCommandLine();
			ParseTaskList taskList = new ParseTaskList(utils, commandLine);
			ParseCmdLine parseCmdLine = new ParseCmdLine(utils, commandLine);
			ProcessKiller processKiller = new ProcessKiller();

			Monior monitor = new Monior(taskList, parseCmdLine, processKiller);
			monitor.getCommandLine();
		}
	}
}