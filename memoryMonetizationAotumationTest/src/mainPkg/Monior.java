package mainPkg;

import java.util.List;
import commands.cmdLine.ParseCmdLine;
import commands.killAndRestoreProcses.ProcessKiller;
import commands.taskList.ParseTaskList;

public class Monior {

	private ParseTaskList parseTaskList;
	private ParseCmdLine parseCmdLine;
	private ProcessKiller processKiller;

	public Monior(ParseTaskList parseTaskList, ParseCmdLine parseCmdLine, ProcessKiller processKiller) {
		this.parseTaskList = parseTaskList;
		this.parseCmdLine = parseCmdLine;
		this.processKiller = processKiller;
	}

	public void getCommandLine() throws Exception {
		// processKiller.killSelectedDepartmantProc("Sample Healthcare");
		List<Integer> FirstTowSortedProcess = parseTaskList.returnFirstTowSortedProcess();
		if (FirstTowSortedProcess.size() >= 2) {
			for (int singleProcessId : FirstTowSortedProcess) {
				String commandLine = parseCmdLine.getCommandLineFromSortedProcesses(singleProcessId);
				String departmantNameToKill = parseCmdLine.getDepartmentName(commandLine);
				String farmStateName = parseCmdLine.getFarmState(commandLine);
				try {
					if (!farmStateName.equals("Building")) {
						processKiller.killSelectedDepartmantProc(departmantNameToKill);
						break;
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}
}