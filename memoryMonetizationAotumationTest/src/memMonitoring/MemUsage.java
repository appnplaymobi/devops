package memMonitoring;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import xmlConfiguration.UsingXml;

public class MemUsage {

	private long getTotalMemorySize() throws IOException {
		OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
		long total = 0;
		for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
			method.setAccessible(true);
			if (method.getName().equals("getTotalPhysicalMemorySize")) {
				Object value;
				try {
					value = method.invoke(operatingSystemMXBean);
					total = (long) value;
				} catch (Exception e) {
					value = e;
					System.out.println(value.toString());
				}
			}
		}
		return total;
	}

	private long getFreeMemSize() throws IOException {
		OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
		long free = 0;
		for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
			method.setAccessible(true);
			if (method.getName().equals("getFreePhysicalMemorySize")) {
				Object value;
				try {
					value = method.invoke(operatingSystemMXBean);
					free = (long) value;
				} catch (Exception e) {
					value = e;
					System.out.println(value.toString());
				}
			}
		}
		return free;
	}

	private long convertBytesToGigaBytes(long bytes) {
		final long GABYTE = (1024 * 1024 * 1024);
		return bytes / GABYTE;
	}

	public boolean isHighMemoryPercentage() throws ParserConfigurationException, SAXException, IOException {
		long totalMemorySize = getTotalMemorySize();
		long freeMemSize = getFreeMemSize();
		long allocatedMemory = totalMemorySize - freeMemSize;

		float floatTotalMemorySize = convertBytesToGigaBytes(totalMemorySize);
		float floatAllocatedMemory = convertBytesToGigaBytes(allocatedMemory);

		float memoryPercentageUsage = floatAllocatedMemory / floatTotalMemorySize;
		memoryPercentageUsage = memoryPercentageUsage * 100;

		String memToCheck = UsingXml.getXmlValue("memCheckSize", "memoryMonetizationConfig.xml");
		int memSize = Integer.parseInt(memToCheck);
		return memoryPercentageUsage >= memSize;
	}
}