package commands.killAndRestoreProcses;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import xmlConfiguration.UsingXml;

public class ProcessKiller {
	

	public void run(String p1, String exeToRun, String command)
			throws ParserConfigurationException, SAXException, IOException {
		Process proc = Runtime.getRuntime().exec(exeToRun + command);
		proc.getOutputStream().close();
	}

	public void killSelectedDepartmantProc(String departmantName) throws Exception {
		try {
			String exePath = UsingXml.getXmlValue("psmExe", "memoryMonetizationConfig.xml");
			ProcessBuilder builder = new ProcessBuilder(exePath, "ecube", "stop", "name=" + departmantName);
			builder.redirectErrorStream(true);
			Process pr = builder.start();
			pr.waitFor();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}