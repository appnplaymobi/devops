package commands.cmdLine;

import toolsAndConfiguration.Tools;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import commands.SendCommandLine;

public class ParseCmdLine {

	private Tools utils;
	private SendCommandLine commandLine;
	
	
	public ParseCmdLine(Tools utils, SendCommandLine commandLine) {
		this.utils = utils;
		this.commandLine = commandLine;
	}

	private List<String> getCommandLine(int procName) throws Exception {
		List<String> commandline = null;
		String[] commandParams = commandLine.commandLineParams(procName);
		Process pr = utils.executeConsoleCommand(commandParams);
		commandline = utils.getConsoleOutput(pr);
		return commandline;
	}

	private String getStringFromHashMap(String fullCmdLine, String stringToExtract) throws Exception {
		Map<String, String> map = hashMapQuery(fullCmdLine);
		fullCmdLine = map.get(stringToExtract);
		return fullCmdLine;
	}

	private Map<String, String> hashMapQuery(String allCommandLineArgumants) throws Exception {
		Map<String, String> result = new HashMap<String, String>();
		String[] allQuery = allCommandLineArgumants.split("--");
		for (String singleQueryString : allQuery) {
			String[] pair = singleQueryString.split("=");
			String val = null;
			if (pair.length == 2) {
				val = pair[1];
			}
			result.put(pair[0], val);
		}
		return result;
	}
	
	public String getDepartmentName(String fullCmdLine) throws Exception {
		String extract = getStringFromHashMap(fullCmdLine, "dbname");
		String filteredDepartmantName = extract.substring(1).replace("XwAa", "_").replace("IAAa", " ").trim();
		return filteredDepartmantName;
	}

	public String getFarmState(String fullCmdLIne) throws Exception {
		String farmState = getStringFromHashMap(fullCmdLIne, "farmstate");
		return farmState ;
	}

	public String getCommandLineFromSortedProcesses(Integer procId) throws Exception {
		String singleCommandLIne = "";
		List<String> commandLineDetails = getCommandLine(procId);
		for (String commandLines : commandLineDetails) {
			// remove the first entry (garbage)
			commandLineDetails.remove(0);
			// return the actual string
			commandLines = commandLineDetails.get(0);
			singleCommandLIne = commandLines;
		}
		return singleCommandLIne;
	}
}