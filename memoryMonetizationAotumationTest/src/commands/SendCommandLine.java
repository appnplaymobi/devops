package commands;

public class SendCommandLine {
	
	public String[] commandLineParams(int procId) {
		String[] params = new String[8];
		params[0] = "cmd";
		params[1] = "/c";
		params[2] = "wmic";
		params[3] = "process";
		params[4] = "where";
		params[5] = "\"ProcessID=" + procId + "\"";
		params[6] = "get";
		params[7] = "commandline";
		return params;
	}
	
	public String[] getTaskListParams(String procName) {
		String[] params = new String[7];
		params[0] = "cmd";
		params[1] = "/c";
		params[2] = "tasklist";
		params[3] = "/fi";
		params[4] = "\"" + "imagename";
		params[5] = "eq";
		params[6] = procName + "\"";
		return params;
	}
}