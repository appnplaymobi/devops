package commands.taskList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import commands.SendCommandLine;
import toolsAndConfiguration.Tools;
import xmlConfiguration.UsingXml;

public class ParseTaskList {

	private Tools utils;
	private SendCommandLine commandLine;
	
	public ParseTaskList(Tools utils, SendCommandLine commandLine) {
		this.utils = new Tools();
		this.commandLine = commandLine;
	}

	private List<String> getTaskListParams(String procName) throws Exception {
		List<String> commandline = null;

		String[] commandParams = commandLine.getTaskListParams(procName);
		Process pr = utils.executeConsoleCommand(commandParams);
		commandline = utils.getConsoleOutput(pr);
		return commandline;
	}

	private List<TaskListParser> extractTaskList() throws Exception {
		List<String> taskList = getTaskListParams(UsingXml.getXmlValue("processName", "memoryMonetizationConfig.xml"));
		taskList.remove(0);
		taskList.remove(0);

		List<TaskListParser> parserList = new ArrayList<TaskListParser>();
		for (String s : taskList) {
			TaskListParser parser = new TaskListParser(s);
			parserList.add(parser);
		}

		Collections.sort(parserList, Collections.reverseOrder());
		return parserList;
	}

	public List<Integer> returnFirstTowSortedProcess() throws Exception {
		List<Integer> FirstTowSortedProcess = new ArrayList<Integer>();
		List<TaskListParser> allProcesses = extractTaskList();
		for (TaskListParser procId : allProcesses.subList(0, 2)) {
			FirstTowSortedProcess.add(procId.getprocessId());
		}
		return FirstTowSortedProcess;
	}
}