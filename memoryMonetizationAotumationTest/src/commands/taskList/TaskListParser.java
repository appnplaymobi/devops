package commands.taskList;

public class TaskListParser implements Comparable<TaskListParser> {

	private int processId;
	private int memUsage;

	public TaskListParser(String taskLine) {
		String[] outPut = taskLine.split("\\s+");
		this.processId = Integer.parseInt(outPut[1]);
		this.memUsage = Integer.parseInt(outPut[4].replace(",", ""));
	}

	public int getprocessId() {
		return processId;
	}

	public int getmemUsage() {
		return memUsage;
	}

	@Override
	public int compareTo(TaskListParser other) {
		return getmemUsage() - other.getmemUsage();
	}

}