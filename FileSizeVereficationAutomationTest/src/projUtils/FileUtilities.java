package projUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;
import xmlConfiguration.UsingXml;

public class FileUtilities {

	public static String getConfigurationName() {
		String configurationFileName = "exeTestSize.xml";
		return configurationFileName;
	}
	
	public static List<String> getListOfFilesTocheck() throws SAXException, IOException, ParserConfigurationException {
		String Recipients = UsingXml.getXmlValue("filesToCheck", getConfigurationName());
		List<String> recipientsAsList = Arrays.asList(Recipients.split(","));
		return recipientsAsList;
	}
	
	public static String getDownloadFromPathLocation() throws ParserConfigurationException, SAXException, IOException {
		String downlodFromPath = UsingXml.getXmlValue("linkToDownload", getConfigurationName());
		return downlodFromPath;
	}

	public static String getDownloadToPathLocation() throws ParserConfigurationException, SAXException, IOException {
		String downloadToPath = System.getProperty("user.home") + "\\Downloads\\";
		return downloadToPath;
	}
	
	public static String getExeName(String downloadFrom) throws ParserConfigurationException, SAXException, IOException {
		String downloadFileName = FilenameUtils.getName(downloadFrom);
		if (!downloadFileName.contains(".")) {
			downloadFileName = downloadFileName + ".exe"; 
		}
		return downloadFileName;
	}
	
	public static String getSlackUrl() throws ParserConfigurationException, SAXException, IOException {
		String downloadToPath = UsingXml.getXmlValue("slackUrl", getConfigurationName());
		return downloadToPath;
	}
	
	public static String getSlackChannel() throws ParserConfigurationException, SAXException, IOException {
		String downloadToPath = UsingXml.getXmlValue("automationAlertsChannel", getConfigurationName());
		return downloadToPath;
	}
}