package projUtils;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import slackMessaging.MessageToSlack;

public class ErrorMessages {

	private static MessageToSlack slack = new MessageToSlack();

	public static String empyFileMsg(String fileName, String downloadFrom) {
		String msg = String.format("The file: %s,  that was download from: %s, is empty", fileName, downloadFrom);
		return msg;
	}
	
	public static String brokenUrl(String url) {
		String msg = String.format("The response code of the url: %s, is 404", url);
		return msg;
	}

	public static String fileNotDownloadMsg(String fileName, String downloadFrom) {
		String msg = String.format("The file: %s,  that was download from: %s, could not be download", fileName,
				downloadFrom);
		return msg;
	}

	public static void sendMsg(String message)
			throws IOException, ParserConfigurationException, SAXException {
		slack.notificationMassage("#FF0000", message, FileUtilities.getSlackUrl(),
				FileUtilities.getSlackChannel());
	}
}