package manageDownloadFile;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;

public class DownloadAndCheck {

	public void downLoadFile(String from, String downlodLocation, String fileName)
			throws MalformedURLException, IOException {
		FileUtils.copyURLToFile(new URL(from), new File(downlodLocation + fileName), 60000, 60000);
	}

	private long getFileSizeInKB(String fileToCheck) {
		long fileSize = 0;
		fileSize = new File(fileToCheck).length();
		return fileSize;
	}

	public boolean ifFileExist(String exeToCheck) {
		File file = new File(exeToCheck);
		boolean ifExists = file.exists();
		return ifExists;
	}

	public boolean checkFileSize(String downlodLocation, String fileName)
			throws ParserConfigurationException, SAXException, IOException {
		return getFileSizeInKB(downlodLocation + fileName) < 2;
	}
	
	public boolean validUrl(String urlToCheck) throws IOException {
		boolean statusCode = false;
		
		HttpURLConnection connection;
		try  {
			URL url = new URL(urlToCheck);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();
			int code = connection.getResponseCode();
			if (code == 200) {
				statusCode = true;
			}
			connection.disconnect();
		} catch (Exception e) {
			return false;
		}

		return statusCode;
	}
}