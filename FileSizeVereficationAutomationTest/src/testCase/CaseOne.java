package testCase;

import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import manageDownloadFile.DownloadAndCheck;
import projUtils.ErrorMessages;
import projUtils.FileUtilities;

public class CaseOne {

	private DownloadAndCheck fileUse;
	List<String> linksToFiles;

	public CaseOne(List<String> linksToFiles) throws SAXException, IOException, ParserConfigurationException {
		this.fileUse = new DownloadAndCheck();
		if (linksToFiles == null || linksToFiles.isEmpty()) {
			this.linksToFiles = FileUtilities.getListOfFilesTocheck();
		} else {
			this.linksToFiles = linksToFiles;
		}
	}

	public void getAndTestFile() throws Exception {
		for (String singelFileUrl : linksToFiles) {
			boolean fileExist = fileUse.validUrl(singelFileUrl);
			if (fileExist) {
				try {
					String fileName = FileUtilities.getExeName(singelFileUrl);
					fileUse.downLoadFile(singelFileUrl, FileUtilities.getDownloadToPathLocation(), fileName);
					testFile(FileUtilities.getDownloadToPathLocation(), fileName, singelFileUrl);
				} catch (Exception e) {
					ErrorMessages.sendMsg(e.getMessage());
				}
			} else {
				ErrorMessages.sendMsg(ErrorMessages.brokenUrl(singelFileUrl));
			}

		}
	}

	private void testFile(String testedFileLocation, String testedFileName, String downloadFrom) throws Exception {
		boolean smallFileSize = fileUse.checkFileSize(testedFileLocation, testedFileName);
		boolean fileExist = fileUse.ifFileExist(testedFileLocation + testedFileName);

		if (fileExist && smallFileSize) {
			ErrorMessages.sendMsg(ErrorMessages.empyFileMsg(testedFileName, downloadFrom));
		} else if (!fileExist) {
			ErrorMessages.sendMsg(ErrorMessages.fileNotDownloadMsg(testedFileName, downloadFrom));
		}
	}
}