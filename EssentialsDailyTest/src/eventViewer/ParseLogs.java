package eventViewer;

import com.sun.jna.platform.win32.Advapi32Util.EventLogIterator;
import com.sun.jna.platform.win32.Advapi32Util.EventLogRecord;

public class ParseLogs {

	public boolean lookForEvents() {
		EventLogIterator iter = new EventLogIterator("Microsoft Antimalware");
		boolean foundEvent = false;
		while (iter.hasNext()) {
			EventLogRecord record = iter.next();
			if (record.getEventId() == 1116 || record.getEventId() == 1117 || record.getEventId() == 1118) {
				foundEvent = true;
			}
		}
		return foundEvent;
	}
}