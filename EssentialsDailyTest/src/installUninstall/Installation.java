package installUninstall;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;
import tools.PathManager;
import tools.Utils;
import usingExternalFiles.UsingXmlData;

public class Installation {
	private PathManager pathTo;
	private Utils utils;

	public Installation(PathManager pathTo, Utils utils) {
		this.pathTo = pathTo;
		this.utils = utils;
	}

	public void updateEssentials() throws ParserConfigurationException, SAXException, IOException {
		String essentialsExe = UsingXmlData.getXmlValue("essentialsExeLocation");
		String commandToRun = UsingXmlData.getXmlValue("essentialsCommand");
		utils.run(essentialsExe, commandToRun);
	}

	public void downloadInstallationFile()
			throws MalformedURLException, IOException, ParserConfigurationException, SAXException {
		String azureExePath = pathTo.returnAzureExepath();
		String downloadToLocal = pathTo.returnExeLocalPath();
		try {
			FileUtils.copyURLToFile(new URL(azureExePath), new File(downloadToLocal), 60000, 60000);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String createInstallationScript(String publisherName, String installationChannel)
			throws ParserConfigurationException, SAXException, IOException {

		String exeToInstall = pathTo.returnExeLocalPath();
		String installationScript = exeToInstall + "{'packer':{'DistributerName':'" + publisherName + "','ChannelId':'"
				+ installationChannel + "},'Agent':{'SetAll':'true','TraceLevel':'trace'}}";

		return installationScript;
	}
}