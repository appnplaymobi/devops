package controllingVm;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import tools.Utils;
import usingExternalFiles.UsingXmlData;

public class TurnVmOn {
	private Utils utils;

	public TurnVmOn(Utils utils) {
		this.utils = utils;
	}

	public String powerShellCommand() throws ParserConfigurationException, SAXException, IOException {
		String powerShellLocalPath = UsingXmlData.getXmlValue("powerShellPath");
		String vm = UsingXmlData.getXmlValue("vmName");
		String snapshot = UsingXmlData.getXmlValue("snapshotName");
		
		String powerShellCommand = "Invoke-Command -Computername LINKURYHV -ScriptBlock { Get-VM " + "'" + vm + "' " 
		+ "| get-vmsnapshot -Name " + "'" + snapshot + "'" 
		+ "| Restore-VMSnapshot -Confirm:$false ; Start-VM -Name "  + "'" + vm + "' " + "}";
		
		String finalCommand = powerShellLocalPath + " " + powerShellCommand;
		return finalCommand;
	}

	public void vmOn() throws ParserConfigurationException, SAXException, IOException {
		String turnOnByPowerShell = powerShellCommand();
		utils.run(turnOnByPowerShell, "");
	}
}