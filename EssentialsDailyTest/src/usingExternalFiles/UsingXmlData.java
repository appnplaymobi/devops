package usingExternalFiles;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class UsingXmlData {

	public static String getXmlValue(String elementName)
			throws ParserConfigurationException, SAXException, IOException {
		File fXmlFile = new File("D:/Bitbucket/devops/EssentialsDailyTest/src/usingExternalFiles/projConfiguration.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		return doc.getElementsByTagName(elementName).item(0).getTextContent();
	}

}
