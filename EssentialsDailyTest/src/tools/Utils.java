package tools;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class Utils {

	public void run(String exeToRun, String command) throws ParserConfigurationException, SAXException, IOException {
		Process proc = Runtime.getRuntime().exec(exeToRun + command);
		proc.getOutputStream().close();
	}
}
