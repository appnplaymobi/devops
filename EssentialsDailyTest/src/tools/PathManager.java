package tools;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import usingExternalFiles.UsingXmlData;

public class PathManager {
	
	public String returnAzureExepath() throws ParserConfigurationException, SAXException, IOException {
		String azureExePath = UsingXmlData.getXmlValue("exeFileFromAzure");
		String douwnloadFileName = UsingXmlData.getXmlValue("exeName");
		String fullPath = azureExePath + douwnloadFileName;
		return fullPath;
	}

	public String returnExeLocalPath() throws ParserConfigurationException, SAXException, IOException {
		String downloadLocation = UsingXmlData.getXmlValue("downloadsFolder");
		String douwnloadFileName = UsingXmlData.getXmlValue("exeName");
		String fullPath = downloadLocation + douwnloadFileName;
		return fullPath;
	}
}
