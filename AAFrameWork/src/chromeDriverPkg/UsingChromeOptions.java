package chromeDriverPkg;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.xml.sax.SAXException;

public class UsingChromeOptions {

	private static WebDriver driver;
	private static ChromeOptions chOptions;

	public void initDriver() throws ParserConfigurationException, SAXException, IOException {
		String chromeDriverLocation = new File("chromedriver.exe").getAbsolutePath();
		System.setProperty("webdriver.chrome.driver", chromeDriverLocation);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	public ChromeOptions returnChromeOptions() throws ParserConfigurationException, SAXException, IOException {
		String chromeDriverLocation = new File("chromedriver.exe").getAbsolutePath();
		System.setProperty("webdriver.chrome.driver", chromeDriverLocation);
		chOptions = new ChromeOptions();
		return chOptions;
	}

	public void loadExtensionFromFile(String pathToExtension)
			throws ParserConfigurationException, SAXException, IOException {
		chOptions = returnChromeOptions();
		chOptions.addExtensions(new File(pathToExtension));
		driver = new ChromeDriver(chOptions);
	}

	public void loadExtensionFromFolder(String pathToExtension)
			throws ParserConfigurationException, SAXException, IOException {
		chOptions = returnChromeOptions();
		chOptions.addArguments("--force-dev-mode-highlighting");
		chOptions.addArguments("--load-extension=" + pathToExtension);
		driver = new ChromeDriver(chOptions);
	}
}
