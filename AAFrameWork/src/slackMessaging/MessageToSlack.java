package slackMessaging;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.xml.sax.SAXException;

public class MessageToSlack {

	private HttpPost createHttpPost(String baseSlackUrl, String slackChannel) throws ParserConfigurationException, SAXException, IOException {
		HttpPost httpPost = new HttpPost(baseSlackUrl + slackChannel);
		return httpPost;
	}

	private CloseableHttpResponse createHttpResponce(HttpPost httpPost) throws ClientProtocolException, IOException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = httpClient.execute(httpPost);
		return response;
	}

	private StringEntity createStringEntity(String linkDescription, String linkDetails) {
		StringEntity jsonMessage = new StringEntity("{\"attachments\": [ {\"title\": " + "\"" + linkDescription + "\","
				+ "\"" + "title_link" + "\":" + " \"" + linkDetails + "\"," + "} ]" + "}", "UTF-8");
		jsonMessage.setContentType("application/json");
		return jsonMessage;
	}

	private StringEntity createNotificationMesage(String MessageColor, String notificationMessage) {
		StringEntity jsonMessage = new StringEntity("{\"attachments\": [ {\"color\": " + "\"" + MessageColor + "\","
				+ "\"" + "title" + "\":" + " \"" + notificationMessage + "\"," + "} ]" + "}", "UTF-8");
		jsonMessage.setContentType("application/json");
		return jsonMessage;
	}
	
	public void postMassage(String linkDescription, String linkDetails, String baseSlackUrl, String slackChannel)
			throws IOException, ParserConfigurationException, SAXException {
		HttpPost httpPost = createHttpPost(baseSlackUrl, slackChannel);
		httpPost.addHeader("Content-type", "application/json");
		StringEntity jsonMessage = createStringEntity(linkDescription, linkDetails);
		httpPost.setEntity(jsonMessage);
		CloseableHttpResponse response = createHttpResponce(httpPost);
		response.close();
	}
	
	public void notificationMassage(String MessageColor, String notificationMessage, String baseSlackUrl, String slackChannel)
			throws IOException, ParserConfigurationException, SAXException {
		HttpPost httpPost = createHttpPost(baseSlackUrl, slackChannel);
		httpPost.addHeader("Content-type", "application/json");
		StringEntity jsonMessage = createNotificationMesage(MessageColor, notificationMessage);
		httpPost.setEntity(jsonMessage);
		CloseableHttpResponse response = createHttpResponce(httpPost);
		response.close();
	}
}