package loggingTool;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Log {

	private final static Logger logger = Logger.getLogger("log");
	private static FileHandler logHandler = null;
	private static Log singleTonObj = new Log();
	
	
	private Log() {
	
	}

	public static Log getInstance() {
		if (singleTonObj == null) {
			synchronized (Log.class) {
				singleTonObj = new Log();
			}
		}
		return singleTonObj;
	}

	public String getLogPath(String logName) throws IOException {
		String logPath = "\\\\linkurydc\\Gal.Levy\\Logs\\";
		File logFile = new File(logPath + logName);
		if (!logFile.exists()) {
			logFile.createNewFile();
		}
		return logFile.toString();
	}

	public void initLog(String logName) throws SecurityException, IOException {
		logHandler = new FileHandler(getLogPath(logName), true);
		logHandler.setFormatter(new SimpleFormatter());
		logger.addHandler(logHandler);
	}
	
	public void close() {
		logHandler.close();
	}

	public void loggr(Level level, Exception ex, String msg) {
		try {
			logger.log(level, String.format("%s, %s", msg, ex.toString()));
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}

	}

	public void loggr(Level level, String msg) {
		try {
			logger.log(level, msg);
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
}