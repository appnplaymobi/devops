package xmlConfiguration;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class UsingXml {
	
	public static String getXmlValue(String elementName, String fileName)
			throws ParserConfigurationException, SAXException, IOException {
		String fXmlFile = new File(fileName).getAbsolutePath();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		NodeList element = doc.getElementsByTagName(elementName);
		Node firstItem = element.item(0);
		return firstItem.getTextContent();
	}
}