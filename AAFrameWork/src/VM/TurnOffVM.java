package VM;

import java.io.IOException;

public class TurnOffVM {
	
	public void sendShutDownCommand() throws IOException {
		Runtime child = Runtime.getRuntime();
		child.exec("shutdown -s");
	}
}
