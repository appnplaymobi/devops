package testCases;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.openqa.selenium.Keys;
import generalTools.Base;
import generalTools.FileAndFolderManipulation;
import webElements.DefineElements;

public class CaseOne extends Base{
	static FileAndFolderManipulation fileManipulation = new FileAndFolderManipulation();
	
	public static void test() throws IOException, InterruptedException
	{
		int count = 0;
		int testCounter = 0;
		File dirToRunOver = new File("D:/LP_automation_files/Jason_Files/");
		ArrayList<File> allFiles = new ArrayList<File>();  
		FileAndFolderManipulation.extractFileFromDirectory(dirToRunOver,  allFiles);
		for (File file: allFiles)
		{
			testCounter ++;
			count = testCounter;
			System.out.println("test num: " + count);
			driver.get("https://www.piliapp.com/javascript-validator/");
			driver.navigate().refresh();
			FileAndFolderManipulation.copyToClipboard(FileAndFolderManipulation.readFileContent(file));
			DefineElements.locateTextArea(driver).sendKeys(Keys.chord(Keys.LEFT_CONTROL, "v"));
			DefineElements.checkBtn(driver).click();
			DefineElements.alert(driver);
		}
	}

}
