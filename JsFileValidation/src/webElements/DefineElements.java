package webElements;

import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import generalTools.Base;
import junit.framework.Assert;

public class DefineElements extends Base {
	public static WebElement locateTextArea(WebDriver driver) {
		singleElement = driver.findElement(By.id("javascript"));
		return singleElement;
	}

	public static WebElement checkBtn(WebDriver driver) throws IOException, InterruptedException {
		singleElement = driver.findElement(By.id("check"));
		return singleElement;
	}

	public static WebElement waitFunction(WebDriver driver, String elementValue) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.id(elementValue))));
		return element;
	}

	public static void alert(WebDriver driver) throws IOException, InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.alertIsPresent());
		Alert simpleAlert = driver.switchTo().alert();
		try {
			String alertText = simpleAlert.getText();
			Assert.assertEquals("Congrats! Javascript looks fine.", alertText);
			simpleAlert.accept();
		} catch (AssertionError assertError) {
			String alertText = simpleAlert.getText();
			System.out.println(alertText);
			simpleAlert.accept();
			System.out.println("assertError: " + assertError.getMessage());
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		}
	}
}
