package mainPkg;

import java.io.IOException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import generalTools.Base;
import testCases.CaseOne;

public class ManageTest extends Base {
	@BeforeClass
	public static void startSession() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\gal.levy\\Desktop\\workPlace\\SeleniumDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	@AfterClass
	public static void endSession() {
		driver.quit();
	}

	@Test
	public void test01() throws IOException, InterruptedException {
		CaseOne.test();
	}
}
