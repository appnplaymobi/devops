package generalTools;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ExternalFunctions extends Base {
	public static String getXmlData(String nodeName) throws ParserConfigurationException, SAXException, IOException {
		File fXmlFile = new File("C:/Users/User/workspace/PageObject/src/finalExam/ExternalDetails.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		return doc.getElementsByTagName(nodeName).item(0).getTextContent();
	}

	public static String captureScreen() throws IOException {
		String pathToFile = "C:/Users/gal.levy/Desktop/workPlace/WorkPlace/src/JSvalidation/"
				+ System.currentTimeMillis() + ".png";
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File((pathToFile)));
		return pathToFile;
	}

	public static void timeOut(int inSeconds) throws InterruptedException {
		Thread.sleep(inSeconds);
	}
}
