package generalTools;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {

	protected static WebDriver driver;
	static WebDriverWait wait;
	static List<WebElement> allElements;
	protected static WebElement singleElement;
}
