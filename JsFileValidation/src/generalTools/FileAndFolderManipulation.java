package generalTools;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.io.FileUtils;

public class FileAndFolderManipulation extends Base {

	public static void extractFileFromDirectory(File dir, ArrayList<File> outFiles) throws IOException {
		try {
			File[] files = dir.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					extractFileFromDirectory(file, outFiles);
				} else {
					outFiles.add(file);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String readFileContent(File file) throws IOException {
		String pathToJsFile = FileUtils.readFileToString(file, "UTF-8");
		return pathToJsFile;
	}
	
	public static void copyToClipboard(String str)
	{
		StringSelection selection = new StringSelection(str);
	    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	    clipboard.setContents(selection, selection);
	}
}
