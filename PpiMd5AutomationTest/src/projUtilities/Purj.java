package projUtilities;

import java.io.File;
import java.io.IOException;

public class Purj {

	public static void preformPurj() throws IOException, InterruptedException {
		String purjExe = new File("StrikeTrackerCdn.exe").getAbsolutePath();
		ProcessBuilder processBuilder = new ProcessBuilder(purjExe, UseDataFromXml.getPurjUrl());
		Process p = processBuilder.start();
		p.waitFor();
	}
}