package projUtilities;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;

public class DownloadUtils {

	public String getLocalDwonloadFolder() throws ParserConfigurationException, SAXException, IOException {
		String fullPathToReport = System.getProperty("user.home") + "\\Downloads\\";
		return fullPathToReport;
	}

	public void downLoadFile(String fromFile, String toFile) throws MalformedURLException, IOException {
		try {
			FileUtils.copyURLToFile(new URL(fromFile), new File(toFile), 60000, 60000);

		} catch (Exception e) {
		}
	}

	public String getFirstPpiFileName() {
		String from = UseDataFromXml.getFirstPpi();
		String fileName = FilenameUtils.getName(from);
		return fileName;
	}

	public String getFirstPpiFilePathAndName() {
		String fileName = "";
		try {
			String from = UseDataFromXml.getFirstPpi();
			fileName = getLocalDwonloadFolder() + FilenameUtils.getName(from);
			return fileName;
		}catch (Exception e) {
			return null;
		}
	}
}