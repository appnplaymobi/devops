package projUtilities;

import xmlConfiguration.UsingXml;

public class UseDataFromXml {

	private static String getConfigurationName() {
		String configurationFileName = "PpiMd5Configuration.xml";
		return configurationFileName;
	}

	public static String getWikiUrl() {
		String pubsUrl = "";
		try {
			pubsUrl = UsingXml.getXmlValue("wikiUrl", getConfigurationName());
		} catch (Exception e) {
			return null;
		}
		return pubsUrl;
	}

	public static String getBaseUrl() {
		String baseUrl = "";
		try {
			baseUrl = UsingXml.getXmlValue("baseUrl", getConfigurationName());
		} catch (Exception e) {
			return null;
		}
		return baseUrl;
	}

	public static String getFirstPpi() {
		String url = "";
		try {
			url = UsingXml.getXmlValue("goClientConfigUrl", getConfigurationName());
		} catch (Exception e) {
			return null;
		}
		return url;
	}

	public static String getSlackUrl() {
		String slackBaseUrl = "";
		try {
			slackBaseUrl = UsingXml.getXmlValue("slackUrl", getConfigurationName());
		} catch (Exception e) {
			return null;
		}
		return slackBaseUrl;
	}

	public static String getSlackChannel() {
		String slackChannel = "";
		try {
			slackChannel = UsingXml.getXmlValue("automationAlertsChannel", getConfigurationName());
		} catch (Exception e) {
			return null;
		}
		return slackChannel;
	}
	
	public static String getPurjUrl() {
		String urlToPurj = "";
		try {
			urlToPurj = UsingXml.getXmlValue("purjUrl", getConfigurationName());
		} catch (Exception e) {
			return null;
		}
		return urlToPurj;
	}
}