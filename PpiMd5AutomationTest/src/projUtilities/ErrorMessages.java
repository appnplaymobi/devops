package projUtilities;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import projUtilities.UseDataFromXml;
import slackMessaging.MessageToSlack;

public class ErrorMessages {

	private static MessageToSlack slack = new MessageToSlack();

	private static String msgTestFail(String publisherName) {
		String msg = String.format("The publisher: %s, has different md5.", publisherName);
		return msg;
	}
	
	private static String msgTestpass() {
		String msg = String.format("All publishers download exe, has the same Md5");
		return msg;
	}
	
	private static String msgLog(String message) {
		String msg = String.format("test failed see file details: %s: %s", message);
		return msg;
	}
	
	private static String msgforPurj() {
		String msg = String.format("Purj was performed");
		return msg;
	}

	public static void sendFailMsg(String publisherName) throws IOException, ParserConfigurationException, SAXException {
		slack.notificationMassage("#FF0000", msgTestFail(publisherName), UseDataFromXml.getSlackUrl(),
				UseDataFromXml.getSlackChannel());
	}
	
	public static void sendMsgTestPass() throws IOException, ParserConfigurationException, SAXException {
		slack.notificationMassage("#23bd26", msgTestpass(), UseDataFromXml.getSlackUrl(),
				UseDataFromXml.getSlackChannel());
	}

	public static void sendLogMsg(String message)
			throws IOException, ParserConfigurationException, SAXException {
		slack.notificationMassage("#FF0000", msgLog(message), UseDataFromXml.getSlackUrl(),
				UseDataFromXml.getSlackChannel());
	}
	
	public static void sendPurjMsg() throws IOException, ParserConfigurationException, SAXException {
		slack.notificationMassage("#23bd26", msgforPurj(), UseDataFromXml.getSlackUrl(),
				UseDataFromXml.getSlackChannel());
	}
}