package wikiTableData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;
import chromeDriverPkg.UsingDriver;
import projUtilities.UseDataFromXml;

public class CollectPublishersFromWiki {

	private UsingDriver driver = new UsingDriver();

	public List<String> getPublishers() throws ParserConfigurationException, SAXException, IOException {
		driver.initDriver();
		driver.getDriver().get(UseDataFromXml.getWikiUrl());
		driver.waitForPageToLoad();
		return addPublishersToList();
	}

	private List<String> addPublishersToList() {
		List<String> allPublishers = new ArrayList<String>();
		List<WebElement> rows = driver.getElements("//*[@id='bodyContent']/table[1]/tbody/tr");
		for (WebElement row : rows) {
			WebElement firstCell = row.findElements(By.tagName("th")).get(0);
			String style = firstCell.getAttribute("style");
			if (style.equals("background: rgb(145, 255, 101);")) {
				allPublishers.add(firstCell.getText());
			}
		}
		driver.endDriver();
		return allPublishers;
	}
}