package ppi;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import projUtilities.DownloadUtils;
import projUtilities.ErrorMessages;
import projUtilities.Purj;
import projUtilities.UseDataFromXml;
import wikiTableData.CollectPublishersFromWiki;

public class DownloadAndTestMd5 {

	private CollectPublishersFromWiki getPublishersNames;
	private DownloadUtils downloadUtils;

	public DownloadAndTestMd5() {
		this.getPublishersNames = new CollectPublishersFromWiki();
		this.downloadUtils = new DownloadUtils();
	}

	private void downloadPpiPerPublisher(String exeName)
			throws ParserConfigurationException, SAXException, IOException {
		String toFile = downloadUtils.getLocalDwonloadFolder() + exeName.concat(".exe");
		String fromFile = UseDataFromXml.getBaseUrl() + exeName;
		downloadUtils.downLoadFile(fromFile, toFile);
	}

	private String downloadFirstPpiAndGetMd5() {
		String md5TocompareWith;
		try {
			String toFile = downloadUtils.getLocalDwonloadFolder() + downloadUtils.getFirstPpiFileName();
			String fromFile = UseDataFromXml.getFirstPpi();
			downloadUtils.downLoadFile(fromFile, toFile);
			md5TocompareWith = getMd5(downloadUtils.getFirstPpiFilePathAndName());
			return md5TocompareWith;
		} catch (Exception e) {
			return null;
		}
	}

	public String getMd5(String fileToCheck) {
		String md5 = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(Files.readAllBytes(Paths.get(fileToCheck)));
			byte[] digest = md.digest();
			md5 = DatatypeConverter.printHexBinary(digest).toUpperCase();
		} catch (Exception e) {
		}
		return md5;

	}

	public void testAllPpis() throws IOException, ParserConfigurationException, SAXException {
		try {
			String md5TocompareWith = downloadFirstPpiAndGetMd5();
			List<String> diffMd5Pubs = new ArrayList<String>();
			downloadPPIperPublisher(md5TocompareWith, diffMd5Pubs);
			if (diffMd5Pubs.isEmpty()) {
				ErrorMessages.sendMsgTestPass();
			} else {
				for (String pubName : diffMd5Pubs) {
					ErrorMessages.sendFailMsg(pubName);
				}
				Purj.preformPurj();
				ErrorMessages.sendPurjMsg();
			}
		} catch (Exception e) {
			ErrorMessages.sendLogMsg(e.getMessage());
		}
	}

	private void downloadPPIperPublisher(String md5TocompareWith, List<String> diffMd5Pubs)
			throws ParserConfigurationException, SAXException, IOException {
		List<String> allPubs = getPublishersNames.getPublishers();
		for (String publisher : allPubs) {
			downloadPpiPerPublisher(publisher);
			String md5TocompareTo = getMd5(downloadUtils.getLocalDwonloadFolder() + publisher.concat(".exe"));
			if (!md5TocompareWith.equals(md5TocompareTo)) {
				diffMd5Pubs.add(publisher);
			}
		}
	}
}