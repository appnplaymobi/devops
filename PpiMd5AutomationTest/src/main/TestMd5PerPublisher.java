package main;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import VM.TurnOffVM;
import ppi.DownloadAndTestMd5;

public class TestMd5PerPublisher {

	public static void main(String[] args)
			throws ParserConfigurationException, SAXException, IOException, NoSuchAlgorithmException, InterruptedException {
		
		DownloadAndTestMd5 downloadAndTestMd5 = new DownloadAndTestMd5();
		downloadAndTestMd5.testAllPpis();
		TurnOffVM turn = new TurnOffVM();
		turn.sendShutDownCommand();
		}
}