package Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;
import chromeDriverPkg.UsingDriver;
import manageDates.GetDate;

public class ZX {

	public static void main(String[] args)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {

		as();

	}

	public static void as() throws ParserConfigurationException, SAXException, IOException, InterruptedException {
		UsingDriver driver = new UsingDriver();
		driver.initDriver();
		driver.getDriver().get("https://www.google.com/doodles/#archive");
		driver.waitForPageToLoad();
		Thread.sleep(1000);
		List<String> allDates = new ArrayList<String>();
		List<WebElement> d = driver.getDriver().findElements(By.cssSelector(".thumb-container .info .date"));
		List<WebElement> r = driver.getDriver().findElements(By.cssSelector(".card-container .cardlink:nth-child(2) a"));
		List<String> allDoodels = new ArrayList<String>();
		for (WebElement f : d) {
			// System.out.println(f.getText());
			allDates.add(f.getText());
		}
		for (String dd : allDates) {
			if (dd.equals(GetDate.getYesterdayDateString())) {
				System.out.println(dd);
			}
		}

	}
}
