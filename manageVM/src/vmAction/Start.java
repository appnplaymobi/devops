package vmAction;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class Start {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		VM vm = new VM();
		vm.On();
	}
}
