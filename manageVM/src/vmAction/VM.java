package vmAction;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import configuration.UsingXML;

public class VM {
	
	private void onWithPScommand(String powerShellPath, String vmName, String checkPointName) throws IOException {
		String powerShellCommand = "Invoke-Command -Computername LINKURYHV -ScriptBlock { Get-VM " + "'" + vmName
				+ "' | get-vmsnapshot -Name " + "'" + checkPointName
				+ "' | Restore-VMSnapshot -Confirm:$false ; Start-VM -Name " + "'" + vmName + "'}";
		Process process = Runtime.getRuntime().exec(powerShellPath + " " + powerShellCommand);
		process.getOutputStream().close();
	}

	public void On() throws ParserConfigurationException, SAXException, IOException {
		String usingPowerShell = UsingXML.getXmlValue("powerShellPath");
		String usingVm = UsingXML.getXmlValue("vmName");
		String usingCheckPoint = UsingXML.getXmlValue("checkPointName");
		onWithPScommand(usingPowerShell, usingVm, usingCheckPoint);
	}
}