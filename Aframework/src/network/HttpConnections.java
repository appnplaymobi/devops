package network;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class HttpConnections {

	public URL returnURLToCheck(String singlUrl) throws MalformedURLException {
		URL urlToCheck;
		urlToCheck = new URL(singlUrl);
		return urlToCheck;
	}

	public String createConnection(String singlUrl) throws IOException {
		URL urlToCheck = returnURLToCheck(singlUrl);
		String response = "";

		HttpURLConnection connection = (HttpURLConnection) urlToCheck.openConnection();
		connection.connect();
		response = connection.getResponseMessage();
		connection.disconnect();
		return response;
	}

}
