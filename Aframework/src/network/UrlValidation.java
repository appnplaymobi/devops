package network;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class UrlValidation {
	
	public boolean checkIfUrlIsUnknownSource(String urlToCheck) throws IOException {
		try {
			URL u = new URL(urlToCheck);
			HttpURLConnection huc = (HttpURLConnection) u.openConnection();
			huc.setRequestMethod("GET");
			huc.connect();
			@SuppressWarnings("unused")
			String respMsg = huc.getResponseMessage();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean ifUnknownSource(String urlToCheck) throws IOException{
		boolean knownSource = checkIfUrlIsUnknownSource(urlToCheck);
		if (! knownSource){
			return true;
		}
		return false;
	}

}
