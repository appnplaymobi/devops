package webElements;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebElementRelatedFunctions {
		
	public String getPageTitle(WebDriver driver) {
		String pageTitle = driver.getTitle();
		return pageTitle;
	}

	public String getPageUrl(WebDriver driver) {
		String pageUrl = driver.getCurrentUrl();
		return pageUrl;
	}

	public List<String> fromWebElementToString(List<WebElement> lst, String value) {
		List<String> stringsList = new ArrayList<String>();
		for (WebElement link : lst) {
			stringsList.add(link.getAttribute(value));
		}
		return stringsList;
	}

	public List<WebElement> findListElementsByTagName(WebDriver driver, String tagName) {
		try {
			return driver.findElements(By.tagName(tagName));
		} catch (Exception e) {
			return null;
		}
	}

	public WebElement findSingleElementByTagName(WebDriver driver, String tagName) {
		try {
			return driver.findElement(By.tagName(tagName));
		} catch (Exception e) {
			return null;
		}
	}

	public List<WebElement> findListElementsByClassName(WebDriver driver, String className) {
		try {
			return driver.findElements(By.className(className));
			//allElements = driver.findElements(By.className(className));
			//return allElements;
		} catch (Exception e) {
			return null;
		}
	}
	
	public WebElement findSingleElementByClassName(WebDriver driver, String className) {
		try {
			return driver.findElement(By.tagName(className));
		} catch (Exception e) {
			return null;
		}
	}
	
	public WebElement findSingleElementByXpath(WebDriver driver, String xpathName) {
		try {
			return driver.findElement(By.xpath(xpathName));
		} catch (Exception e) {
			return null;
		}
	}

	public static WebElement waitByClassName(WebDriver driver, String elementValue) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated((By.className(elementValue))));
		return element;
	}

}
