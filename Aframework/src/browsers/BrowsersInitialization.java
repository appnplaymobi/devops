package browsers;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.xml.sax.SAXException;
import externalFiles.FilesAndFolders;

public class BrowsersInitialization {
	ChromeDriver driver = new ChromeDriver();
	FilesAndFolders files = new FilesAndFolders();

	public WebDriver initChrome(String xmlPath, String valueToExtract)
			throws ParserConfigurationException, SAXException, IOException {
		System.setProperty("webdriver.chrome.driver", files.getDataFromXML(xmlPath, valueToExtract));
		driver.manage().window().maximize();
		return driver;
	}
}
