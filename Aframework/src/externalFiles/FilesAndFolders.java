package externalFiles;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class FilesAndFolders {

	public String getDataFromXML(String elementName, String xmlPath)
			throws ParserConfigurationException, SAXException, IOException {
		File fXmlFile = new File(xmlPath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();

		NodeList element = doc.getElementsByTagName(elementName);
		Node firstItem = element.item(0);
		return firstItem.getTextContent();
	}

	public static void deleteDir(String dirTodelete) {
		try {
			File f = new File(dirTodelete);
			FileUtils.forceDelete(f);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public static void createDir(String dirToCreatePath) {
		File dir = new File(dirToCreatePath);
		dir.mkdir();
	}

	public static void writeToFile(String fileName, String fileContent, String logContent) throws IOException {
		try {
			fileName = fileContent;
			PrintWriter printWriter = null;
			File file = new File(fileName);
			try {
				if (!file.exists()) {
					file.createNewFile();
				}
				printWriter = new PrintWriter(new FileOutputStream(fileName, true));
				printWriter.write(logContent);
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} finally {
				if (printWriter != null) {
					printWriter.flush();
					printWriter.close();
				}
			}
		} catch (Exception e) {
		}
	}

}
