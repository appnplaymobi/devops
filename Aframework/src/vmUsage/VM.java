package vmUsage;

import java.io.IOException;

public class VM {

	public void turnVMon(String vmName, String snampShotName) throws IOException {
		String powerShellPath = "C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe ";
		String powerShellCommand = "Invoke-Command -Computername LINKURYHV -ScriptBlock { Get-VM '" + vmName
				+ "' | get-vmsnapshot -Name '" + snampShotName
				+ "' | Restore-VMSnapshot -Confirm:$false ; Start-VM -Name '" + vmName + "'}";
		Process process = Runtime.getRuntime().exec(powerShellPath + powerShellCommand);
		process.getOutputStream().close();
	}
	
	public void turnVMoff(String vmName, String snampShotName) throws IOException {
		String powerShellPath = "C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe ";
		String powerShellCommand = "Invoke-Command -Computername LINKURYHV -ScriptBlock { Stop-VM -Name '" + vmName + "'}";
		Process process = Runtime.getRuntime().exec(powerShellPath + powerShellCommand);
		process.getOutputStream().close();
	}
		
}
