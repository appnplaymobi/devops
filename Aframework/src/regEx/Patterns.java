package regEx;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Patterns {

	public String findMailRegEx(String regExToFind, String StringToSearch) {
		Pattern checkRegEx = Pattern.compile(regExToFind);
		Matcher regMatch = checkRegEx.matcher(StringToSearch);

		while (regMatch.find()) {
			if (regMatch.group().trim().length() != 0) {
				return regMatch.group(0);
			}
		}
		return null;
	}

	public List<String> findRegEx(String regExToFind, String StringToSearch) {
		Pattern checkRegEx = Pattern.compile(regExToFind);
		Matcher regMatch = checkRegEx.matcher(StringToSearch);
		List<String> allMatches = new ArrayList<String>();

		while (regMatch.find()) {
			String matchGroup = regMatch.group().trim();
			if (!(matchGroup.length() > 10)) {
				allMatches.add(matchGroup);
			}
		}
		List<String> ret = new ArrayList<>(new HashSet<>(allMatches));
		return ret;
	}

	public String ifPatternExist(String regExToFind, String StringToSearch) {
		List<String> allMaches = findRegEx(regExToFind, StringToSearch);

		if (allMaches != null) {
			for (String possibleMatch : allMaches) {
				return possibleMatch;
			}
		}
		return null;
	}

}
