package time;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeMeasuring {

	public static long startNanoTime() throws IOException {
		long start = System.nanoTime();
		return start;
	}

	public static long endNanoTime() throws IOException {
		long end = System.nanoTime();
		return end;
	}

	public static String returnTodayDate() {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
		String todayDate = dateFormat.format(now);
		return todayDate;
	}

	public static void createTodayDir() {
		String todayDir = returnTodayDate();
		File dir = new File(todayDir);
		dir.mkdir();
	}
}
